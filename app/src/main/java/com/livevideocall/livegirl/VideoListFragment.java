package com.livevideocall.livegirl;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.livevideocall.livegirl.adapters.AdapterVideos;
import com.livevideocall.livegirl.databinding.ItemBannerownadBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.CountryRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.models.ThumbRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VideoListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private static final String WEBSITE = "WEBSITE";
    RecyclerView recyclerView;
    AdView adView;
    ProgressBar pd;
    SwipeRefreshLayout swipeRefreshLayout;
    int count = 0;
    private View view;
    private CountryRoot.Datum model;
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Facebook facebook;
    private FragmentActivity context;
    private String ownAdBannerUrl;
    private com.facebook.ads.AdView adViewfb;
    private ImageView imgOwnAd;
    private String adid;
    private String ownWebUrl;
    private AdapterVideos adapterVideos;
    private RelativeLayout lytOwnInter;
    private AdvertisementRoot.Google2 google2;
    private LinearLayout adContainer;
    private RelativeLayout adlyt;
    private ItemBannerownadBinding ownbinding;
    private OwnAdsRoot.DataItem ownAds;

    public VideoListFragment(CountryRoot.Datum model) {

        this.model = model;
        Log.d("TAGcc", "VideoListFragment: " + model.get_id());
    }

    public VideoListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_video_list, container, false);
        context = getActivity();
        recyclerView = view.findViewById(R.id.rvVideos);
        imgOwnAd = view.findViewById(R.id.imgOwnAd);
        lytOwnInter = view.findViewById(R.id.lytOwnInter);
        pd = view.findViewById(R.id.pd);
        swipeRefreshLayout = view.findViewById(R.id.swipe);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        swipeRefreshLayout.setOnRefreshListener(this);


        pd.setVisibility(View.VISIBLE);
        ownbinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.item_bannerownad, (ViewGroup) view.getParent(), false);

        initMain();

    }

    private void initMain() {


        if (model != null && model.get_id() != null) {
            Call<ThumbRoot> call = RetrofitBuilder.create(getActivity()).getThumbs(model.get_id());
            call.enqueue(new Callback<ThumbRoot>() {
                @Override
                public void onResponse(Call<ThumbRoot> call, Response<ThumbRoot> response) {

                    if (response.code() == 200 && !response.body().getData().isEmpty()) {


                        adapterVideos = new AdapterVideos(model.get_id());
                        adapterVideos.addData(response.body().getData());
                        recyclerView.setAdapter(adapterVideos);


                        for (int i = 0; i < response.body().getData().size(); i++) {
                            if (i != 0 && i % 2 == 0) {
//                        setNativeLogic();
                                Log.d("TAG", "onResponse: natvie fetch");
                            }
                        }


                        Log.d("TAG", "onResponse:ccccc  size " + response.body().getData().size());
                    }

                    swipeRefreshLayout.setRefreshing(false);
                    pd.setVisibility(View.GONE);
                    view.findViewById(R.id.pd).setVisibility(View.GONE);
                }

                private void setNativeLogic() {
                    //TODO if google ni ads show naa thay to admin panel ma test id nakhi deva google native ma
                    AdLoader adLoader = new AdLoader.Builder(getActivity(), (google != null) ? google.getJsonMemberNative() : " ")
                            .forUnifiedNativeAd(unifiedNativeAd -> {
                                Log.d("TAG", "onUnifiedNativeAdLoaded: loded");
                                adapterVideos.addAdsOBJ(unifiedNativeAd, count);
                                if (count <= adapterVideos.data.size()) {
                                    count += 0;
                                    setNativeLogic();
                                }
                            })
                            .withAdListener(new AdListener() {
                                @Override
                                public void onAdFailedToLoad(LoadAdError adError) {
                                    // Handle the failure by logging, altering the UI, and so on.
                                }
                            })
                            .withNativeAdOptions(new NativeAdOptions.Builder()
                                    // Methods in the NativeAdOptions.Builder class can be
                                    // used here to specify individual options settings.
                                    .build())
                            .build();
                    adLoader.loadAd(new AdRequest.Builder().build());

                }

                @Override
                public void onFailure(Call<ThumbRoot> call, Throwable t) {
//ll
                }
            });

        }

        getOwnAds();
        SessionManager sessionManager = new SessionManager(getActivity());
        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {


            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
            }
            setBanner(view);
        }
    }

    private void setBanner(View container) {

        adContainer = container.findViewById(R.id.banner_container);
        adView = new AdView(container.getContext());

        if (google != null) {
            Log.d("TAG", "setBanner: " + google.getBanner());

        } else {
            setFbBanner();
            Log.d("TAG", "setBanner: google is null");
        }

        adView.setAdUnitId((google != null) ? google.getBanner() : " ");
        adContainer.setVisibility(View.VISIBLE);
        adContainer.removeAllViews();
        adContainer.addView(adView);

        AdRequest adRequest =
                new AdRequest.Builder()
                        .build();
        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        Log.d("TAG", "setBanner: sucsses");
        adView.loadAd(adRequest);


        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                adContainer.setVisibility(View.GONE);
                setgooglebanner2();
                Log.d("TAG", "onAdFailedToLoad: banner " + loadAdError);
            }

            private void setgooglebanner2() {
                adContainer.setVisibility(View.VISIBLE);
                adView = new AdView(container.getContext());

                if (google2 != null) {
                    Log.d("TAG", "setBanner: " + google2.getBanner());

                } else {
                    Log.d("TAG", "setBanner: google is null");

                }

                adView.setAdUnitId((google2 != null) ? google2.getBanner() : " ");
                adContainer.setVisibility(View.VISIBLE);
                adContainer.removeAllViews();
                adContainer.addView(adView);

                AdRequest adRequest =
                        new AdRequest.Builder()
                                .build();
                AdSize adSize = getAdSize();
                adView.setAdSize(adSize);
                Log.d("TAG", "setBanner: sucsses");
                adView.loadAd(adRequest);


                adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        adContainer.setVisibility(View.GONE);
                        setFbBanner();
                        Log.d("TAG", "onAdFailedToLoad: banner " + loadAdError);
                    }
                });
            }
        });


    }

    private void setFbBanner() {
        adViewfb = new com.facebook.ads.AdView(getActivity(), (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
        adContainer.addView(adViewfb);
        adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Log.d("TAG", "onError: fb " + adError.getErrorMessage());
                ownbinding.lytad.setVisibility(View.VISIBLE);
                if (ownAds != null && ownAds.getId() != null && !ownAds.getId().equals("")) {
                    Anylitecs.sendImpression(context, ownAds.getId());
                }
                adContainer.setVisibility(View.GONE);
                //   binding.itemBannerownad.lytad.setVisibility(View.VISIBLE);
                       /* binding.imgOwnAd.setVisibility(View.VISIBLE);
                        sendImpression(ProfileActivity.this, adid);
                        binding.imgOwnAd.setOnClickListener(v -> {
                            Intent intent = new Intent(ProfileActivity.this, WebActivity.class);
                            intent.putExtra("ADID", String.valueOf(adid));
                            intent.putExtra(WEBSITE, ownWebUrl);
                            intent.putExtra("type", "ads");
                            startActivity(intent);
                        });*/

            }

            @Override
            public void onAdLoaded(Ad ad) {
                adContainer.setVisibility(View.VISIBLE);
                Log.d("TAG", "onAdLoaded: facebbok");
//mm
            }

            @Override
            public void onAdClicked(Ad ad) {
//mm
            }

            @Override
            public void onLoggingImpression(Ad ad) {
//mm
            }
        }).build());


      /*  adViewfb = new com.facebook.ads.AdView(getActivity(), (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
        adContainer.setVisibility(View.VISIBLE);
        adContainer.removeAllViews();
        adContainer.addView(adViewfb);
        adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Log.d("TAG", "onError: fb " + adError.getErrorMessage());

                ownbinding.lytad.setVisibility(View.VISIBLE);
                adContainer.setVisibility(View.GONE);
                *//*imgOwnAd.setVisibility(View.VISIBLE);
                if (adid == null) {
                    lytOwnInter.setVisibility(View.GONE);
                } else if (adid.equals("")) lytOwnInter.setVisibility(View.GONE);

                sendImpression(getActivity(), adid);
                imgOwnAd.setOnClickListener(v -> {
                    Intent intent = new Intent(getActivity(), WebActivity.class);
                    intent.putExtra("ADID", String.valueOf(adid));
                    intent.putExtra(WEBSITE, ownWebUrl);
                    intent.putExtra("type", "ads");
                    startActivity(intent);
                });*//*

            }

            @Override
            public void onAdLoaded(Ad ad) {


//mm
            }

            @Override
            public void onAdClicked(Ad ad) {
//mm
            }

            @Override
            public void onLoggingImpression(Ad ad) {
//mm
            }
        }).build());
*/
    }

    private void getOwnAds() {


        RetrofitBuilder.create(context).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                    Log.d("TAG", "onResponse: gert own adsssd vfsfg");
                    ownAds = response.body().getData().get(0);
                    String[] c1 = ownAds.getColor().split("f");
                    String color = c1[0];


                    ownbinding.tvtitle.setText(ownAds.getName());
                    Glide.with(context).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(ownbinding.imglogo);
                    ownbinding.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    ownbinding.tvbtn.setText(ownAds.getBtnText());
                    ownbinding.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(getActivity(), ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    ownbinding.tvdes.setText(ownAds.getDescription());
                    adlyt = view.findViewById(R.id.lytownbanner);
                    adlyt.removeAllViews();
                    adlyt.addView(ownbinding.getRoot());
                    adlyt.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {
                if (adlyt != null) {
                    adlyt.setVisibility(View.GONE);
                }

            }
        });

    }

    private AdSize getAdSize() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(getActivity(), adWidth);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        initMain();

    }
}