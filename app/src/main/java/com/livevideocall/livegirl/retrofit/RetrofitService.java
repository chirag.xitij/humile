
package com.livevideocall.livegirl.retrofit;

import com.google.gson.JsonObject;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.AnylitecsAddRoot;
import com.livevideocall.livegirl.models.AnylitecsRemoveRoot;
import com.livevideocall.livegirl.models.CommentRoot;
import com.livevideocall.livegirl.models.CountryRoot;
import com.livevideocall.livegirl.models.EmojiIconRoot;
import com.livevideocall.livegirl.models.EmojicategoryRoot;
import com.livevideocall.livegirl.models.HitAdsRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.models.PaperRoot;
import com.livevideocall.livegirl.models.ProductKRoot;
import com.livevideocall.livegirl.models.ProfileRoot;
import com.livevideocall.livegirl.models.ReferRoot;
import com.livevideocall.livegirl.models.SettingsRoot;
import com.livevideocall.livegirl.models.ThumbRoot;
import com.livevideocall.livegirl.models.UserRoot;
import com.livevideocall.livegirl.models.ValidationRoot;
import com.livevideocall.livegirl.models.VideoRoot;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface RetrofitService {

    @GET("/country")
    Call<CountryRoot> getCountries();

    @GET("/category")
    Call<EmojicategoryRoot> getCategories();


    @GET("/random")
    Call<ThumbRoot> getThumbs(@Query("country") String cid);

    @GET("/user/profile")
    Call<ProfileRoot> getUser(@Query("user_id") String uid);

    @GET("/random/comment")
    Call<CommentRoot> getComment(@Query("country") String cid);

    @GET("/gift/category")
    Call<EmojiIconRoot> getEmojiByCategory(@Query("category") String cid);

    @GET("/random/video")
    Call<VideoRoot> getVideo(@Query("country") String cid);


    @POST("/user/emaillogin")
    Call<ValidationRoot> validionFromEmail(@Body JsonObject obj);

    @POST("/user/mobilelogin")
    Call<ValidationRoot> validionFromMobile(@Body JsonObject obj);

    @POST("/user")
    Call<UserRoot> signUpUser(@Body JsonObject object);


    @POST("/user/less")
    Call<UserRoot> lessCoin(@Body JsonObject object);

    @POST("/user/add")
    Call<UserRoot> addCoin(@Body JsonObject object);

    @GET("/advertisement/credential")
    Call<AdvertisementRoot> getAdvertisement();

    @GET("/advertisement")
    Call<OwnAdsRoot> getOwnAds();

    @GET("/reward")
    Call<SettingsRoot> getSettings();

    @POST("/impression")
    Call<HitAdsRoot> sendImpression(@Body JsonObject obj);

    @POST("/hit")
    Call<HitAdsRoot> sendhit(@Body JsonObject obj);


    @GET("/admin/api/gets")
    Call<PaperRoot> getPapers();


    @GET("/api/clientpackage")
    Call<ProductKRoot> getProducts(@Query("key") String key);


    @PUT("liveuser")
    Call<AnylitecsRemoveRoot> removeUser(@Query("user_id") String uid);

    @POST("liveuser")
    Call<AnylitecsAddRoot> addUser(@Body JsonObject jsonObject);


    @POST("/user/referral/")
    Call<ReferRoot> enterReferCode(@Query("user_id") String uid, @Body JsonObject jsonObject);
}
