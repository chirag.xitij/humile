package com.livevideocall.livegirl.retrofit;

public interface Const {
    String BASE_URL = "base";
    String IMAGE_URL = "image";


    String PREF_NAME = "userpref";
    String USER = "user";
    String IS_LOGIN = "islogin";


    String ADS = "ads";
    String ADS_Downloded = "adsdownloded";
    String REWARD_COINS = "rewardcoins";
    String PROFILE_IMAGE = "profile";
    String Country = "country";
    String ADS_TIME = "adstime";
}

