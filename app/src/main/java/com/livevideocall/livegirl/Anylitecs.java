package com.livevideocall.livegirl;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.livevideocall.livegirl.models.AnylitecsAddRoot;
import com.livevideocall.livegirl.models.AnylitecsRemoveRoot;
import com.livevideocall.livegirl.models.HitAdsRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Anylitecs {


    public static void addUser(Context context) {
        SessionManager sessionManager = new SessionManager(context);
        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("user_id", sessionManager.getUser().get_id());
            jsonObject.addProperty("country", sessionManager.getStringValue(Const.Country));
            Call<AnylitecsAddRoot> call = RetrofitBuilder.create(context).addUser(jsonObject);
            call.enqueue(new Callback<AnylitecsAddRoot>() {
                @Override
                public void onResponse(Call<AnylitecsAddRoot> call, Response<AnylitecsAddRoot> response) {
                    if (response.code() == 200) {
                        if (response.body().getStatus() == 200) {
                            Log.d("TAGany", "onResponse: anylites send");
                        }
                    }
                }

                @Override
                public void onFailure(Call<AnylitecsAddRoot> call, Throwable t) {
                    Log.d("TAGany", "onFailure: " + t.getMessage());
                }
            });
        }


    }

    public static void removeSesson(Context context) {
        Log.d("TAGany", "removeSesson: started");
        SessionManager sessionManager = new SessionManager(context);
        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            Call<AnylitecsRemoveRoot> call = RetrofitBuilder.create(context).removeUser(sessionManager.getUser().get_id());
            call.enqueue(new Callback<AnylitecsRemoveRoot>() {
                @Override
                public void onResponse(Call<AnylitecsRemoveRoot> call, Response<AnylitecsRemoveRoot> response) {
                    if (response.code() == 200) {
                        if (response.body().getStatus() == 200) {
                            Log.d("TAGany", "onResponse: anylites remove");
                        }
                    }
                }

                @Override
                public void onFailure(Call<AnylitecsRemoveRoot> call, Throwable t) {
                    Log.d("TAGany", "onFailure: " + t.getMessage());
                }
            });
        }
    }


    public static void sendImpression(Context context, String adid) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ad_id", adid);
        jsonObject.addProperty("country", new SessionManager(context).getStringValue(Const.Country));
        Call<HitAdsRoot> call = RetrofitBuilder.create(context).sendImpression(jsonObject);
        call.enqueue(new Callback<HitAdsRoot>() {
            @Override
            public void onResponse(Call<HitAdsRoot> call, Response<HitAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200) {
                    Log.d("TAG", "onResponse: impressonsended");
                }
            }

            @Override
            public void onFailure(Call<HitAdsRoot> call, Throwable t) {
                Log.d("TAG", "onFailure: error 421" + t.toString());
            }
        });

    }


    public static void sendhit(Context context, String adid) {


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ad_id", adid);
        jsonObject.addProperty("country", new SessionManager(context).getStringValue(Const.Country));
        Call<HitAdsRoot> call = RetrofitBuilder.create(context).sendhit(jsonObject);
        call.enqueue(new Callback<HitAdsRoot>() {
            @Override
            public void onResponse(Call<HitAdsRoot> call, Response<HitAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200) {
                    Log.d("TAG", "onResponse: impressonsended");
                }
            }

            @Override
            public void onFailure(Call<HitAdsRoot> call, Throwable t) {
                Log.d("TAG", "onFailure: error 421" + t.toString());
            }
        });

    }
}
