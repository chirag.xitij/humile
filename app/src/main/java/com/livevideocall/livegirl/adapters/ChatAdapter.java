package com.livevideocall.livegirl.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.livevideocall.livegirl.BuildConfig;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.databinding.ItemChatAdBinding;
import com.livevideocall.livegirl.databinding.ItemChatBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.ModelChat;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ADTYPE = 1;
    private static final int CHATTYPE = 2;
    List<ModelChat> chats = new ArrayList<>();
    private Context context;
    private int tpos = 0;
    private AdvertisementRoot.Google2 google2;
    private AdvertisementRoot.Facebook facebook;


    @Override
    public int getItemViewType(int position) {
        if (chats.get(position) == null) {
            return ADTYPE;
        } else {
            return CHATTYPE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        if (viewType == ADTYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_ad, parent, false);
            return new AdViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
            return new ChatViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == ADTYPE) {
            AdViewHolder adViewHolder = (AdViewHolder) holder;
            adViewHolder.setData();
        } else {
            ChatViewHolder viewHolder = (ChatViewHolder) holder;
            viewHolder.setData(position);
        }
    }


    @Override
    public int getItemCount() {
        return chats.size();
    }

    public void addData(ModelChat modelChat) {
        chats.add(modelChat);
        notifyItemInserted(chats.size() - 1);

        tpos++;
        if (tpos == 2) {
            tpos = 0;
            chats.add(null);
            notifyItemInserted(chats.size() - 1);
        }

    }

    public class AdViewHolder extends RecyclerView.ViewHolder {
        ItemChatAdBinding binding;
        private SessionManager sessionManager;
        private AdvertisementRoot.Google google;

        public AdViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemChatAdBinding.bind(itemView);
        }

        public void setData() {

            sessionManager = new SessionManager(context);
            if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {
                if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                    google = sessionManager.getAdsKeys().getGoogle();

                }
                if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                    google2 = sessionManager.getAdsKeys().getGoogle2();
                }
                if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                    facebook = sessionManager.getAdsKeys().getFacebook();
                }

            }


            AdLoader adLoader = new AdLoader.Builder(context, (google != null) ? google.getJsonMemberNative() : "")
                    .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                        @Override
                        public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                            Log.d("adapte images ", "onUnifiedNativeAdLoaded: loded");
                            binding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                            UnifiedNativeAdView adView =
                                    (UnifiedNativeAdView) LayoutInflater.from(context)
                                            .inflate(R.layout.google_native_chet, null);

                            ImageView imageView = adView.findViewById(R.id.ad_media);
                            Glide.with(context.getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage())
                                    .addListener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            return true;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            imageView.setImageDrawable(resource);
                                            return true;
                                        }
                                    }).circleCrop().into(imageView);
                            // Set other ad assets.
                            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                            adView.setBodyView(adView.findViewById(R.id.ad_body));

                            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));

                            /*adView.setPriceView(adView.findViewById(R.id.ad_price));
                            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
                            adView.setStoreView(adView.findViewById(R.id.ad_store));
                            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));*/

                            // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                            ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                            // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                            // check before trying to display them.
                            if (unifiedNativeAd.getBody() == null) {
                                adView.getBodyView().setVisibility(View.INVISIBLE);
                            } else {
                                adView.getBodyView().setVisibility(View.VISIBLE);
                                ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                            }

                            if (unifiedNativeAd.getCallToAction() == null) {
                                adView.getCallToActionView().setVisibility(View.INVISIBLE);
                            } else {
                                adView.getCallToActionView().setVisibility(View.VISIBLE);
                                ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                            }


                            /*if (unifiedNativeAd.getPrice() == null) {
                                adView.getPriceView().setVisibility(View.INVISIBLE);
                            } else {
                                adView.getPriceView().setVisibility(View.VISIBLE);
                                ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                            }

                            if (unifiedNativeAd.getStore() == null) {
                                adView.getStoreView().setVisibility(View.INVISIBLE);
                            } else {
                                adView.getStoreView().setVisibility(View.VISIBLE);
                                ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                            }

                            if (unifiedNativeAd.getStarRating() == null) {
                                adView.getStarRatingView().setVisibility(View.INVISIBLE);
                            } else {
                                ((RatingBar) adView.getStarRatingView())
                                        .setRating(unifiedNativeAd.getStarRating().floatValue());
                                adView.getStarRatingView().setVisibility(View.VISIBLE);
                            }*/

                            /*if (unifiedNativeAd.getAdvertiser() == null) {
                                adView.getAdvertiserView().setVisibility(View.INVISIBLE);
                            } else {
                                ((TextView) adView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
                                adView.getAdvertiserView().setVisibility(View.VISIBLE);
                            }*/

                            // This method tells the Google Mobile Ads SDK that you have finished populating your
                            // native ad view with this native ad.
                            adView.setNativeAd(unifiedNativeAd);

                            // Get the video controller for the ad. One will always be provided, even if the ad doesn't
                            // have a video asset.


                            // Updates the UI to say whether or not this ad has a video asset.

                            adView.setNativeAd(unifiedNativeAd);
                            binding.flAdplaceholderbanner.removeAllViews();
                            binding.flAdplaceholderbanner.addView(adView);
                            Log.d("MyApp", "onUnifiedNativeAdLoaded: loded");
                            // Show the ad.
                        }
                    })
                    .withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {
                            setgoogle2native();
                            // Handle the failure by logging, altering the UI, and so on.
                        }

                        private void setgoogle2native() {
                            AdLoader adLoader = new AdLoader.Builder(context, (google2 != null) ? google2.getJsonMemberNative() : "")
                                    .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                                        @Override
                                        public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                                            Log.d("adapte images ", "onUnifiedNativeAdLoaded: loded");
                                            UnifiedNativeAdView adView =
                                                    (UnifiedNativeAdView) LayoutInflater.from(context)
                                                            .inflate(R.layout.google_native_chet, null);

                                            ImageView imageView = adView.findViewById(R.id.ad_media);
                                            Glide.with(context.getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage())
                                                    .addListener(new RequestListener<Drawable>() {
                                                        @Override
                                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                            return true;
                                                        }

                                                        @Override
                                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                            imageView.setImageDrawable(resource);
                                                            return true;
                                                        }
                                                    }).circleCrop().into(imageView);
                                            // Set other ad assets.
                                            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                                            adView.setBodyView(adView.findViewById(R.id.ad_body));

                                            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));

                            /*adView.setPriceView(adView.findViewById(R.id.ad_price));
                            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
                            adView.setStoreView(adView.findViewById(R.id.ad_store));
                            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));*/

                                            // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                                            ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                                            // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                                            // check before trying to display them.
                                            if (unifiedNativeAd.getBody() == null) {
                                                adView.getBodyView().setVisibility(View.INVISIBLE);
                                            } else {
                                                adView.getBodyView().setVisibility(View.VISIBLE);
                                                ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                                            }

                                            if (unifiedNativeAd.getCallToAction() == null) {
                                                adView.getCallToActionView().setVisibility(View.INVISIBLE);
                                            } else {
                                                adView.getCallToActionView().setVisibility(View.VISIBLE);
                                                ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                                            }


                            /*if (unifiedNativeAd.getPrice() == null) {
                                adView.getPriceView().setVisibility(View.INVISIBLE);
                            } else {
                                adView.getPriceView().setVisibility(View.VISIBLE);
                                ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                            }

                            if (unifiedNativeAd.getStore() == null) {
                                adView.getStoreView().setVisibility(View.INVISIBLE);
                            } else {
                                adView.getStoreView().setVisibility(View.VISIBLE);
                                ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                            }

                            if (unifiedNativeAd.getStarRating() == null) {
                                adView.getStarRatingView().setVisibility(View.INVISIBLE);
                            } else {
                                ((RatingBar) adView.getStarRatingView())
                                        .setRating(unifiedNativeAd.getStarRating().floatValue());
                                adView.getStarRatingView().setVisibility(View.VISIBLE);
                            }*/

                            /*if (unifiedNativeAd.getAdvertiser() == null) {
                                adView.getAdvertiserView().setVisibility(View.INVISIBLE);
                            } else {
                                ((TextView) adView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
                                adView.getAdvertiserView().setVisibility(View.VISIBLE);
                            }*/

                                            // This method tells the Google Mobile Ads SDK that you have finished populating your
                                            // native ad view with this native ad.
                                            adView.setNativeAd(unifiedNativeAd);

                                            // Get the video controller for the ad. One will always be provided, even if the ad doesn't
                                            // have a video asset.


                                            // Updates the UI to say whether or not this ad has a video asset.

                                            adView.setNativeAd(unifiedNativeAd);
                                            binding.flAdplaceholderbanner.removeAllViews();
                                            binding.flAdplaceholderbanner.addView(adView);
                                            Log.d("MyApp", "onUnifiedNativeAdLoaded: loded");
                                            // Show the ad.
                                        }
                                    })
                                    .withAdListener(new AdListener() {
                                        @Override
                                        public void onAdFailedToLoad(LoadAdError adError) {
                                            binding.flAdplaceholderbanner.setVisibility(View.GONE);
                                            setfbnative();
                                            // Handle the failure by logging, altering the UI, and so on.
                                        }

                                        private void setfbnative() {
                                            NativeAd nativeAd = new NativeAd(context, (facebook != null) ? facebook.getJsonMemberNative() : " ");
                                            NativeAdListener nativeAdListener = new NativeAdListener() {
                                                @Override
                                                public void onMediaDownloaded(Ad ad) {
                                                    // Native ad finished downloading all assets
                                                    Log.e("TAG", "Native ad finished downloading all assets.");
                                                }

                                                @Override
                                                public void onError(Ad ad, AdError adError) {
                                                    binding.nativeAdFb.setVisibility(View.GONE);
                                                    // getown ad  >> set show
                                                    RetrofitBuilder.create(context).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
                                                        @Override
                                                        public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                                                            if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {

                                                                OwnAdsRoot.DataItem ownAds = response.body().getData().get(0);
                                                                String[] c1 = ownAds.getColor().split("f");
                                                                String color = c1[0];

                                                                binding.itemBannerownad.tvtitle.setText(ownAds.getName());
                                                                binding.itemBannerownad.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                                                                Glide.with(context.getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemBannerownad.imglogo);
                                                                binding.itemBannerownad.tvbtn.setText(ownAds.getBtnText());
                                                                binding.itemBannerownad.tvbtn.setOnClickListener(new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        String url = ownAds.getWebsite();
                                                                        Intent i = new Intent(Intent.ACTION_VIEW);
                                                                        i.setData(Uri.parse(url));
                                                                        context.startActivity(i);
                                                                    }
                                                                });
                                                                binding.itemBannerownad.tvdes.setText(ownAds.getDescription());
                                                                binding.itemBannerownad.lytad.setVisibility(View.VISIBLE);
                                                            }

                                                            // Native ad failed to load
                                                            Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
                                                        }

                                                        @Override
                                                        public void onFailure(Call<OwnAdsRoot> call, Throwable t) {
                                                            binding.itemBannerownad.lytad.setVisibility(View.GONE);
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onAdLoaded(Ad ad) {
                                                    if (nativeAd == null || nativeAd != ad) {
                                                        return;
                                                    }
                                                    inflateAd(nativeAd);
                                                    // Native ad is loaded and ready to be displayed
                                                    Log.d("TAG", "Native ad is loaded and ready to be displayed!");

                                                }

                                                private void inflateAd(NativeAd nativeAd) {
                                                    binding.nativeAdFb.setVisibility(View.VISIBLE);
                                                    nativeAd.unregisterView();

                                                    // Add the Ad view into the ad container.

                                                    NativeAdLayout nativeAdLayout = binding.nativeAdFb;

                                                    LayoutInflater inflater = LayoutInflater.from(context);
                                                    // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                                                    LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_fb_popup2, nativeAdLayout, false);
                                                    nativeAdLayout.addView(adView);


                                                    // Create native UI using the ad metadata.
                                                    ImageView nativeAdIcon = adView.findViewById(R.id.ad_logo);
                                                    com.facebook.ads.MediaView nativemedia = adView.findViewById(R.id.ad_media);
                                                    TextView nativeAdTitle = adView.findViewById(R.id.ad_headline);
                                                    /*MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);*/
                                                    /* TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);*/
                                                    TextView nativeAdBody = adView.findViewById(R.id.ad_body);
                                                    /* TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);*/
                                                    TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

                                                    // Set the Text.
                                                    nativeAdTitle.setText(nativeAd.getAdvertiserName());
                                                    nativeAdBody.setText(nativeAd.getAdBodyText());
                                                    /*nativeAdSocialContext.setText(nativeAd.getAdSocialContext());*/
                                                    nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                                                    nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//

                                                    // Create a list of clickable views
                                                    List<View> clickableViews = new ArrayList<>();
                                                    clickableViews.add(nativeAdTitle);
                                                    clickableViews.add(nativeAdCallToAction);

                                                    // Register the Title and CTA button to listen for clicks.
                                                    nativeAd.registerViewForInteraction(
                                                            adView, nativemedia, nativeAdIcon, clickableViews);
                                                }

                                                @Override
                                                public void onAdClicked(Ad ad) {

                                                }

                                                @Override
                                                public void onLoggingImpression(Ad ad) {

                                                }
                                            };

                                            nativeAd.loadAd(
                                                    nativeAd.buildLoadAdConfig()
                                                            .withAdListener(nativeAdListener)
                                                            .build());

                                        }

                                    })
                                    .withNativeAdOptions(new NativeAdOptions.Builder()
                                            // Methods in the NativeAdOptions.Builder class can be
                                            // used here to specify individual options settings.
                                            .build())
                                    .build();
                            adLoader.loadAd(new AdRequest.Builder().build());
                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder()
                            // Methods in the NativeAdOptions.Builder class can be
                            // used here to specify individual options settings.
                            .build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());
        }
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder {
        ItemChatBinding binding;

        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemChatBinding.bind(itemView);
        }

        public void setData(int position) {
            ModelChat modelChat = chats.get(position);
            binding.tvrobot.setVisibility(View.GONE);
            binding.tvuser.setVisibility(View.GONE);
            binding.lytimagerobot.setVisibility(View.GONE);
            binding.lytimageuser.setVisibility(View.GONE);
            if (modelChat.isUser()) {
                if (!modelChat.getMessage().equals("")) {
                    binding.tvuser.setText(modelChat.getMessage());
                    binding.tvuser.setVisibility(View.VISIBLE);
                }
                if (modelChat.getImageuri() != null) {
                    binding.imageuser.setImageURI(modelChat.getImageuri());
                    binding.lytimageuser.setVisibility(View.VISIBLE);
                }

            }

            if (!modelChat.isUser()) {
                if (!modelChat.getMessage().equals("")) {
                    binding.tvrobot.setText(modelChat.getMessage());
                    binding.tvrobot.setVisibility(View.VISIBLE);
                }
                if (modelChat.getImagename() != null) {
                    binding.imagerobot.setImageResource(context.getResources().getIdentifier("raw/" + modelChat.getImagename(), null, context.getPackageName()));
                    binding.lytimagerobot.setVisibility(View.VISIBLE);
                }

            }

        }
    }
}
