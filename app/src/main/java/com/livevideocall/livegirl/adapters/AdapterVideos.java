package com.livevideocall.livegirl.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.gson.Gson;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.activity.VideoActivity2;
import com.livevideocall.livegirl.databinding.ItemNativeadImagesBinding;
import com.livevideocall.livegirl.databinding.ItemVideoBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.ThumbRoot;
import com.livevideocall.livegirl.retrofit.Const;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AdapterVideos extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ADTYPE = 1;
    private static final int VIEWTYPE = 2;
    public List<Object> data = new ArrayList<>();
    private Context context;
    private String cid;

    private AdvertisementRoot.Google google;
    private int adPosition = 5;
    private int adSetPos = 0;
    private InterstitialAd mInterstitialAd;


    public AdapterVideos(String cid) {

        this.cid = cid;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof UnifiedNativeAd) {
            return ADTYPE;
        } else {
            return VIEWTYPE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        /* *//*if (viewType == ADTYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nativead_images, parent, false);
            return new AdViewHolder(view);*//*
        } else {*/
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false);
        return new VideoViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        if (getItemViewType(position) == ADTYPE) {
            AdViewHolder adViewHolder = (AdViewHolder) holder;
            adViewHolder.setData(position);
        } else {
            VideoViewHolder videoViewHolder = (VideoViewHolder) holder;
            videoViewHolder.setData(position);
        }
    }

    public void addData(List<ThumbRoot.Datum> list) {
        for (int i = 0; i < list.size(); i++) {
            this.data.add(list.get(i));
            notifyItemInserted(list.size() - 1);
        }
    }

    public void addAdsOBJ(UnifiedNativeAd bannerUnit, int i) {
        if (i < data.size()) {
            data.add(i, bannerUnit);
            notifyItemInserted(i);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {
        ItemNativeadImagesBinding binding;
        private SessionManager sessionManager;
        private AdvertisementRoot.Google google;

        public AdViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemNativeadImagesBinding.bind(itemView);
        }

        public void setData(int position) {
            Log.d("TAGnnn", "setData:position " + position);
          /*  Log.d("TAGnnn", "setData: " + ads.size());
            Log.d("TAGnnn", "setData: " + ads.isEmpty());*/
            // GGAdview view = (GGAdview) data.get(position);
           /* if (binding.flAdplaceholder.getChildCount() == 0) {
                if (view != null && view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view);
                }
                binding.flAdplaceholder.removeAllViews();
                binding.flAdplaceholder.addView(view);
            }*/

           /* UnifiedNativeAd unifiedNativeAd = (UnifiedNativeAd) data.get(position);
            try {
                if(unifiedNativeAd != null) {
                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) LayoutInflater.from(context)
                                    .inflate(R.layout.native_google_thumb, null);
                    ImageView imageView = adView.findViewById(R.id.ad_media);
                    Glide.with(context).load(unifiedNativeAd.getMediaContent().getMainImage()).centerCrop().into(imageView);


                    // Set other ad assets.
                    adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                    adView.setBodyView(adView.findViewById(R.id.ad_body));
                    adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
                    adView.setIconView(adView.findViewById(R.id.ad_app_icon));
//                    adView.setPriceView(adView.findViewById(R.id.ad_price));
//                    adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
//                    adView.setStoreView(adView.findViewById(R.id.ad_store));
//                    adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

                    // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                    ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());

                    // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                    // check before trying to display them.
                    if(unifiedNativeAd.getBody() == null) {
                        adView.getBodyView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getBodyView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                    }

                    if(unifiedNativeAd.getCallToAction() == null) {
                        adView.getCallToActionView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getCallToActionView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                    }

                    if(unifiedNativeAd.getIcon() == null) {
                        adView.getIconView().setVisibility(View.GONE);
                    } else {
                        ((ImageView) adView.getIconView()).setImageDrawable(
                                unifiedNativeAd.getIcon().getDrawable());
                        adView.getIconView().setVisibility(View.VISIBLE);
                    }

                   *//* if(unifiedNativeAd.getPrice() == null) {
                        adView.getPriceView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getPriceView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                    }

                    if(unifiedNativeAd.getStore() == null) {
                        adView.getStoreView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getStoreView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                    }

                    if(unifiedNativeAd.getStarRating() == null) {
                        adView.getStarRatingView().setVisibility(View.INVISIBLE);
                    } else {
                        ((RatingBar) adView.getStarRatingView())
                                .setRating(unifiedNativeAd.getStarRating().floatValue());
                        adView.getStarRatingView().setVisibility(View.VISIBLE);
                    }

                    if(unifiedNativeAd.getAdvertiser() == null) {
                        adView.getAdvertiserView().setVisibility(View.INVISIBLE);
                    } else {
                        ((TextView) adView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
                        adView.getAdvertiserView().setVisibility(View.VISIBLE);
                    }*//*

                    // This method tells the Google Mobile Ads SDK that you have finished populating your
                    // native ad view with this native ad.
                    adView.setNativeAd(unifiedNativeAd);

                    // Get the video controller for the ad. One will always be provided, even if the ad doesn't
                    // have a video asset.
                    VideoController vc = unifiedNativeAd.getVideoController();

                    // Updates the UI to say whether or not this ad has a video asset.

                    adView.setNativeAd(unifiedNativeAd);
                    binding.flAdplaceholder.removeAllViews();
                    binding.flAdplaceholder.addView(adView);
                    binding.flAdplaceholder.setVisibility(View.VISIBLE);

                   *//* binding.flAdplaceholder.removeAllViews();
                    binding.flAdplaceholder.addView(view);
                    binding.flAdplaceholder.setVisibility(View.VISIBLE);*//*
                } else {
                    Log.d("TAG", "setData: noads");
                    setFacebookNative();
                }

            } catch (Exception o) {
                setFacebookNative();
                Log.d("TAG", "setData: no ads" + o.getMessage());
            }*/
/*
            if (!ads.isEmpty()) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

                if (ads.get(adSetPos) != null && binding.flAdplaceholder.getChildCount() == 0) {

                  */
/*//*
/  binding.flAdplaceholder.addView(ads.get(0),0);
                    GGAdview ggAdview=itemView.findViewById(R.id.ggAdView);
                    ggAdview=ads.get(adSetPos);
                    adSetPos++;
                   // binding.ggAdView=ads.get(0);*//*


                }
            }
*/
           /* sessionManager = new SessionManager(context);
            if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {
                if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                    google = sessionManager.getAdsKeys().getGoogle();

                    GGAdview bannerUnit = new GGAdview(context);
                    //   bannerUnit.setUnitId((google != null) ? google.getJsonMemberNative() : " ");  //Replace with your Ad Unit ID here
                    bannerUnit.setUnitId("AD_UNIT_ID_HERE");
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    binding.flAdplaceholder.removeAllViews();
                    binding.flAdplaceholder.addView(bannerUnit, layoutParams);
                    bannerUnit.loadAd(new AdLoadCallback() {
                                          @Override
                                          public void onReadyForRefresh() {
                                              Log.d("GGADS1", "Ad Ready for refresh");
                                          }

                                          @Override
                                          public void onUiiClosed() {
                                              Log.d("GGADS1", "Uii closed");
                                          }

                                          @Override
                                          public void onUiiOpened() {
                                              Log.d("GGADS1", "Uii Opened");
                                          }

                                          @Override
                                          public void onAdLoadFailed(AdRequestErrors cause) {
                                              Log.d("GGADS1", "Ad Load Failed " + cause);
                                          }

                                          @Override
                                          public void onAdLoaded() {
                                              Log.d("GGADS1", "Ad Loaded");
                                          }
                                      }
                    );

                }
            }*/



          /*  AdLoader adLoader = new AdLoader.Builder(context, "ca-app-pub-3940256099942544/2247696110")
                    .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                        @Override
                        public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                            Log.d("adapte images ", "onUnifiedNativeAdLoaded: loded");
                            UnifiedNativeAdView adView =
                                    (UnifiedNativeAdView) LayoutInflater.from(context)
                                            .inflate(R.layout.native_google_thumb, null);
                            ImageView imageView=adView.findViewById(R.id.ad_media);
                            Glide.with(context).load(unifiedNativeAd.getMediaContent().getMainImage()).centerCrop().into(imageView);


                            // Set other ad assets.
                            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                            adView.setBodyView(adView.findViewById(R.id.ad_body));
                            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
                            adView.setIconView(adView.findViewById(R.id.ad_app_icon));
                            adView.setPriceView(adView.findViewById(R.id.ad_price));
                            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
                            adView.setStoreView(adView.findViewById(R.id.ad_store));
                            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

                            // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                            ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());

                            // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                            // check before trying to display them.
                            if (unifiedNativeAd.getBody() == null) {
                                adView.getBodyView().setVisibility(View.INVISIBLE);
                            } else {
                                adView.getBodyView().setVisibility(View.VISIBLE);
                                ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                            }

                            if (unifiedNativeAd.getCallToAction() == null) {
                                adView.getCallToActionView().setVisibility(View.INVISIBLE);
                            } else {
                                adView.getCallToActionView().setVisibility(View.VISIBLE);
                                ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                            }

                            if (unifiedNativeAd.getIcon() == null) {
                                adView.getIconView().setVisibility(View.GONE);
                            } else {
                                ((ImageView) adView.getIconView()).setImageDrawable(
                                        unifiedNativeAd.getIcon().getDrawable());
                                adView.getIconView().setVisibility(View.VISIBLE);
                            }

                            if (unifiedNativeAd.getPrice() == null) {
                                adView.getPriceView().setVisibility(View.INVISIBLE);
                            } else {
                                adView.getPriceView().setVisibility(View.VISIBLE);
                                ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                            }

                            if (unifiedNativeAd.getStore() == null) {
                                adView.getStoreView().setVisibility(View.INVISIBLE);
                            } else {
                                adView.getStoreView().setVisibility(View.VISIBLE);
                                ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                            }

                            if (unifiedNativeAd.getStarRating() == null) {
                                adView.getStarRatingView().setVisibility(View.INVISIBLE);
                            } else {
                                ((RatingBar) adView.getStarRatingView())
                                        .setRating(unifiedNativeAd.getStarRating().floatValue());
                                adView.getStarRatingView().setVisibility(View.VISIBLE);
                            }

                            if (unifiedNativeAd.getAdvertiser() == null) {
                                adView.getAdvertiserView().setVisibility(View.INVISIBLE);
                            } else {
                                ((TextView) adView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
                                adView.getAdvertiserView().setVisibility(View.VISIBLE);
                            }

                            // This method tells the Google Mobile Ads SDK that you have finished populating your
                            // native ad view with this native ad.
                            adView.setNativeAd(unifiedNativeAd);

                            // Get the video controller for the ad. One will always be provided, even if the ad doesn't
                            // have a video asset.
                            VideoController vc = unifiedNativeAd.getVideoController();

                            // Updates the UI to say whether or not this ad has a video asset.

                            adView.setNativeAd(unifiedNativeAd);
                            binding.flAdplaceholder.removeAllViews();
                            binding.flAdplaceholder.addView(adView);
                            Log.d("MyApp", "onUnifiedNativeAdLoaded: loded");
                            // Show the ad.
                        }
                    })
                    .withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {
                            // Handle the failure by logging, altering the UI, and so on.
                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder()
                            // Methods in the NativeAdOptions.Builder class can be
                            // used here to specify individual options settings.
                            .build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());*/


        }

        private void setFacebookNative() {
            sessionManager = new SessionManager(context);
            AdvertisementRoot.Facebook facebook = sessionManager.getAdsKeys().getFacebook();

            NativeAd nativeAd = new NativeAd(context, (facebook != null) ? facebook.getReward() : " ");
            //NativeAd nativeAd = new NativeAd(context, "VID_HD_16_9_46S_APP_INSTALL#YOUR_PLACEMENT_ID");
            NativeAdListener nativeAdListener = new NativeAdListener() {
                @Override
                public void onMediaDownloaded(Ad ad) {
                    // Native ad finished downloading all assets
                    Log.e("TAG", "Native ad finished downloading all assets.");
                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    // Native ad failed to load
                    Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    if (nativeAd == null || nativeAd != ad) {
                        return;
                    }
                    inflateAd(nativeAd);
                    // Native ad is loaded and ready to be displayed
                    Log.d("TAG", "Native ad is loaded and ready to be displayed!");

                }


                private void inflateAd(NativeAd nativeAd) {
                    Log.d("TAG", "inflateAd: ");
                    binding.nativeAdContainerfb.setVisibility(View.VISIBLE);
                    nativeAd.unregisterView();

                    // Add the Ad view into the ad container.

                    NativeAdLayout nativeAdLayout = binding.nativeAdContainerfb;

                    LayoutInflater inflater = LayoutInflater.from(context);
                    // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                    RelativeLayout adView = (RelativeLayout) inflater.inflate(R.layout.native_ad_layout_1, nativeAdLayout, false);
                    nativeAdLayout.addView(adView);

                    // Add the AdOptionsView
                      /*  LinearLayout adChoicesContainer = itemView.findViewById(R.id.ad_choices_container);
                        AdOptionsView adOptionsView = new AdOptionsView(context, nativeAd, nativeAdLayout);
                        adChoicesContainer.removeAllViews();
                        adChoicesContainer.addView(adOptionsView, 0);*/

                    // Create native UI using the ad metadata.
                    MediaView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
                    TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
                    MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
                    TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
                    TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
                    TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
                    Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

                    // Set the Text.
                    nativeAdTitle.setText(nativeAd.getAdvertiserName());
                    nativeAdBody.setText(nativeAd.getAdBodyText());
                    nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
                    nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                    nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
                    sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                    // Create a list of clickable views
                    List<View> clickableViews = new ArrayList<>();
                    clickableViews.add(nativeAdTitle);
                    clickableViews.add(nativeAdCallToAction);

                    // Register the Title and CTA button to listen for clicks.
                    nativeAd.registerViewForInteraction(
                            adView, nativeAdMedia, nativeAdIcon, clickableViews);
                }


                @Override
                public void onAdClicked(Ad ad) {
                    // Native ad clicked
                    Log.d("TAG", "Native ad clicked!");
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                    // Native ad impression
                    Log.d("TAG", "Native ad impression logged!");
                }
            };

            // Request an ad
            nativeAd.loadAd(
                    nativeAd.buildLoadAdConfig()
                            .withAdListener(nativeAdListener)
                            .build());
        }
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder {
        ItemVideoBinding binding;

        public VideoViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemVideoBinding.bind(itemView);
        }

        public void setData(int position) {

            Random rand = new Random();
            int randInt1 = rand.nextInt((9999 - 999) + 1) + 999;

            double coin;
            if (randInt1 >= 1000) {
                coin = (double) randInt1 / 1000;
                DecimalFormat df = new DecimalFormat("#.##");

                binding.tvCoins.setText(String.valueOf(df.format(coin)).concat("K"));
            } else {
                coin = randInt1;
                binding.tvCoins.setText(String.valueOf(coin));
            }
            Log.d("TAG", "onBindViewHolder: " + randInt1);
            Log.d("TAG", "onBindViewHolder: " + coin);

            ThumbRoot.Datum datum = (ThumbRoot.Datum) data.get(position);
            Glide.with(context.getApplicationContext())
                    .load(new SessionManager(context).getStringValue(Const.IMAGE_URL) + datum.getImage())
                    .into(binding.imgThumb);


            String name = datum.getName().substring(0, 1).toUpperCase().concat(datum.getName().substring(1));
            binding.tvName.setText(name);


            ScaleAnimation btnAnimation = new ScaleAnimation(1f, 0.8f, // Start and end values for the X axis scaling
                    1f, 0.8f,
                    Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                    Animation.RELATIVE_TO_SELF, 0.5f);
            btnAnimation.setDuration(2000); //1 second duration for each animation cycle
            btnAnimation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
            btnAnimation.setFillAfter(true);
            btnAnimation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
            binding.livebtn.startAnimation(btnAnimation);

            try {
                binding.imgThumb.setOnClickListener(v -> context.startActivity(new Intent(context, VideoActivity2.class).putExtra("cid", cid).putExtra("model", new Gson().toJson(datum))));
            } catch (Exception e) {
                Log.d("TAG", "setData: cresh inter " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

/*
    private void showAds() {

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.d(TAG, "onAdLoaded: ");
                mInterstitialAd.show();
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d(TAG, "onAdFailedToLoad: " + adError.toString());
                context.startActivity(new Intent(context, VideoActivity.class).putExtra("cid", cid).putExtra("model", new Gson().toJson(datum)));
//nn
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                context.startActivity(new Intent(context, VideoActivity.class).putExtra("cid", cid).putExtra("model", new Gson().toJson(datum)));
                }

        });

    }
*/

}
