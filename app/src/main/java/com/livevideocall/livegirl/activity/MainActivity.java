package com.livevideocall.livegirl.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.tabs.TabLayout;
import com.livevideocall.livegirl.Anylitecs;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.adapters.CountrtyAdapter;
import com.livevideocall.livegirl.adapters.ViewPagerAdapter;
import com.livevideocall.livegirl.databinding.ActivityMainBinding;
import com.livevideocall.livegirl.databinding.PopUpShowAdsBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.CountryRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;
import com.livevideocall.livegirl.retrofit.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "mainact";
    private static final String WEBSITE = "WEBSITE";
    ActivityMainBinding binding;
    RetrofitService service;
    InterstitialAdListener interstitialAdListener;
    AlertDialog aleart;
    boolean fetched;
    Dialog dialog;
    CountryRoot.Datum tempobj;
    List<CountryRoot.Datum> finelCountry = new ArrayList<>();
    private String adid;
    private com.facebook.ads.InterstitialAd interstitialAdfb;
    private InterstitialAd mInterstitialAd;
    private String ownAdRewardUrl = "";
    private String ownAdBannerUrl = "";
    private String ownAdInstarUrl = "";
    private String ownWebUrl = "";
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Facebook facebook;
    private SessionManager sessionManager;
    private List<CountryRoot.Datum> countries;
    private AdvertisementRoot.Google2 google2;
    private InterstitialAd mInterstitialAd2;
    private boolean ownLoded = false;
//    private GGInterstitialAd mAd;


    public MainActivity() {
    }

    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.chatprimary));
            window.setNavigationBarColor(ContextCompat.getColor(activity, android.R.color.transparent));
        }
    }


    @Override
    protected void onDestroy() {
        Anylitecs.removeSesson(this);
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Anylitecs.removeSesson(this);
        Log.d(TAG, "onPause: ");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setStatusBarGradiant(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        service = RetrofitBuilder.create(this);
        sessionManager = new SessionManager(this);


        initView();
        binding.drawerlayout.setViewScale(GravityCompat.START, 0.9f); //set height scale for main view (0f to 1f)
        binding.drawerlayout.setViewElevation(GravityCompat.START, 30); //set main view elevation when drawer open (dimension)
        binding.drawerlayout.setViewScrimColor(GravityCompat.START, ContextCompat.getColor(this, R.color.colorPrimary)); //set drawer overlay coloe (color)
        binding.drawerlayout.setDrawerElevation(30); //set drawer elevation (dimension)
        //  binding.drawerLayout.setContrastThreshold(0); //set maximum of contrast ratio between white text and background color.
        binding.drawerlayout.setRadius(GravityCompat.START, 25);
        binding.drawerlayout.setViewRotation(GravityCompat.START, 0);
        binding.drawerlayout.closeDrawer(GravityCompat.START);
        binding.drawerlayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                setStatusBarGradiant(MainActivity.this);
            }
        });
        initDrawerListners();


        loadAds();
        Anylitecs.addUser(this);
    }


    private void loadAds() {
        MobileAds.initialize(this, initializationStatus -> {
        });
        AudienceNetworkAds.initialize(this);

        getOwnAds();
        fetched = true;
        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {

            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
                Log.d(TAG, "loadAds: fb show yes");
            }

//            mAd = new GGInterstitialAd(this, (google != null) ? google.getInterstitial() : "");
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId((google != null) ? google.getInterstitial() : "");
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            interAdListnear();

            mInterstitialAd2 = new InterstitialAd(this);
            mInterstitialAd2.setAdUnitId((google2 != null) ? google2.getInterstitial() : "");
            mInterstitialAd2.loadAd(new AdRequest.Builder().build());
            interAdListnear2();

            fbInterAdListnear();
            interstitialAdfb = new com.facebook.ads.InterstitialAd(this, (facebook != null) ? facebook.getInterstitial() : "");
            interstitialAdfb.loadAd(
                    interstitialAdfb.buildLoadAdConfig()
                            .withAdListener(interstitialAdListener)
                            .build());


        } else {
            Toast.makeText(this, "ads not downloded ", Toast.LENGTH_SHORT).show();
        }
    }

    private void interAdListnear2() {
        mInterstitialAd2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
//mm
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                finishAffinity();
                // Code to be executed when the interstitial ad is closed.
            }
        });

    }

    private void initDrawerListners() {
        binding.appbarImgmenu.setOnClickListener(v -> binding.drawerlayout.openDrawer(GravityCompat.START, true));
        binding.navToolbar.navhome.setOnClickListener(v -> binding.drawerlayout.closeDrawer(GravityCompat.START));
        binding.navToolbar.navabout.setOnClickListener(v -> startActivity(new Intent(this, WebActivity.class).putExtra(WEBSITE, "https://codderlab.com/images/about.html").putExtra("title", "About Us")));
        binding.navToolbar.navprivacy.setOnClickListener(v -> startActivity(new Intent(this, WebActivity.class).putExtra(WEBSITE, "https://codderlab.com/images/terms.html").putExtra("title", "Privacy Policy")));
        binding.navToolbar.navProfile.setOnClickListener(v -> startActivity(new Intent(this, ProfileActivity.class)));
        binding.navToolbar.navEarnCoins.setOnClickListener(v -> startActivity(new Intent(this, EarnActivity.class)));
        binding.navToolbar.navshare.setOnClickListener(v -> {
            final String appLink = "\nhttps://play.google.com/store/apps/details?id=" + getPackageName();
            Intent sendInt = new Intent(Intent.ACTION_SEND);
            sendInt.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            sendInt.putExtra(Intent.EXTRA_TEXT, "Humile - live video chat  Download Now  " + appLink);
            sendInt.setType("text/plain");
            startActivity(Intent.createChooser(sendInt, "Share"));
        });
        binding.navToolbar.navmore.setOnClickListener(v -> {
            final String appName = getPackageName();
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
            }
        });
        binding.navToolbar.navrate.setOnClickListener(v -> {
            final String appName = getPackageName();
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
            }
        });
    }

    private void initView() {
        getCountryList();
        // getCountery();

        binding.videobtn.setOnClickListener(v -> {
            //startActivity(new Intent(this,LiveActivity.class));


           /* MyPopup myPopup = new MyPopup(this);
            myPopup.showPopup("Hello Dear, " + sessionManager.getUser().getFname(), "Do you want to live with Us?", "Countinue");
            myPopup.setOnDilogClickListnear(new MyPopup.OnDilogClickListnear() {
                @Override
                public void onDilogClickClose() {
                    myPopup.dismissPopup();
                }

                @Override
                public void onDilogPositiveClick() {
                    Toast.makeText(MainActivity.this, "Your Request has been Sended!!", Toast.LENGTH_SHORT).show();
                    myPopup.dismissPopup();
                }
            });*/
            openPopup();
        });
    }

    private void getCountery() {
        Call<CountryRoot> call = service.getCountries();
        call.enqueue(new Callback<CountryRoot>() {
            @Override
            public void onResponse(Call<CountryRoot> call, Response<CountryRoot> response) {
                if (response.code() == 200) {
                    if (response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                        CountrtyAdapter countrtyAdapter = new CountrtyAdapter(response.body().getData());
                        binding.rvCountry.setAdapter(countrtyAdapter);

                    }
                }
            }

            @Override
            public void onFailure(Call<CountryRoot> call, Throwable t) {

            }
        });
    }

    private void openPopup() {
        dialog = new Dialog(this, R.style.customStyle);
        if (dialog.isShowing()) {
            return;
        }
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        PopUpShowAdsBinding popupbinding = DataBindingUtil.inflate(inflater, R.layout.pop_up_show_ads, null, false);


        dialog.setContentView(popupbinding.getRoot());

       /* GGAdview bannerUnit = new GGAdview(this);
        bannerUnit.setUnitId((google != null) ? google.getBanner() : " ");  //Replace with your Ad Unit ID here
        bannerUnit.setAdsMaxHeight(60); //Value is in pixels, not in dp
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 60);
        popupbinding.flAdplaceholderbanner.removeAllViews();

        popupbinding.flAdplaceholderbanner.addView(bannerUnit);
        bannerUnit.loadAd(new AdLoadCallback() {
                              @Override
                              public void onReadyForRefresh() {
                                  Log.d("GGADS1", "Ad Ready for refresh");
                              }

                              @Override
                              public void onUiiClosed() {
                                  Log.d("GGADS1", "Uii closed");
                              }

                              @Override
                              public void onUiiOpened() {
                                  Log.d("GGADS1", "Uii Opened");
                              }

                              @Override
                              public void onAdLoadFailed(AdRequestErrors cause) {
                                  Log.d("GGADS1", "Ad Load Failed " + cause);
                              }

                              @Override
                              public void onAdLoaded() {
                                  Log.d("GGADS1", "Ad Loaded");
                                  //setNativeLogic();
                                  popupbinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                              }
                          }
        );*/

        AdLoader adLoader = new AdLoader.Builder(this, (google != null) ? google.getBanner() : " ")
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    Log.d(TAG, "showPopup: popup native loded");

                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) getLayoutInflater()
                                    .inflate(R.layout.native_google_popup, null);

                    ImageView imageView = adView.findViewById(R.id.ad_media);
                    Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).circleCrop().transform(new RoundedCorners(20)).into(imageView);
                    // Set other ad assets.
                    adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                    adView.setBodyView(adView.findViewById(R.id.ad_body));
                    adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));

                    adView.setPriceView(adView.findViewById(R.id.ad_price));
                    adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
                    adView.setStoreView(adView.findViewById(R.id.ad_store));
                    adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

                    // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                    ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                    // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                    // check before trying to display them.
                    if (unifiedNativeAd.getBody() == null) {
                        adView.getBodyView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getBodyView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                    }

                    if (unifiedNativeAd.getCallToAction() == null) {
                        adView.getCallToActionView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getCallToActionView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                    }


                    if (unifiedNativeAd.getPrice() == null) {
                        adView.getPriceView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getPriceView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                    }

                    if (unifiedNativeAd.getStore() == null) {
                        adView.getStoreView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getStoreView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                    }

                    if (unifiedNativeAd.getStarRating() == null) {
                        adView.getStarRatingView().setVisibility(View.INVISIBLE);
                    } else {
                        ((RatingBar) adView.getStarRatingView())
                                .setRating(unifiedNativeAd.getStarRating().floatValue());
                        adView.getStarRatingView().setVisibility(View.VISIBLE);
                    }

                    if (unifiedNativeAd.getAdvertiser() == null) {
                        adView.getAdvertiserView().setVisibility(View.INVISIBLE);
                    } else {
                        ((TextView) adView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
                        adView.getAdvertiserView().setVisibility(View.VISIBLE);
                    }


                    adView.setNativeAd(unifiedNativeAd);
                    popupbinding.flAdplaceholderbanner.removeAllViews();
                    popupbinding.flAdplaceholderbanner.addView(adView);


                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());

        if (sessionManager.getStringValue(Const.PROFILE_IMAGE).equals("")) {
            popupbinding.profilechar.setVisibility(View.VISIBLE);
            popupbinding.profilechar.setText(String.valueOf(sessionManager.getUser().getFname().charAt(0)).toUpperCase());
        }

        Glide.with(getApplicationContext())
                .load(sessionManager.getStringValue(Const.PROFILE_IMAGE)).error(R.drawable.bg_whitebtnround)
                .placeholder(R.drawable.bg_whitebtnround)
                .circleCrop()
                .into(popupbinding.imagepopup);
        if (sessionManager.getUser().getFname() != null) {
            popupbinding.tv1.setText("Hello Dear, " + sessionManager.getUser().getFname());
        }
        popupbinding.textview.setText("Do you want to live with Us?");

        popupbinding.tvPositive.setOnClickListener(v -> {


            dialog.dismiss();
            Toast.makeText(MainActivity.this, "Your Request has been Sended!!", Toast.LENGTH_SHORT).show();
        });
        popupbinding.tvCencel.setOnClickListener(v -> dialog.dismiss());


        dialog.show();


    }

    private void getCountryList() {
        Call<CountryRoot> call = service.getCountries();
        call.enqueue(new Callback<CountryRoot>() {
            @Override
            public void onResponse(Call<CountryRoot> call, Response<CountryRoot> response) {
                if (response.code() == 200 && !response.body().getData().isEmpty()) {
                    countries = response.body().getData();
                    for (int i = 0; i < countries.size(); i++) {
                        if (Boolean.TRUE.equals(countries.get(i).getIsTop())) {
                            tempobj = countries.get(i);
                            Log.d(TAG, "onResponse: temp " + tempobj.get_id());
                            Log.d(TAG, "onResponse: temp " + tempobj.getName());
                        } else {
                            finelCountry.add(countries.get(i));
                            Log.d(TAG, "onResponse: obg" + countries.get(i).getName());
                        }
                    }
                    if (tempobj != null) {
                        finelCountry.add(0, tempobj);
                    }


                    ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), finelCountry, getAdSize());
                    binding.viewPager.setAdapter(viewPagerAdapter);
                    binding.tablayout1.setupWithViewPager(binding.viewPager);


                    settabLayout(finelCountry);

                    binding.tablayout1.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            View view = tab.getCustomView();
                            TextView selectedText = (TextView) view.findViewById(R.id.tvTab);
                            selectedText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.black));
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {
                            View view = tab.getCustomView();
                            TextView selectedText = (TextView) view.findViewById(R.id.tvTab);
                            selectedText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white));
                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {
//ll
                        }
                    });


                }
            }

            @Override
            public void onFailure(Call<CountryRoot> call, Throwable t) {
//ll
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.drawerlayout.closeDrawer(GravityCompat.START);
        Anylitecs.addUser(this);


    }

    private void settabLayout(List<CountryRoot.Datum> countries) {
        binding.tablayout1.setTabGravity(TabLayout.GRAVITY_FILL);
        binding.tablayout1.removeAllTabs();
        for (int i = 0; i < countries.size(); i++) {

            binding.tablayout1.addTab(binding.tablayout1.newTab().setCustomView(createCustomView(this.finelCountry.get(i), i)));

        }
    }


    private View createCustomView(CountryRoot.Datum datum, int pos) {
        Log.d(TAG, "settabLayout: " + datum.getName());
        Log.d(TAG, "settabLayout: " + datum.getLogo());
        View v = LayoutInflater.from(this).inflate(R.layout.custom_tabhorizontol, null);
        TextView tv = (TextView) v.findViewById(R.id.tvTab);
        tv.setText(datum.getName());
        ImageView img = (ImageView) v.findViewById(R.id.imagetab);

        if (pos == 0) {
            tv.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.black));

        }
        Glide.with(getApplicationContext())
                .load(new SessionManager(MainActivity.this).getStringValue(Const.IMAGE_URL) + datum.getLogo()).error(R.drawable.coins)
                .placeholder(R.drawable.ic_gift)
                .circleCrop()
                .into(img);
        return v;

    }


    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }


    public void onClickProfile(View view) {
        startActivity(new Intent(this, ProfileActivity.class));
    }

    private void getOwnAds() {
        RetrofitBuilder.create(this).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                    ownLoded = true;
                    OwnAdsRoot.DataItem ownAds = response.body().getData().get(0);

                    //TODO istastial


                }
            }

            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {

///mm
            }
        });
    }


    private void interAdListnear() {
       /* mAd.loadAd(new GGInterstitialEventsListener() {
                       @Override
                       public void onAdLoaded() {
                           Log.d("GGADS", "Ad Loaded");
                       }

                       @Override
                       public void onAdLeftApplication() {
                           Log.d("GGADS", "Ad Left Application");
                       }

                       @Override
                       public void onAdClosed() {
                           Log.d("GGADS", "Ad Closed");
                           finish();
                           //Interstitial ad will be automatically refreshed by SDKX when     closed
                       }

                       @Override
                       public void onAdOpened() {
                           Log.d("GGADS", "Ad Opened");
                       }

                       @Override
                       public void onAdLoadFailed(AdRequestErrors cause) {
                           Log.d("GGADS", "Ad Load Failed " + cause);
                       }
                   }
        );*/

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
//mm
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                finishAffinity();
                // Code to be executed when the interstitial ad is closed.
            }
        });


    }

    private void fbInterAdListnear() {
        interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                finishAffinity();
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {


                // Interstitial ad is loaded and ready to be displayed
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                // Show the ad
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        };
    }


    @Override
    public void onBackPressed() {

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else {
            finish();
        }


    }


}