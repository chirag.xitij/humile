package com.livevideocall.livegirl.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.livevideocall.livegirl.Anylitecs;
import com.livevideocall.livegirl.BuildConfig;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.databinding.ActivitySpleshBinding;
import com.livevideocall.livegirl.forceupdate.ForceUpdateAsync;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.models.SettingsRoot;
import com.livevideocall.livegirl.models.ThumbRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import io.github.erehmi.countdown.CountDownTask;
import io.github.erehmi.countdown.CountDownTimers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.setAutoLogAppEventsEnabled;

public class SpleshActivity extends AppCompatActivity {
    private static final String TAG = "spppp";
    SessionManager sessionManager;
    boolean adloded = false;
    boolean ownAdLoded = false;

    boolean settingLoded = false;
    ActivitySpleshBinding binding;
    private boolean finish = false;
    private String sabNam;
    private AlertDialog aleart;
    private boolean fetched = false;
    private boolean isnotification;
    private String countryid;
    private AdvertisementRoot.Google google;
    private ThumbRoot.Datum datum;
    private AdvertisementRoot.Facebook facebook;
    private AdvertisementRoot.Google2 google2;
    private boolean ownLoded = false;
    private String ownadId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        binding = DataBindingUtil.setContentView(this, R.layout.activity_splesh);
        startcountdown();
        sessionManager = new SessionManager
                (this);
        FacebookSdk.setIsDebugEnabled(true);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
        setAutoLogAppEventsEnabled(true);
        FacebookSdk.setAutoInitEnabled(true);
        FacebookSdk.fullyInitialize();

        sessionManager.saveStringValue(Const.BASE_URL, BuildConfig.TMDB_API_KEY);
        sessionManager.saveStringValue(Const.IMAGE_URL, BuildConfig.TMDB_API_KEY);


        MobileAds.initialize(this, initializationStatus -> {
        });

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String countryCode = tm.getSimCountryIso();
        Log.d("TAG", "onCreate: ttt " + countryCode);

        String localeCountry = tm.getSimCountryIso();
        Log.d("clll", "onCreate: " + localeCountry);
        Locale loc = new Locale("en", localeCountry);
        Log.d("TAG", "onCreate: finel " + loc.getDisplayCountry());
        Log.d("TAG", "onCreate: finel " + loc.getCountry());
        Log.d("TAG", "onCreate: finel  eee  " + loc.getDisplayCountry(loc));
        sessionManager.saveStringValue(Const.Country, loc.getDisplayCountry(loc));


        FirebaseMessaging.getInstance().subscribeToTopic("Users")
                .addOnCompleteListener(task -> Log.d("nottttt", "onComplete: done" + task.toString()));
        FirebaseMessaging.getInstance().subscribeToTopic("coma")
                .addOnCompleteListener(task -> Log.d("nottttt", "onComplete: done" + task.toString()));
        FirebaseMessaging.getInstance().subscribeToTopic("test")
                .addOnCompleteListener(task -> Log.d("nottttt", "onComplete: done" + task.toString()));
        FirebaseMessaging.getInstance().subscribeToTopic("akbari")
                .addOnCompleteListener(task -> Log.d("nottttt", "onComplete: done" + task.toString()));


        Intent intent = getIntent();
        if (intent != null) {
            Bundle b = intent.getExtras();
            if (b != null) {
                Set<String> keys = b.keySet();
                for (String key : keys) {

                    String cid = String.valueOf(getIntent().getExtras().get("countryid"));
                    String thumb = String.valueOf(getIntent().getExtras().get("thumb"));
                    String name = String.valueOf(getIntent().getExtras().get("name"));
                    Log.d("notificationData", "Bundle Contains: key=" + cid);
                    Log.d("notificationData", "Bundle Contains: key=" + thumb);
                    Log.d("notificationData", "Bundle Contains: key=" + name);
                    if (cid != null && !cid.equals("") && !cid.equals("null")) {
                        isnotification = true;
                        countryid = cid;
                        datum = new ThumbRoot.Datum();
                        datum.setImage(thumb);
                        datum.setName(name);
                    } else {
                        isnotification = false;
                    }

                }
            } else {
                Log.w("notificationData", "onCreate: BUNDLE is null");
            }
        } else {
            Log.w("notificationData", "onCreate: INTENT is null");
        }
        getCredential();
        getAdsData();
        getOwnAds();

        // forceUpdate();
    }

    private void startcountdown() {
        CountDownTask countDownTask = CountDownTask.create();
        long targetMillis = CountDownTask.elapsedRealtime() + 1000 * 5;

        final int CD_INTERVAL = 1000;

        countDownTask.until(binding.tvTime, targetMillis, CD_INTERVAL, new CountDownTimers.OnCountDownListener() {
            @Override
            public void onTick(View view, long millisUntilFinished) {
                ((TextView) view).setText(String.valueOf(millisUntilFinished / CD_INTERVAL));
            }

            @Override
            public void onFinish(View view) {
                binding.ivArrow.setVisibility(View.VISIBLE);
                binding.tvTime.setVisibility(View.GONE);
            }
        });
    }

    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion, this).execute();
    }

    private void getCredential() {
        sessionManager.saveStringValue(Const.REWARD_COINS, "200");
        sessionManager.saveStringValue(Const.REWARD_COINS, "200");

        Call<SettingsRoot> call = RetrofitBuilder.create(this).getSettings();
        call.enqueue(new Callback<SettingsRoot>() {
            @Override
            public void onResponse(Call<SettingsRoot> call, Response<SettingsRoot> response) {
                if (response.code() == 200) {
                    if (response != null) {
                        if (response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                            if (response.body().getData().get(0).getSecond() != null) {
                                sessionManager.saveStringValue(Const.ADS_TIME, String.valueOf(response.body().getData().get(0).getSecond()));
                            }
                            if (response.body().getData().get(0).getVideoBonus() != null) {
                                sessionManager.saveStringValue(Const.REWARD_COINS, String.valueOf(response.body().getData().get(0).getVideoBonus()));
                            }

                        }
                        settingLoded = true;
                        nextActivity();
                    }

                }
            }

            @Override
            public void onFailure(Call<SettingsRoot> call, Throwable t) {
//ll
            }
        });
    }

    private void getOwnAds() {
        Call<OwnAdsRoot> call = RetrofitBuilder.create(this).getOwnAds();
        call.enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response != null) {
                    if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                        ownLoded = true;
                        OwnAdsRoot.DataItem ownAds = response.body().getData().get(0);
                        ownadId = ownAds.getId();

                        String[] c1 = ownAds.getColor().split("f");
                        String color = c1[0];
                        Log.d(TAG, "onResponse: " + color);

                        binding.itemNative.adHeadline.setText(ownAds.getName());
                        Log.d(TAG, "onResponse: color " + ownAds.getColor());
                        Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getImage()).centerCrop().into(binding.itemNative.adMedia);
                        Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemNative.tvLogo);

                        binding.itemNative.ivColor.getBackground().setColorFilter((Color.parseColor(color)), PorterDuff.Mode.SRC_ATOP);

                        binding.itemNative.adCallToAction.setText(ownAds.getBtnText());
                        binding.itemNative.adCallToAction.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Anylitecs.sendhit(SpleshActivity.this, ownadId);
                                String url = ownAds.getWebsite();
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(url));
                                startActivity(i);
                            }
                        });
                        binding.itemNative.adBody.setText(ownAds.getDescription());

                    }
                }
            }


            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {
                binding.itemNative.llAdon.setVisibility(View.GONE);

            }
        });
    }

    private void getAdsData() {
        Call<AdvertisementRoot> call = RetrofitBuilder.create(this).getAdvertisement();
        call.enqueue(new Callback<AdvertisementRoot>() {
            @Override
            public void onResponse(Call<AdvertisementRoot> call, Response<AdvertisementRoot> response) {
                if (response != null) {
                    if (response.code() == 200 && response.body().getStatus() == 200 && response.body().getGoogle() != null && response.body().getFacebook() != null) {


                        sessionManager.saveAds(response.body());
                        sessionManager.saveBooleanValue(Const.ADS_Downloded, true);

                        adloded = true;
                        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {

                            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                                google = sessionManager.getAdsKeys().getGoogle();

                            }
                            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                                google2 = sessionManager.getAdsKeys().getGoogle2();

                            }
                            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                                facebook = sessionManager.getAdsKeys().getFacebook();
                            }
                            showNative();
                            /* openfacebookads();*/
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<AdvertisementRoot> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
//ll
            }
        });
    }

    private void nextActivity() {
        new Handler().postDelayed(() -> {
            binding.btnGotoApp.setOnClickListener(v -> {
                if (!finish) {
                    if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {

                        if (isnotification) {
                            isnotification = false;
                            if (countryid != null && !countryid.equals("") && datum != null) {

                                startActivity(new Intent(this, VideoActivity2.class).putExtra("cid", countryid)
                                        .putExtra("model", new Gson().toJson(datum)));
                            } else {
                                Intent i = new Intent(SpleshActivity.this, MainActivity.class);
                                startActivity(i);
                                finish();

                            }
                        } else {
                            Intent i = new Intent(SpleshActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();

                        }
                    } else {
                        Intent i = new Intent(SpleshActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                                /*new Handler().postDelayed(() -> {
                                    Intent i = new Intent(SpleshActivity.this, LoginActivity.class);
                                    startActivity(i);
                                    finish();
                                }, 1000);*/

                    }
                    finish = true;
                }
            });


        }, 3000);
    }

    private void showNative() {
//        Log.d(TAG, "showNative: "+ google.getJsonMemberNative());
        AdLoader adLoader = new AdLoader.Builder(this, (google != null) ? google.getJsonMemberNative() : "")
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    Log.d(TAG, "showPopup: popup native loded");

                    binding.flAdplaceholder.setVisibility(View.VISIBLE);
                    binding.btnGotoApp.setVisibility(View.VISIBLE);

                    /*new Handler().postDelayed(() -> {
                        // This method will be executed once the timer is over
                        binding.btnGotoApp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(SpleshActivity.this, MainActivity.class);
                                startActivity(i);
                            }
                        });


                    }, 5000);*/
                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) getLayoutInflater()
                                    .inflate(R.layout.splace_native_google, null);

                    ImageView imageView = adView.findViewById(R.id.ad_media);
                    Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).into(imageView);
                    // Set other ad assets.
                    adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                    adView.setBodyView(adView.findViewById(R.id.ad_body));
                    adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));

                    /*adView.setPriceView(adView.findViewById(R.id.ad_price));
                    adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
                    adView.setStoreView(adView.findViewById(R.id.ad_store));
                    adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));*/

                    // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                    ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                    // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                    // check before trying to display them.
                    if (unifiedNativeAd.getBody() == null) {
                        adView.getBodyView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getBodyView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                    }

                    if (unifiedNativeAd.getCallToAction() == null) {
                        adView.getCallToActionView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getCallToActionView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                    }


                   /* if (unifiedNativeAd.getPrice() == null) {
                        adView.getPriceView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getPriceView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                    }

                    if (unifiedNativeAd.getStore() == null) {
                        adView.getStoreView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getStoreView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                    }

                    if (unifiedNativeAd.getStarRating() == null) {
                        adView.getStarRatingView().setVisibility(View.INVISIBLE);
                    } else {
                        ((RatingBar) adView.getStarRatingView())
                                .setRating(unifiedNativeAd.getStarRating().floatValue());
                        adView.getStarRatingView().setVisibility(View.VISIBLE);
                    }*/


                    adView.setNativeAd(unifiedNativeAd);
                    binding.flAdplaceholder.removeAllViews();
                    binding.flAdplaceholder.addView(adView);


                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());
                        shownativegoogle2();
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void shownativegoogle2() {

        AdLoader adLoader = new AdLoader.Builder(this, (google2 != null) ? google2.getJsonMemberNative() : "")
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    Log.d(TAG, "showPopup: popup native loded");

                    binding.flAdplaceholder.setVisibility(View.VISIBLE);
                    binding.btnGotoApp.setVisibility(View.VISIBLE);

                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) getLayoutInflater()
                                    .inflate(R.layout.splace_native_google, null);

                    ImageView imageView = adView.findViewById(R.id.ad_media);
                    Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).into(imageView);
                    // Set other ad assets.
                    adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                    adView.setBodyView(adView.findViewById(R.id.ad_body));
                    adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));

                    /*adView.setPriceView(adView.findViewById(R.id.ad_price));
                    adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
                    adView.setStoreView(adView.findViewById(R.id.ad_store));
                    adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));*/

                    // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                    ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                    // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                    // check before trying to display them.
                    if (unifiedNativeAd.getBody() == null) {
                        adView.getBodyView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getBodyView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                    }

                    if (unifiedNativeAd.getCallToAction() == null) {
                        adView.getCallToActionView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getCallToActionView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                    }


                   /* if (unifiedNativeAd.getPrice() == null) {
                        adView.getPriceView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getPriceView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                    }

                    if (unifiedNativeAd.getStore() == null) {
                        adView.getStoreView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getStoreView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                    }

                    if (unifiedNativeAd.getStarRating() == null) {
                        adView.getStarRatingView().setVisibility(View.INVISIBLE);
                    } else {
                        ((RatingBar) adView.getStarRatingView())
                                .setRating(unifiedNativeAd.getStarRating().floatValue());
                        adView.getStarRatingView().setVisibility(View.VISIBLE);
                    }*/


                    adView.setNativeAd(unifiedNativeAd);
                    binding.flAdplaceholder.removeAllViews();
                    binding.flAdplaceholder.addView(adView);


                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());
                        openfacebookads();
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());

    }

    private void openfacebookads() {
        NativeAd nativeAd = new NativeAd(this, (facebook != null) ? facebook.getJsonMemberNative() : " ");
        NativeAdListener nativeAdListener = new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
                Log.e("TAG", "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                if (ownLoded) {
                    binding.itemNative.llAdon.setVisibility(View.VISIBLE);
                    Anylitecs.sendImpression(SpleshActivity.this, ownadId);
                }

                // Native ad failed to load
                Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                inflateAd(nativeAd);
                // Native ad is loaded and ready to be displayed
                Log.d("TAG", "Native ad is loaded and ready to be displayed!");

            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        };

        nativeAd.loadAd(
                nativeAd.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build());
    }

    private void inflateAd(NativeAd nativeAd) {
        binding.nativeAdFb.setVisibility(View.VISIBLE);
        nativeAd.unregisterView();

        // Add the Ad view into the ad container.

        NativeAdLayout nativeAdLayout = binding.nativeAdFb;

        LayoutInflater inflater = LayoutInflater.from(this);
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.facebook_native_splace, nativeAdLayout, false);
        nativeAdLayout.addView(adView);

        // Add the AdOptionsView
                      /*  LinearLayout adChoicesContainer = itemView.findViewById(R.id.ad_choices_container);
                        AdOptionsView adOptionsView = new AdOptionsView(context, nativeAd, nativeAdLayout);
                        adChoicesContainer.removeAllViews();
                        adChoicesContainer.addView(adOptionsView, 0);*/

        // Create native UI using the ad metadata.
        MediaView nativeAdIcon = adView.findViewById(R.id.native_ad_media);
        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
        /*MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);*/
        /* TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);*/
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
        /* TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);*/
        TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

        // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        /*nativeAdSocialContext.setText(nativeAd.getAdSocialContext());*/
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView, nativeAdIcon, clickableViews);
    }
}
