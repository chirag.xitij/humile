package com.livevideocall.livegirl.activity;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.RewardedVideoAd;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.greedygame.core.adview.GGAdview;
import com.livevideocall.livegirl.Anylitecs;
import com.livevideocall.livegirl.BuildConfig;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.adapters.BottomViewPagerAdapter;
import com.livevideocall.livegirl.adapters.CommentAdapter;
import com.livevideocall.livegirl.adapters.EmojiAdapter;
import com.livevideocall.livegirl.databinding.ActivityVideoBinding;
import com.livevideocall.livegirl.databinding.PopUpShowAdsBinding;
import com.livevideocall.livegirl.databinding.PopUpShowOnActivityBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.CommentRoot;
import com.livevideocall.livegirl.models.EmojiIconRoot;
import com.livevideocall.livegirl.models.EmojicategoryRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.models.ThumbRoot;
import com.livevideocall.livegirl.models.UserRoot;
import com.livevideocall.livegirl.models.VideoRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;
import com.livevideocall.livegirl.utils.MyPopup;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import jp.wasabeef.glide.transformations.BlurTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.animation.ValueAnimator.INFINITE;
import static com.google.android.exoplayer2.Player.STATE_BUFFERING;
import static com.google.android.exoplayer2.Player.STATE_ENDED;
import static com.google.android.exoplayer2.Player.STATE_IDLE;
import static com.google.android.exoplayer2.Player.STATE_READY;
import static com.livevideocall.livegirl.activity.EarnActivity.sendImpression;

public class VideoActivity extends AppCompatActivity {

    private static final int SHEET_OPEN = 1;
    private static final int SHEET_CLOSE = 2;
    private static final String TAG = "vidact";
    private static final String WEBSITE = "WEBSITE";
    private final boolean playWhenReady = true;
    private final int currentWindow = 0;
    private final long playbackPosition = 0;
    private final boolean ggnativeloded = false;
    int adsDiffTime = 15;
    ActivityVideoBinding binding;
    BottomSheetBehavior sheetBehavior;
    Animation animZoomIn;
    CommentAdapter commentAdapter;
    Handler handler;
    Runnable runnable;
    SessionManager sessionManager;
    PlayerView playerView;
    String url;
    String path;
    String name;
    int currentPos = 0;
    com.facebook.ads.AdListener adListener;
    AlertDialog aleartdilog;
    boolean cleartext = false;
    List<EmojicategoryRoot.Datum> finelCategories = new ArrayList<>();
    EmojicategoryRoot.Datum tempobj;
    long position = 0;
    boolean paused = false;
    long adPosition = 0;
    boolean isMute = false;
    ProgressDialog progress;
    boolean googleorfbrunning = false;
    boolean flagAds = false;
    boolean videoAdShow = false;
    Random rand = new Random();
    com.facebook.ads.InterstitialAd interstitialAdfb;
    InterstitialAdListener interstitialAdListener;
    private EmojiAdapter emojiAdapter;
    private String videoURL = "";
    private int count = 0;
    private List<CommentRoot.Datum> comments = new ArrayList<>();
    private String countryid;
    private InterstitialAd mInterstitialAd;
    private boolean showAds = false;
    private int time = 0;
    private boolean start = false;
    private SimpleExoPlayer player;
    private String userId;
    private AlertDialog aleart;
    private boolean fetched;
    private String adid;
    private RewardedVideoAd rewardedVideoAd;
    private String ownAdRewardUrl = "";
    private String ownAdBannerUrl = "";
    private String ownAdInstarUrl = "";
    private String ownWebUrl = "";
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Facebook facebook;
    private int rendtemp = 0;
    private Dialog dialog;
    private String thumbImage;
    private boolean ownLoded = false;
    private SimpleExoPlayer adPlayer;
    private List<EmojicategoryRoot.Datum> categories;
    private boolean googleNativeLoded = false;
    //    GGInterstitialAd mAd;
    private GGAdview nativeUnit;
    private InterstitialAd mInterstitialAd2;
    private com.facebook.ads.InterstitialAd interstitialAdfb2;
    private boolean isFirstAdShowed = false;
    private AdvertisementRoot.Google2 google2;
    private InterstitialAd mInterstitialAd3;
    private PopUpShowAdsBinding popupbinding;
    private PopUpShowOnActivityBinding popUpShowOnActivityBinding;

    private void chkConnection2() {
        aleart = new AlertDialog.Builder(VideoActivity.this).
                setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Internet Connection Alert")
                .setMessage("Please Turn on Internet Connection")
                .setPositiveButton("Close", (dialog, which) -> finishAffinity()).create();
        aleart.setCancelable(false);
        final Handler handler = new Handler();
        final Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(() -> {
                    try {
                        if (isConnected()) {
                            aleart.dismiss();
                            if (!fetched) {
                                initMain();
                            }
                        } else {
                            if (!aleart.isShowing()) {
                                fetched = false;
                                aleart.show();
                            }

                        }

                    } catch (Exception e) {
                        //mm
                    }
                });
            }

            private boolean isConnected() {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                return networkInfo != null && networkInfo.isConnected();
            }

        };
        timer.schedule(doAsynchronousTask, 0, 1000);
    }

    private void initMain() {

        int randInt1 = rand.nextInt((600 - 450) + 1) + 450;

        rendtemp = randInt1;

        binding.tvCoin.setText(String.valueOf(rendtemp));
        fetched = true;


        addRandomCoins();
        getOwnAds();

        Intent intent = getIntent();
        if (intent != null) {
            String cid = intent.getStringExtra("cid");
            String model = intent.getStringExtra("model");
            if (model != null && !model.equals("")) {
                ThumbRoot.Datum object = new Gson().fromJson(model, ThumbRoot.Datum.class);
                if (object != null) {
                    Log.d(TAG, "getIntentdata:image " + object.getImage());
                    Log.d(TAG, "getIntentdata:image " + object.getName());
                    thumbImage = object.getImage();
                    String s = String.valueOf(object.getName().charAt(0)).toUpperCase();
                    binding.tvName.setText(s.concat(object.getName().substring(1)));
                }

            }
        }
        initFirstAds();
        initAds();



        /* AlertDialog alertDialog1=new  AlertDialog.Builder(VideoActivity.this).
                 setIcon(android.R.drawable.ic_dialog_alert)
                 .setTitle("showads")
                 .setMessage("showwwww")
                 .setPositiveButton("Close", (dialog, which) ->openFirstAd())
                 .setNegativeButton("NO",(dialog1, which) -> {
                     Toast.makeText(this, "false", Toast.LENGTH_SHORT).show();
                 }).create();
         alertDialog1.show();*/


    }

    private void showAdPopup() {
        Log.d(TAG, "showAdPopup: openpopup");

        if (dialog.isShowing()) {
            time = 0;
            return;
        }
        dialog.setContentView(popUpShowOnActivityBinding.getRoot());
        // popupbinding.tvPositive.setVisibility(View.GONE);

       /* GGAdview bannerUnit = new GGAdview(this);
        bannerUnit.setUnitId((google != null) ? google.getJsonMemberNative() : " ");  //Replace with your Ad Unit ID here
        bannerUnit.setAdsMaxHeight(400);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        popupbinding.flAdplaceholderbanner.removeAllViews();
        popupbinding.flAdplaceholderbanner.addView(bannerUnit, layoutParams);
        bannerUnit.loadAd(new AdLoadCallback() {
                              @Override
                              public void onReadyForRefresh() {
                                  Log.d("GGADS", "Ad Ready for refresh");
                              }

                              @Override
                              public void onUiiClosed() {
                                  Log.d("GGADS", "Uii closed");
                              }

                              @Override
                              public void onUiiOpened() {
                                  Log.d("GGADS", "Uii Opened");
                              }

                              @Override
                              public void onAdLoadFailed(AdRequestErrors cause) {
                                  Log.d("GGADS", "Ad Load Failed " + cause);

                                  popupbinding.tvPositive.setVisibility(View.VISIBLE);

                              }

                              @Override
                              public void onAdLoaded() {
                                  Log.d("GGADS", "Ad Loaded");
                                  popupbinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                                  popupbinding.tvPositive.setVisibility(View.VISIBLE);
                              }
                          }
        );*/


        try {
            AdLoader adLoader = new AdLoader.Builder(this, (google != null) ? google.getJsonMemberNative() : "")
                    .forUnifiedNativeAd(unifiedNativeAd -> {
                        // Show the ad.
                        Log.d(TAG, "showPopup: popup native loded");
                        popUpShowOnActivityBinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                        popUpShowOnActivityBinding.tvYes.setVisibility(View.VISIBLE);
                        popUpShowOnActivityBinding.tvNo.setVisibility(View.VISIBLE);
                        UnifiedNativeAdView adView =
                                (UnifiedNativeAdView) getLayoutInflater()
                                        .inflate(R.layout.native_google_popup, null);

                        ImageView imageView = adView.findViewById(R.id.ad_media);
                        Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).circleCrop().transform(new RoundedCorners(20)).into(imageView);
                        // Set other ad assets.
                        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                        adView.setBodyView(adView.findViewById(R.id.ad_body));
                        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));

                        adView.setPriceView(adView.findViewById(R.id.ad_price));
                        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
                        adView.setStoreView(adView.findViewById(R.id.ad_store));
                        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

                        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                        ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                        // check before trying to display them.
                        if (unifiedNativeAd.getBody() == null) {
                            adView.getBodyView().setVisibility(View.INVISIBLE);
                        } else {
                            adView.getBodyView().setVisibility(View.VISIBLE);
                            ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                        }

                        if (unifiedNativeAd.getCallToAction() == null) {
                            adView.getCallToActionView().setVisibility(View.INVISIBLE);
                        } else {
                            adView.getCallToActionView().setVisibility(View.VISIBLE);
                            ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                        }


                        if (unifiedNativeAd.getPrice() == null) {
                            adView.getPriceView().setVisibility(View.INVISIBLE);
                        } else {
                            adView.getPriceView().setVisibility(View.VISIBLE);
                            ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                        }

                        if (unifiedNativeAd.getStore() == null) {
                            adView.getStoreView().setVisibility(View.INVISIBLE);
                        } else {
                            adView.getStoreView().setVisibility(View.VISIBLE);
                            ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                        }

                        if (unifiedNativeAd.getStarRating() == null) {
                            adView.getStarRatingView().setVisibility(View.INVISIBLE);
                        } else {
                            ((RatingBar) adView.getStarRatingView())
                                    .setRating(unifiedNativeAd.getStarRating().floatValue());
                            adView.getStarRatingView().setVisibility(View.VISIBLE);
                        }


                        adView.setNativeAd(unifiedNativeAd);
                        popUpShowOnActivityBinding.flAdplaceholderbanner.removeAllViews();
                        popUpShowOnActivityBinding.flAdplaceholderbanner.addView(adView);


                    })
                    .withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {
                            showgoogle2native();
                            Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());
                            popUpShowOnActivityBinding.tvYes.setVisibility(View.VISIBLE);
                            popUpShowOnActivityBinding.tvNo.setVisibility(View.VISIBLE);
                            // Handle the failure by logging, altering the UI, and so on.
                        }

                        private void showgoogle2native() {
                            AdLoader adLoader = new AdLoader.Builder(VideoActivity.this, (google2 != null) ? google2.getJsonMemberNative() : "")
                                    .forUnifiedNativeAd(unifiedNativeAd -> {
                                        // Show the ad.
                                        Log.d(TAG, "showPopup: popup native loded");
                                        popUpShowOnActivityBinding.tvYes.setVisibility(View.VISIBLE);
                                        popUpShowOnActivityBinding.tvNo.setVisibility(View.VISIBLE);
                                        popUpShowOnActivityBinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                                        UnifiedNativeAdView adView =
                                                (UnifiedNativeAdView) getLayoutInflater()
                                                        .inflate(R.layout.native_google_popup, null);

                                        ImageView imageView = adView.findViewById(R.id.ad_media);
                                        Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).circleCrop().transform(new RoundedCorners(20)).into(imageView);
                                        // Set other ad assets.
                                        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                                        adView.setBodyView(adView.findViewById(R.id.ad_body));
                                        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));

                                        adView.setPriceView(adView.findViewById(R.id.ad_price));
                                        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
                                        adView.setStoreView(adView.findViewById(R.id.ad_store));
                                        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

                                        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                                        ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                                        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                                        // check before trying to display them.
                                        if (unifiedNativeAd.getBody() == null) {
                                            adView.getBodyView().setVisibility(View.INVISIBLE);
                                        } else {
                                            adView.getBodyView().setVisibility(View.VISIBLE);
                                            ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                                        }

                                        if (unifiedNativeAd.getCallToAction() == null) {
                                            adView.getCallToActionView().setVisibility(View.INVISIBLE);
                                        } else {
                                            adView.getCallToActionView().setVisibility(View.VISIBLE);
                                            ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                                        }


                                        if (unifiedNativeAd.getPrice() == null) {
                                            adView.getPriceView().setVisibility(View.INVISIBLE);
                                        } else {
                                            adView.getPriceView().setVisibility(View.VISIBLE);
                                            ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                                        }

                                        if (unifiedNativeAd.getStore() == null) {
                                            adView.getStoreView().setVisibility(View.INVISIBLE);
                                        } else {
                                            adView.getStoreView().setVisibility(View.VISIBLE);
                                            ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                                        }

                                        if (unifiedNativeAd.getStarRating() == null) {
                                            adView.getStarRatingView().setVisibility(View.INVISIBLE);
                                        } else {
                                            ((RatingBar) adView.getStarRatingView())
                                                    .setRating(unifiedNativeAd.getStarRating().floatValue());
                                            adView.getStarRatingView().setVisibility(View.VISIBLE);
                                        }


                                        adView.setNativeAd(unifiedNativeAd);
                                        popUpShowOnActivityBinding.flAdplaceholderbanner.removeAllViews();
                                        popUpShowOnActivityBinding.flAdplaceholderbanner.addView(adView);

                                    })
                                    .withAdListener(new AdListener() {
                                        @Override
                                        public void onAdFailedToLoad(LoadAdError adError) {
                                            shownativefb();
                                            Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());
                                            popUpShowOnActivityBinding.flAdplaceholderbanner.setVisibility(View.GONE);
                                            popUpShowOnActivityBinding.tvYes.setVisibility(View.VISIBLE);
                                            popUpShowOnActivityBinding.tvNo.setVisibility(View.VISIBLE);
                                            // Handle the failure by logging, altering the UI, and so on.
                                        }

                                        private void shownativefb() {
                                            sessionManager = new SessionManager(VideoActivity.this);
                                            AdvertisementRoot.Facebook facebook = sessionManager.getAdsKeys().getFacebook();

                                            NativeAd nativeAd = new NativeAd(VideoActivity.this, (facebook != null) ? facebook.getJsonMemberNative() : " ");
                                            NativeAdListener nativeAdListener = new NativeAdListener() {
                                                @Override
                                                public void onMediaDownloaded(Ad ad) {
                                                    // Native ad finished downloading all assets
                                                    Log.e("TAG", "Native ad finished downloading all assets.");
                                                }

                                                @Override
                                                public void onError(Ad ad, AdError adError) {
                                                    popUpShowOnActivityBinding.itemBannerownad.lytad.setVisibility(View.VISIBLE);
                                                    popUpShowOnActivityBinding.tvYes.setVisibility(View.VISIBLE);
                                                    popUpShowOnActivityBinding.tvNo.setVisibility(View.VISIBLE);
                                                    popUpShowOnActivityBinding.nativeAdFb.setVisibility(View.GONE);
                                                    // Native ad failed to load
                                                    Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
                                                }

                                                @Override
                                                public void onAdLoaded(Ad ad) {
                                                    if (nativeAd == null || nativeAd != ad) {
                                                        return;
                                                    }
                                                    inflateAd(nativeAd);
                                                    // Native ad is loaded and ready to be displayed
                                                    Log.d("TAG", "Native ad is loaded and ready to be displayed!");

                                                }

                                                private void inflateAd(NativeAd nativeAd) {
                                                    popUpShowOnActivityBinding.nativeAdFb.setVisibility(View.VISIBLE);
                                                    nativeAd.unregisterView();

                                                    // Add the Ad view into the ad container.

                                                    NativeAdLayout nativeAdLayout = popUpShowOnActivityBinding.nativeAdFb;

                                                    LayoutInflater inflater = LayoutInflater.from(VideoActivity.this);
                                                    // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                                                    LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_fb_popup, nativeAdLayout, false);
                                                    nativeAdLayout.addView(adView);

                                                    // Add the AdOptionsView
                      /*  LinearLayout adChoicesContainer = itemView.findViewById(R.id.ad_choices_container);
                        AdOptionsView adOptionsView = new AdOptionsView(context, nativeAd, nativeAdLayout);
                        adChoicesContainer.removeAllViews();
                        adChoicesContainer.addView(adOptionsView, 0);*/

                                                    // Create native UI using the ad metadata.
                                                    com.facebook.ads.MediaView nativeAdIcon = adView.findViewById(R.id.native_ad_media);
                                                    TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
                                                    /*MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);*/
                                                    /* TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);*/
                                                    TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
                                                    /* TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);*/
                                                    TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

                                                    // Set the Text.
                                                    nativeAdTitle.setText(nativeAd.getAdvertiserName());
                                                    nativeAdBody.setText(nativeAd.getAdBodyText());
                                                    /*nativeAdSocialContext.setText(nativeAd.getAdSocialContext());*/
                                                    nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                                                    nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                                                    // Create a list of clickable views
                                                    List<View> clickableViews = new ArrayList<>();
                                                    clickableViews.add(nativeAdTitle);
                                                    clickableViews.add(nativeAdCallToAction);

                                                    // Register the Title and CTA button to listen for clicks.
                                                    nativeAd.registerViewForInteraction(
                                                            adView, nativeAdIcon, clickableViews);
                                                }

                                                @Override
                                                public void onAdClicked(Ad ad) {

                                                }

                                                @Override
                                                public void onLoggingImpression(Ad ad) {

                                                }
                                            };

                                            nativeAd.loadAd(
                                                    nativeAd.buildLoadAdConfig()
                                                            .withAdListener(nativeAdListener)
                                                            .build());

                                        }
                                    })
                                    .withNativeAdOptions(new NativeAdOptions.Builder()
                                            // Methods in the NativeAdOptions.Builder class can be
                                            // used here to specify individual options settings.
                                            .build())
                                    .build();
                            adLoader.loadAd(new AdRequest.Builder().build());
                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder()
                            // Methods in the NativeAdOptions.Builder class can be
                            // used here to specify individual options settings.
                            .build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());
        } catch (Exception o) {
            Log.d(TAG, "showPopup: trycatch " + o.getMessage());
        }

        dialog.setCancelable(false);
        Glide.with(getApplicationContext())
                .load(new SessionManager(VideoActivity.this).getStringValue(Const.IMAGE_URL) + thumbImage)
                .circleCrop()
                .into(popUpShowOnActivityBinding.imagepopup);
        popUpShowOnActivityBinding.tv1.setText("Hello Dear, " + sessionManager.getUser().getFname());
        popUpShowOnActivityBinding.textview.setText("To countinue with " + binding.tvName.getText().toString());

        popUpShowOnActivityBinding.tvNo.setOnClickListener(v -> {
            showAds();
            finish();
        });
        popUpShowOnActivityBinding.tvYes.setOnClickListener(v -> {
            showAds();
            openFirstAd();
        });
        popUpShowOnActivityBinding.tvCencel.setOnClickListener(v -> {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            showAds();
            finish();
        });

        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }

    }

    private void initFirstAds() {
        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {

            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
            }

            // initNativeGoogle();
//            mAd = new GGInterstitialAd(this, (google != null) ? google.getInterstitial() : "");
            //  mAd = new GGInterstitialAd( this,"AD_UNIT_ID_HERE");
            mInterstitialAd2 = new InterstitialAd(this);
            mInterstitialAd2.setAdUnitId((google != null) ? google.getInterstitial() : "");
            mInterstitialAd2.loadAd(new AdRequest.Builder().build());
            mInterstitialAd2.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    // Code to be executed when an ad finishes loading.
                    showAdPopup();

                }

                @Override
                public void onAdFailedToLoad(LoadAdError adError) {
                    Log.d(TAG, "onAdFailedToLoad: " + adError.toString());
                    InterstitialAdListener interstitialAdListener2 = new InterstitialAdListener() {
                        @Override
                        public void onInterstitialDisplayed(Ad ad) {
                            // Interstitial ad displayed callback
                            Log.e(TAG, "Interstitial ad displayed.");


                        }

                        @Override
                        public void onInterstitialDismissed(Ad ad) {

                            initView();
                            initListnear();

                        }

                        @Override
                        public void onError(Ad ad, AdError adError) {
                            // Ad error callback
                            Log.e(TAG, "Interstitial ad failed to load:aa " + adError.getErrorMessage());
                            initView();
                            initListnear();
                        }

                        @Override
                        public void onAdLoaded(Ad ad) {
                            showAdPopup();

                            Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");

                        }

                        @Override
                        public void onAdClicked(Ad ad) {
                            // Ad clicked callback
                            Log.d(TAG, "Interstitial ad clicked!");
                        }

                        @Override
                        public void onLoggingImpression(Ad ad) {
                            // Ad impression logged callback
                            Log.d(TAG, "Interstitial ad impression logged!");
                        }
                    };

                    interstitialAdfb2 = new com.facebook.ads.InterstitialAd(VideoActivity.this, (facebook != null) ? facebook.getInterstitial() : "");
                    interstitialAdfb2.loadAd(
                            interstitialAdfb2.buildLoadAdConfig()
                                    .withAdListener(interstitialAdListener2)
                                    .build());


//nn
                }

                @Override
                public void onAdOpened() {
                    // Code to be executed when the ad is displayed.


                }

                @Override
                public void onAdClicked() {
                    // Code to be executed when the user clicks on an ad.
                }

                @Override
                public void onAdLeftApplication() {
                    // Code to be executed when the user has left the app.
                }

                @Override
                public void onAdClosed() {
                    initView();
                    initListnear();
                    // Code to be executed when the interstitial ad is closed.
                }
            });


        }
    }

    private void openFirstAd() {
        isFirstAdShowed = true;
        if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
            initAds();
        } else if (mInterstitialAd3.isLoaded()) {
            mInterstitialAd3.show();
            initAds();
        } else if (interstitialAdfb2.isAdLoaded()) {
            interstitialAdfb2.show();
            initAds();
        } else {
            isFirstAdShowed = false;
        }


    }

    private void initAds() {

        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {

            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
            }

            // initNativeGoogle();
//            mAd = new GGInterstitialAd(this, (google != null) ? google.getInterstitial() : "");
            //  mAd = new GGInterstitialAd( this,"AD_UNIT_ID_HERE");
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId((google != null) ? google.getInterstitial() : "");
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            interAdListnear();

            mInterstitialAd3 = new InterstitialAd(this);
            mInterstitialAd3.setAdUnitId((google2 != null) ? google2.getInterstitial() : "");
            mInterstitialAd3.loadAd(new AdRequest.Builder().build());
            interAdListnear2();

            fbInterAdListnear();
            interstitialAdfb = new com.facebook.ads.InterstitialAd(this, (facebook != null) ? facebook.getInterstitial() : "");
            interstitialAdfb.loadAd(
                    interstitialAdfb.buildLoadAdConfig()
                            .withAdListener(interstitialAdListener)
                            .build());


        }


    }

    private void interAdListnear2() {
        mInterstitialAd3.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d(TAG, "onAdFailedToLoad: " + adError.toString());
//nn
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
                mInterstitialAd3.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "onAdClosed g in : " + googleorfbrunning);
                if (googleorfbrunning) {
                    flagAds = false;


                    googleorfbrunning = false;
                    showAds = false;
                    start = true;
                    time = 0;

                    if (player != null && player.getPlaybackState() != STATE_IDLE) {
                        player.seekTo(position);
                        player.setPlayWhenReady(true);
                    } else {
                        setvideoURL();
                        if (player != null && player.getPlaybackState() != STATE_IDLE) {
                            player.seekTo(position);
                            player.setPlayWhenReady(true);
                        }
                    }

                    dialog.dismiss();
                    time = 0;


                } else {
                    Log.d(TAG, "onAdClosed: finish" + googleorfbrunning);
                    if (player != null) {
                        player.release();
                    }
                    finish();
                }
                // Code to be executed when the interstitial ad is closed.
            }
        });
    }

    private void initNativeGoogle() {

      /*  GGAdview ggAdView = new GGAdview(this);
        //  ggAdView.setUnitId((google != null) ? google.getJsonMemberNative() : "");  //Replace with your Ad Unit ID here
        // ggAdView.setUnitId("AD_UNIT_ID_HERE");  //Replace with your Ad Unit ID here
        //Value is in pixels, not in dp

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        binding.lytNative.flAdplaceholder.removeAllViews();
        binding.lytNative.flAdplaceholder.addView(ggAdView, layoutParams);


        ggAdView.loadAd(new AdLoadCallback() {
            @Override
            public void onReadyForRefresh() {
                                Log.d("GGADS", "Ad Ready for refresh");
                            }

                            @Override
                            public void onUiiClosed() {
                                Log.d("GGADS", "Uii closed");
                            }

                            @Override
                            public void onUiiOpened() {
                                Log.d("GGADS", "Uii Opened");
                            }

                            @Override
                            public void onAdLoadFailed(AdRequestErrors cause) {
                                Log.d("GGADS", "Ad Load Failed " + cause);
                            }

                            @Override
                            public void onAdLoaded() {
                                Log.d("GGADS", "Ad Loaded");
                                googleNativeLoded = true;
                            }
                        }
        );*/


        AdLoader adLoader = new AdLoader.Builder(this, google.getJsonMemberNative())
                .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        googleNativeLoded = true;
                        UnifiedNativeAdView adView =
                                (UnifiedNativeAdView) getLayoutInflater()
                                        .inflate(R.layout.native_google, null);
                        populateUnifiedNativeAdView(unifiedNativeAd, adView);
                        adView.setNativeAd(unifiedNativeAd);
                        binding.lytNative.flAdplaceholder.removeAllViews();
                        binding.lytNative.flAdplaceholder.addView(adView);
                        Log.d("MyApp", "onUnifiedNativeAdLoaded: loded");
                        // Show the ad.
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void addRandomCoins() {

        Handler handler2 = new Handler();
        Runnable runnable2 = new Runnable() {

            @Override
            public void run() {
                try {
                    int randInt1 = rand.nextInt((50 - 10) + 1) + 10;

                    rendtemp = rendtemp + randInt1;


                    double coin1;
                    if (rendtemp >= 1000) {
                        coin1 = (double) rendtemp / 1000;
                        DecimalFormat df = new DecimalFormat("#.##");

                        binding.tvCoin.setText(df.format(coin1).concat("K"));
                    } else {
                        coin1 = rendtemp;
                        binding.tvCoin.setText(String.valueOf((int) coin1));
                    }
                    Log.d(TAG, "run: coin " + coin1);
                    Log.d(TAG, "run: rendm  " + rendtemp);
                    handler2.postDelayed(this, 5000);

                } catch (IllegalStateException ed) {
                    ed.printStackTrace();
                }
            }
        };

        handler2.postDelayed(runnable2, 5000);

    }


    private void getOwnAds() {
        RetrofitBuilder.create(this).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                    ownLoded = true;
                    OwnAdsRoot.DataItem ownAds = response.body().getData().get(0);


                    popupbinding.itemBannerownad.tvtitle.setText(ownAds.getName());
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(popupbinding.itemBannerownad.imglogo);
                    popupbinding.itemBannerownad.tvbtn.setText(ownAds.getBtnText());
                    popupbinding.itemBannerownad.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    popupbinding.itemBannerownad.tvdes.setText(ownAds.getDescription());
                    popupbinding.itemBannerownad.lytad.setVisibility(View.VISIBLE);


                    popUpShowOnActivityBinding.itemBannerownad.tvtitle.setText(ownAds.getName());
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(popUpShowOnActivityBinding.itemBannerownad.imglogo);
                    popUpShowOnActivityBinding.itemBannerownad.tvbtn.setText(ownAds.getBtnText());
                    popUpShowOnActivityBinding.itemBannerownad.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    popUpShowOnActivityBinding.itemBannerownad.tvdes.setText(ownAds.getDescription());
                    popUpShowOnActivityBinding.itemBannerownad.lytad.setVisibility(View.VISIBLE);


                }
            }

            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {

//nn
            }
        });
    }

    private void setOwnAds() {
        Log.d(TAG, "setOwnAds: videoad");

        binding.lytOwnAds.setVisibility(View.VISIBLE);
        binding.adVideoview.setVisibility(View.VISIBLE);
        binding.videoView.setVisibility(View.GONE);
        sendImpression(this, adid);

        adPlayer = new SimpleExoPlayer.Builder(this).build();
        binding.adVideoview.setPlayer(adPlayer);
        binding.adVideoview.setShowBuffering(true);
        Uri uri = Uri.parse(new SessionManager(VideoActivity.this).getStringValue(Const.IMAGE_URL) + ownAdRewardUrl);
        MediaSource mediaSource = buildMediaSource(uri);
        Log.d("TAG", "initializePlayer:videoad " + uri);
        adPlayer.setPlayWhenReady(true);
        adPlayer.seekTo(currentWindow, playbackPosition);
        adPlayer.prepare(mediaSource, false, false);
        binding.tvTime.setVisibility(View.GONE);
        binding.btnClose.setVisibility(View.GONE);
        videoAdShow = true;
        Log.d(TAG, "setOwnAds: videoad" + ownAdRewardUrl);
        adPlayer.addListener(new Player.EventListener() {

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case STATE_BUFFERING:
                        binding.adVideoview.setVisibility(View.VISIBLE);
                        videoAdShow = true;
                        Log.d(TAG, "buffer:videoad " + uri);
                        binding.tvTime.setVisibility(View.VISIBLE);
                        binding.btnClose.setVisibility(View.GONE);
                        break;
                    case STATE_ENDED:
                        videoAdShow = false;
                        binding.btnClose.setVisibility(View.VISIBLE);
                        flagAds = false;

                        showAds = false;
                        binding.lytOwnAds.setVisibility(View.GONE);
                        binding.videoView.setVisibility(View.VISIBLE);
                        if (adPlayer != null) {
                            adPlayer.release();
                            adPlayer = null;
                        }
                        if (player != null) {
                            Log.d(TAG, "onPlayerStateChanged: player ready main videoad");
                            player.setPlayWhenReady(true);
                        }
                        dialog.dismiss();
                        start = true;
                        binding.tvTime.setVisibility(View.GONE);
                        binding.btnClose.setVisibility(View.VISIBLE);
                        Log.d(TAG, "end:videoad " + uri);
                        break;
                    case STATE_IDLE:
                        Log.d(TAG, "idle:videoad " + uri);
                        if (adPlayer != null) {
                            adPlayer.release();
                            adPlayer = null;
                        }

                        break;

                    case STATE_READY:
                        new CountDownTimer(15000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                videoAdShow = true;
                                binding.tvTime.setVisibility(View.GONE);
                                binding.btnClose.setVisibility(View.GONE);
                                binding.btnVolume.setOnClickListener(v -> {
                                    if (isMute) {
                                        binding.btnVolume.setImageDrawable(ContextCompat.getDrawable(VideoActivity.this, R.drawable.ic_round_volume_down_24));
                                        adPlayer.setVolume(1000);
                                        isMute = false;
                                    } else {
                                        adPlayer.setVolume(0);
                                        binding.btnVolume.setImageDrawable(ContextCompat.getDrawable(VideoActivity.this, R.drawable.ic_round_volume_off_24));
                                        isMute = true;
                                    }
                                });
                            }

                            @Override
                            public void onFinish() {

                                Log.d(TAG, "onFinish: videoad");
                                videoAdShow = false;
                                binding.tvTime.setVisibility(View.GONE);
                                binding.btnClose.setVisibility(View.VISIBLE);
                                binding.btnClose.setOnClickListener(v -> closeOwnAds());
                                if (adPlayer != null) {
                                    adPlayer.release();
                                    adPlayer = null;
                                }
                            }
                        }.start();


                        Log.d(TAG, "ready:videoad " + uri);

                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                MyPopup myPopup = new MyPopup(VideoActivity.this);
                myPopup.showPopup("Hello Dear, " + sessionManager.getUser().getFname(), "Live video is Ended", "Countinue");
                myPopup.setOnDilogClickListnear(new MyPopup.OnDilogClickListnear() {
                    @Override
                    public void onDilogClickClose() {
                        myPopup.dismissPopup();
                        finish();
                    }

                    @Override
                    public void onDilogPositiveClick() {

                        myPopup.dismissPopup();
                        finish();
                    }
                });

                videoAdShow = false;
                Log.d(TAG, "onError: videoad " + error.toString());
                if (adPlayer != null) {
                    adPlayer.release();
                    adPlayer = null;
                }
                binding.lytOwnAds.setVisibility(View.GONE);
                binding.videoView.setVisibility(View.VISIBLE);
                binding.tvTime.setVisibility(View.GONE);
                binding.btnClose.setVisibility(View.VISIBLE);
                showAds = false;

                if (player != null) {
                    player.setPlayWhenReady(true);
                }
                dialog.dismiss();
                start = true;
                getOwnAds();
            }
        });


        binding.adVideoview.setOnClickListener(v -> {
            if (adPlayer != null) {
                adPlayer.release();
                adPlayer = null;
            }
            Intent intent = new Intent(VideoActivity.this, WebActivity.class);
            intent.putExtra("ADID", String.valueOf(adid));
            intent.putExtra(WEBSITE, ownWebUrl);
            intent.putExtra("type", "ads");
            startActivity(intent);
        });
        binding.lytOwnAds.setOnClickListener(v -> {
            if (adPlayer != null) {
                adPlayer.release();
                adPlayer = null;
            }
            Intent intent = new Intent(VideoActivity.this, WebActivity.class);
            intent.putExtra("ADID", String.valueOf(adid));
            intent.putExtra(WEBSITE, ownWebUrl);
            intent.putExtra("type", "ads");
            startActivity(intent);
        });

    }

    private void closeOwnAds() {
        videoAdShow = false;
        binding.tvTime.setVisibility(View.GONE);
        binding.btnClose.setVisibility(View.VISIBLE);
        Log.d(TAG, "closeOwnAds: videoad");
        binding.lytOwnAds.setVisibility(View.GONE);
        binding.videoView.setVisibility(View.VISIBLE);
        showAds = false;
        start = true;
        time = 0;
        if (adPlayer != null) {
            adPlayer.release();
            adPlayer = null;
        }
        if (player != null) {
            player.seekTo(position);
            player.setPlayWhenReady(true);
        }

        dialog.dismiss();
        time = 0;


        getOwnAds();


    }

    private void getIntentdata() {
        Intent intent = getIntent();
        if (intent != null) {
            String cid = intent.getStringExtra("cid");
            String model = intent.getStringExtra("model");
            if (model != null && !model.equals("")) {
                binding.lytthumb.setVisibility(View.VISIBLE);
                ThumbRoot.Datum object = new Gson().fromJson(model, ThumbRoot.Datum.class);
                if (object != null) {
                    Log.d(TAG, "getIntentdata:image " + object.getImage());
                    Log.d(TAG, "getIntentdata:image " + object.getName());
                    thumbImage = object.getImage();
                    Glide.with(getApplicationContext())
                            .load(new SessionManager(VideoActivity.this).getStringValue(Const.IMAGE_URL) + thumbImage)
                            .transform(new BlurTransformation(25), new CenterCrop())
                            .circleCrop()
                            .into(binding.imgthumb);
                    Glide.with(getApplicationContext())
                            .load(new SessionManager(VideoActivity.this).getStringValue(Const.IMAGE_URL) + thumbImage)
                            .circleCrop()
                            .into(binding.imgprofile);
                    String s = String.valueOf(object.getName().charAt(0)).toUpperCase();
                    binding.tvName.setText(s.concat(object.getName().substring(1)));
                }

            }
            if (cid != null && !cid.equals("")) {
                countryid = cid;
                getVideo(cid);
                getComments(cid);
            }
        }
    }

    private void getComments(String cid) {
        Call<CommentRoot> call = RetrofitBuilder.create(this).getComment(cid);
        call.enqueue(new Callback<CommentRoot>() {
            @Override
            public void onResponse(Call<CommentRoot> call, Response<CommentRoot> response) {
                Log.d(TAG, "onResponse: " + response.code());
                if (response.code() == 200 && !response.body().getData().isEmpty()) {

                    comments = response.body().getData();
                    addDataTOCommentAdapter();


                }
            }

            @Override
            public void onFailure(Call<CommentRoot> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.toString());
            }
        });
    }

    private void setRandomViews() {
        Log.d(TAG, "setRandomViews: ");
        int randInt1 = rand.nextInt((1500 - 1100) + 1) + 1100;
        binding.tvviews.setText(String.valueOf(randInt1));
    }

    private void addDataTOCommentAdapter() {
        final Timer timer = new Timer();
        count = 0;
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(() -> {
                    if (count < comments.size()) {
                        commentAdapter.additem(comments.get(count));
                        binding.rvComments.scrollToPosition(count);
                        count++;
                    } else {
                        count = 0;
                        timer.cancel();
                        getComments(countryid);
                    }


                    setRandomViews();
                });


            }
        }, 0, 10000);


    }

    private void initView() {
        flagAds = false;
        getIntentdata();

        playerView = findViewById(R.id.video_view);

        updetUI(SHEET_CLOSE);

        setUI();

        getGiftsCategories();


        commentAdapter = new CommentAdapter();
        binding.rvComments.setAdapter(commentAdapter);


        ObjectAnimator animation = ObjectAnimator.ofFloat(binding.imggift2, "rotationY", 0.0f, 360f);
        animation.setDuration(5000);
        animation.setRepeatCount(INFINITE);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.start();
    }


    private void initListnear() {

        binding.etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//ll
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    //ll
                } else {
                    cleartext = false;
                    binding.lytbuttons.setVisibility(View.GONE);
                    binding.btnsend.setVisibility(View.VISIBLE);
                    binding.etComment.setVisibility(View.VISIBLE);
                    binding.lytShare.setVisibility(View.GONE);
                    binding.lytbuttons.setVisibility(View.GONE);
                    binding.btnsend.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//ll
            }
        });
        binding.btnsend.setOnClickListener(v -> {
            binding.btnsend.setVisibility(View.VISIBLE);
            binding.lytbuttons.setVisibility(View.VISIBLE);
            if (!binding.etComment.getText().toString().equals("")) {
                Log.d(TAG, "initListnear: comment1");
                CommentRoot.Datum datum = new CommentRoot.Datum();
                datum.setComment(binding.etComment.getText().toString().trim());
                datum.setName(sessionManager.getUser().getFname());
                Log.d(TAG, "initListnear: comment2");
                commentAdapter.additem(datum);
                Log.d(TAG, "initListnear: cmt3");
                binding.rvComments.setAdapter(commentAdapter);
                binding.rvComments.scrollToPosition(commentAdapter.getItemCount());
                Log.d(TAG, "initListnear: cmt4");

                setUI();


                cleartext = true;
                Log.d(TAG, "initListnear: cmt5");

            }
        });

        animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoomin);

        binding.imggift2.setOnClickListener(v -> {
            binding.bottomPage.tvUsereCoin.setText(String.valueOf(sessionManager.getUser().getCoin()));
            binding.tvusercoins.setText(String.valueOf(sessionManager.getUser().getCoin()));
            updetUI(SHEET_OPEN);
        });
        binding.videoView.setOnClickListener(v -> updetUI(SHEET_CLOSE));
        binding.bottomPage.btnclose.setOnClickListener(v -> updetUI(SHEET_CLOSE));


    }

    private void getGiftsCategories() {
        Call<EmojicategoryRoot> call = RetrofitBuilder.create(this).getCategories();
        call.enqueue(new Callback<EmojicategoryRoot>() {
            @Override
            public void onResponse(Call<EmojicategoryRoot> call, Response<EmojicategoryRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {

                    categories = response.body().getData();

                    for (int i = 0; i < categories.size(); i++) {
                        if (Boolean.TRUE.equals(categories.get(i).getIsTop())) {
                            tempobj = categories.get(i);
                        } else {
                            finelCategories.add(categories.get(i));
                        }
                    }
                    finelCategories.add(0, tempobj);
                    setGiftList();


                    BottomViewPagerAdapter bottomViewPagerAdapter = new BottomViewPagerAdapter(finelCategories);
                    binding.bottomPage.viewpager.setAdapter(bottomViewPagerAdapter);
                    settabLayout(finelCategories);
                    binding.bottomPage.viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.bottomPage.tablayout));
                    binding.bottomPage.tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            binding.bottomPage.viewpager.setCurrentItem(tab.getPosition());
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {
                            //ll
                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {
                            //ll
                        }
                    });
                    bottomViewPagerAdapter.setEmojiListnerViewPager((bitmap, coin) -> {
                        sendGift(bitmap, coin);
                        updetUI(SHEET_CLOSE);
                    });
                }
            }

            private void settabLayout(List<EmojicategoryRoot.Datum> categories) {
                binding.bottomPage.tablayout.setTabGravity(TabLayout.GRAVITY_FILL);
                for (int i = 0; i < categories.size(); i++) {

                    binding.bottomPage.tablayout.addTab(binding.bottomPage.tablayout.newTab().setCustomView(createCustomView(categories.get(i))));

                }
            }

            @Override
            public void onFailure(Call<EmojicategoryRoot> call, Throwable t) {
//ll
            }
        });
    }

    private View createCustomView(EmojicategoryRoot.Datum datum) {
        Log.d(TAG, "settabLayout: " + datum.getName());
        Log.d(TAG, "settabLayout: " + datum.getIcon());
        View v = LayoutInflater.from(this).inflate(R.layout.custom_tabgift, null);
        TextView tv = (TextView) v.findViewById(R.id.tvTab);
        tv.setText(datum.getName());
        ImageView img = (ImageView) v.findViewById(R.id.imagetab);

        Glide.with(getApplicationContext())
                .load(new SessionManager(VideoActivity.this).getStringValue(Const.IMAGE_URL) + datum.getIcon())
                .placeholder(R.drawable.ic_gift)
                .into(img);
        return v;

    }

    private void sendGift(Bitmap bitmap, Long coin) {


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", userId);
        jsonObject.addProperty("coin", coin);
        Call<UserRoot> call = RetrofitBuilder.create(this).lessCoin(jsonObject);
        call.enqueue(new Callback<UserRoot>() {
            @Override
            public void onResponse(Call<UserRoot> call, Response<UserRoot> response) {
                if (response.code() == 200) {

                    if (response.body().getStatus() == 200) {
                        // long c = rendtemp + coin;

                        rendtemp = (int) (rendtemp + coin);


                        double coin1;
                        if (rendtemp >= 1000) {
                            coin1 = (double) rendtemp / 1000;
                            DecimalFormat df = new DecimalFormat("#.##");

                            binding.tvCoin.setText(df.format(coin1).concat("K"));
                        } else {
                            coin1 = rendtemp;
                            binding.tvCoin.setText(String.valueOf((int) coin1));
                        }
                        // binding.tvCoin.setText(String.valueOf(c));
                        binding.tvusercoins.setText(String.valueOf(response.body().getData().getCoin()));
                        binding.bottomPage.tvUsereCoin.setText(String.valueOf(response.body().getData().getCoin()));

                        Log.d(TAG, "onResponse: success coin minused");
                        sessionManager.saveUser(response.body().getData());

                        binding.imgAnimation.setImageBitmap(bitmap);
                        binding.imgAnimation.setVisibility(View.VISIBLE);
                        binding.imgAnimation.startAnimation(animZoomIn);
                        new Handler().postDelayed(() -> {
                            binding.imgAnimation.setImageBitmap(null);
                            binding.imgAnimation.setVisibility(View.GONE);
                        }, 2000);
                    } else if (response.body().getStatus() == 422) {
                        Toast.makeText(VideoActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserRoot> call, Throwable t) {
                Log.d(TAG, "onFailure: 452 " + t.toString());
            }
        });


    }

    private void getVideo(String cid) {
        binding.pd.setVisibility(View.VISIBLE);
        Call<VideoRoot> call = RetrofitBuilder.create(this).getVideo(cid);
        call.enqueue(new Callback<VideoRoot>() {
            @Override
            public void onResponse(Call<VideoRoot> call, Response<VideoRoot> response) {
                if (response.code() == 200 && response.body() != null) {
                    if (!response.body().getData().isEmpty()) {
                        name = response.body().getData().get(0).getName();

                    }
                    if (response.body().getVideo() != null) {
                        videoURL = response.body().getVideo().getVideo();
                        Log.d(TAG, "onResponse: " + videoURL);

                        setvideoURL();

                    }
                }
            }

            @Override
            public void onFailure(Call<VideoRoot> call, Throwable t) {
                binding.pd.setVisibility(View.GONE);
            }
        });
    }

    private MediaSource buildMediaSource(Uri uri) {
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, "exoplayer-codelab");
        return new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);
    }


    private void setGiftList() {
        Call<EmojiIconRoot> call1 = RetrofitBuilder.create(this).getEmojiByCategory(finelCategories.get(0).get_id());
        call1.enqueue(new Callback<EmojiIconRoot>() {
            private void onEmojiClick(Bitmap bitmap, Long coin) {
                sendGift(bitmap, coin);
            }

            @Override
            public void onResponse(Call<EmojiIconRoot> call, Response<EmojiIconRoot> response) {
                Log.d(TAG, "onResponse: emoji yes" + response.code());
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {

                    emojiAdapter = new EmojiAdapter(response.body().getData());
                    binding.rvEmogi.setAdapter(emojiAdapter);

                    emojiAdapter.setOnEmojiClickListnear(this::onEmojiClick);


                }
            }

            @Override
            public void onFailure(Call<EmojiIconRoot> call, Throwable t) {
//ll
            }
        });
    }

    private void startTimer() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d(TAG, "run: " + time);
                    if (start) {
                        if (time == adsDiffTime) {


                            if (player != null) {
                                player.setPlayWhenReady(false);
                            }
                            if (dialog != null) {

                                showPopup();
                            }

                            Log.d(TAG, "run: popup showed");
                            start = false;
                            time = 0;
                        } else {
                            time++;
                        }
                    } else {
                        time = 0;
                    }
                    handler.postDelayed(this, 1000);

                } catch (IllegalStateException ed) {
                    Log.d(TAG, "run: " + ed.toString());
                    ed.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 1000);

    }

    private void setvideoURL() {
        binding.pd.setVisibility(View.VISIBLE);
        player = new SimpleExoPlayer.Builder(this).build();
        playerView.setPlayer(player);
        playerView.setShowBuffering(true);
        Log.d(TAG, "setvideoURL: " + videoURL);
        Uri uri = Uri.parse(videoURL);
        // Uri uri = Uri.parse( sessionManager.getStringValue(Const.BASE_URL)+videoURL);
        MediaSource mediaSource = buildMediaSource(uri);
        Log.d(TAG, "initializePlayer: " + uri);
        player.setPlayWhenReady(playWhenReady);
        player.seekTo(currentWindow, playbackPosition);
        player.prepare(mediaSource, false, false);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case STATE_BUFFERING:
                        Log.d(TAG, "buffer: " + uri);
                        break;
                    case STATE_ENDED:
                        player.setRepeatMode(Player.REPEAT_MODE_ALL);
                        Log.d(TAG, "end: " + uri);
                        break;
                    case STATE_IDLE:
                        Log.d(TAG, "idle: " + uri);
//                        if (player != null) {
//                            player.release();
//                        }
                        //  finish();
                        // getVideo(countryid);
                        break;

                    case STATE_READY:
                        binding.lytthumb.setVisibility(View.GONE);
                        binding.pd.setVisibility(View.GONE);
                        time = 0;
                        start = true;
                        Log.d(TAG, "ready: " + uri);

                        break;
                    default:
                        break;
                }
            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_video);
        sessionManager = new SessionManager(this);
        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            userId = sessionManager.getUser().get_id();
            binding.tvusercoins.setText(String.valueOf(sessionManager.getUser().getCoin()));
            Log.d(TAG, "onCreate: " + sessionManager.getUser().getCoin());
        }

        if (player != null) {
            player.release();
        }
        dialog = new Dialog(this, R.style.customStyle);
        adsDiffTime = Integer.parseInt(sessionManager.getStringValue(Const.ADS_TIME).equals("") ? "30" : sessionManager.getStringValue(Const.ADS_TIME));

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        popupbinding = DataBindingUtil.inflate(inflater, R.layout.pop_up_show_ads, null, false);

        LayoutInflater inflater2 = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        popUpShowOnActivityBinding = DataBindingUtil.inflate(inflater2, R.layout.pop_up_show_on_activity, null, false);

        startTimer();
        initMain();
        chkConnection2();


        binding.bubbleEmitter.emitBubble(20);
        binding.bubbleEmitter.canExplode(true);

        binding.lytusercoin.setOnClickListener(v -> startActivity(new Intent(this, EarnActivity.class)));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Anylitecs.removeSesson(this);
    }


    private void setUI() {
        binding.etComment.setText(null);
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(binding.getRoot().getWindowToken(), 0);
        binding.btnsend.setVisibility(View.VISIBLE);
        binding.lytbuttons.setVisibility(View.VISIBLE);

        binding.etComment.setVisibility(View.VISIBLE);
        binding.lytShare.setVisibility(View.VISIBLE);
    }

    private void updetUI(int state) {
        if (state == SHEET_OPEN) {
            binding.bottomPage.lyt2.setVisibility(View.VISIBLE);

            binding.rvComments.setVisibility(View.GONE);
            binding.rvEmogi.setVisibility(View.GONE);
            binding.lytbottom.setVisibility(View.GONE);
            binding.lytShare.setVisibility(View.GONE);
            binding.lytusercoin.setVisibility(View.GONE);
        } else {
            binding.bottomPage.lyt2.setVisibility(View.GONE);
            binding.rvComments.setVisibility(View.VISIBLE);
            binding.rvEmogi.setVisibility(View.VISIBLE);
            binding.lytbottom.setVisibility(View.VISIBLE);
            binding.lytShare.setVisibility(View.VISIBLE);
            binding.lytusercoin.setVisibility(View.VISIBLE);
        }
    }

    public void onClickClose(View view) {
        if (player != null) {
            player.release();
        }
        if (!showAds) {
            showAds = true;
            if (player != null) {
                player.setPlayWhenReady(false);
            }
            start = false;
        } else {
            return;
        }
        if (binding.lytOwnAds.getVisibility() == View.GONE) {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                initAds();
            } else if (mInterstitialAd3.isLoaded()) {
                mInterstitialAd3.show();
                initAds();
            } else if (interstitialAdfb.isAdLoaded()) {
                interstitialAdfb.show();
                initAds();
            } else if (ownLoded) {

                binding.lytOwnInter.setVisibility(View.VISIBLE);
                sendImpression(this, adid);
                binding.lytOwnInter.setOnClickListener(v -> {
                    Intent intent = new Intent(VideoActivity.this, WebActivity.class);
                    intent.putExtra("ADID", String.valueOf(adid));
                    intent.putExtra(WEBSITE, ownWebUrl);
                    intent.putExtra("type", "ads");
                    startActivity(intent);
                });
                binding.imgCloseInter.setOnClickListener(v -> finish());
            } else {
                if (player != null) {
                    player.release();
                }
                finish();
            }
        }


    }

    private void interAdListnear() {
        /*mAd.loadAd(new GGInterstitialEventsListener() {
                       @Override
                       public void onAdLoaded() {
                           Log.d("GGADS", "Ad Loaded");
                       }

                       @Override
                       public void onAdLeftApplication() {
                           Log.d("GGADS", "Ad Left Application");
                       }

                       @Override
                       public void onAdClosed() {
                           Log.d("GGADS", "Ad Closed");
                           if (googleorfbrunning) {
                               flagAds = false;


                               googleorfbrunning = false;
                               showAds = false;
                               start = true;
                               time = 0;

                               if (player != null && player.getPlaybackState() != STATE_IDLE) {
                                   player.seekTo(position);
                                   player.setPlayWhenReady(true);
                               } else {
                                   setvideoURL();
                                   if (player != null && player.getPlaybackState() != STATE_IDLE) {
                                       player.seekTo(position);
                                       player.setPlayWhenReady(true);
                                   }
                               }

                               dialog.dismiss();
                               time = 0;


                           } else {
                               Log.d(TAG, "onAdClosed: finish" + googleorfbrunning);
                               if (player != null) {
                                   player.release();
                               }
                               finish();
                           }
                           //Interstitial ad will be automatically refreshed by SDKX when     closed
                       }

                       @Override
                       public void onAdOpened() {
                           Log.d("GGADS", "Ad Opened");
                       }

                       @Override
                       public void onAdLoadFailed(AdRequestErrors cause) {
                           Log.d("GGADS", "Ad Load Failed " + cause);
                       }
                   }
        );*/

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d(TAG, "onAdFailedToLoad: " + adError.toString());
//nn
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.


                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "onAdClosed g in : " + googleorfbrunning);
                if (googleorfbrunning) {
                    flagAds = false;


                    googleorfbrunning = false;
                    showAds = false;
                    start = true;
                    time = 0;

                    if (player != null && player.getPlaybackState() != STATE_IDLE) {
                        player.seekTo(position);
                        player.setPlayWhenReady(true);
                    } else {
                        setvideoURL();
                        if (player != null && player.getPlaybackState() != STATE_IDLE) {
                            player.seekTo(position);
                            player.setPlayWhenReady(true);
                        }
                    }

                    dialog.dismiss();
                    time = 0;


                } else {
                    Log.d(TAG, "onAdClosed: finish" + googleorfbrunning);
                    if (player != null) {
                        player.release();
                    }
                    finish();
                }
                // Code to be executed when the interstitial ad is closed.
            }
        });


    }

    private void showAds() {


        if (player != null) {
            player.setPlayWhenReady(false);
            position = player.getCurrentPosition();
        }
        start = false;
        dialog.dismiss();
        Log.d(TAG, "showAds: " + showAds + flagAds);
        if (!showAds) {
            showAds = true;
            flagAds = true;

        } else {
            return;
        }
        Log.d(TAG, "showAds: " + googleNativeLoded);
        //  test devic3e 2FC1ED7FE95A342A976AB3AC32489A56
       /*
        else*/
        /*if (googleNativeLoded) {
            googleNativeLoded = false;
            Log.d(TAG, "showAds: native   ========================================================================");
            binding.lytNative.adlyt.setVisibility(View.VISIBLE);
            binding.lytNative.btnclose.setOnClickListener(v -> {
                binding.lytNative.adlyt.setVisibility(View.GONE);
                start = true;
                time = 0;
                Log.d(TAG, "showAds: exoplayer state " + player.getPlaybackState());
                if (player != null && player.getPlaybackState() != STATE_IDLE) {
                    player.seekTo(position);
                    player.setPlayWhenReady(true);
                }else {
                    setvideoURL();
                    if (player != null&&player.getPlaybackState()!= STATE_IDLE) {
                        player.seekTo(position);
                        player.setPlayWhenReady(true);
                    }
                }
                showAds = false;
                flagAds = false;
                dialog.dismiss();
                time = 0;
                Log.d(TAG, "showAds: clicked   ========================================================================");
            });*/

        /* else *//*if (ggnativeloded) {

            Log.d(TAG, "showAds: gg native "+ggnativeloded);
            Log.d(TAG, "showAds: exop "+player.getPlaybackState());



            binding.lytNative.adlyt.setVisibility(View.VISIBLE);

            ggnativeloded = false;
        }*/
        Log.d(TAG, "showAds: googleintr " + mInterstitialAd.isLoaded());
        Log.d(TAG, "showAds: fvinter " + interstitialAdfb.isAdLoaded());
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            initAds();
            googleorfbrunning = true;
        } else if (mInterstitialAd3.isLoaded()) {
            Log.d(TAG, "showAds: inter fb loded");
            googleorfbrunning = true;
            mInterstitialAd3.show();
            initAds();
        } else if (interstitialAdfb.isAdLoaded()) {
            Log.d(TAG, "showAds: inter fb loded");
            googleorfbrunning = true;
            interstitialAdfb.show();
            initAds();
        } /*else if (ownLoded) {
            setOwnAds();
        }*/ else {
            Log.d(TAG, "showAds:  nothing loded ");
            flagAds = false;

            showAds = false;
            start = true;
            time = 0;
            if (player != null && player.getPlaybackState() != STATE_IDLE) {
                player.seekTo(position);
                player.setPlayWhenReady(true);
            } else {
                setvideoURL();
                if (player != null && player.getPlaybackState() != STATE_IDLE) {
                    player.seekTo(position);
                    player.setPlayWhenReady(true);
                }
            }

            dialog.dismiss();
            time = 0;

        }
        initAds();
    }

    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        // Set the media view.
        adView.setMediaView((MediaView) adView.findViewById(R.id.ad_media));

        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        adView.getMediaView().setMediaContent(nativeAd.getMediaContent());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad.
        adView.setNativeAd(nativeAd);

        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAd.getVideoController();

        // Updates the UI to say whether or not this ad has a video asset.
        binding.lytNative.flAdplaceholder.removeAllViews();
        binding.lytNative.flAdplaceholder.addView(adView);
    }

    private void fbInterAdListnear() {
        interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.e(TAG, "Interstitial ad displayed.");


/*
                interstitialAdfb.loadAd(
                        interstitialAdfb.buildLoadAdConfig()
                                .withAdListener(interstitialAdListener)
                                .build());
*/

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {

                if (googleorfbrunning) {
                    flagAds = false;


                    googleorfbrunning = false;
                    showAds = false;
                    start = true;
                    time = 0;
                    if (player != null && player.getPlaybackState() != STATE_IDLE) {
                        player.seekTo(position);
                        player.setPlayWhenReady(true);
                    } else {
                        setvideoURL();
                        if (player != null && player.getPlaybackState() != STATE_IDLE) {
                            player.seekTo(position);
                            player.setPlayWhenReady(true);
                        }
                    }

                    dialog.dismiss();
                    time = 0;


                } else {
                    if (player != null) {
                        player.release();
                    }
                    finish();
                }
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Log.e(TAG, "Interstitial ad failed to load:aa " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {


                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");

            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        };
    }

    private void showPopup() {

        if (dialog.isShowing()) {
            time = 0;
            return;
        }


        dialog.setContentView(popupbinding.getRoot());
        // popupbinding.tvPositive.setVisibility(View.GONE);

       /* GGAdview bannerUnit = new GGAdview(this);
        bannerUnit.setUnitId((google != null) ? google.getJsonMemberNative() : " ");  //Replace with your Ad Unit ID here
        bannerUnit.setAdsMaxHeight(400);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        popupbinding.flAdplaceholderbanner.removeAllViews();
        popupbinding.flAdplaceholderbanner.addView(bannerUnit, layoutParams);
        bannerUnit.loadAd(new AdLoadCallback() {
                              @Override
                              public void onReadyForRefresh() {
                                  Log.d("GGADS", "Ad Ready for refresh");
                              }

                              @Override
                              public void onUiiClosed() {
                                  Log.d("GGADS", "Uii closed");
                              }

                              @Override
                              public void onUiiOpened() {
                                  Log.d("GGADS", "Uii Opened");
                              }

                              @Override
                              public void onAdLoadFailed(AdRequestErrors cause) {
                                  Log.d("GGADS", "Ad Load Failed " + cause);

                                  popupbinding.tvPositive.setVisibility(View.VISIBLE);

                              }

                              @Override
                              public void onAdLoaded() {
                                  Log.d("GGADS", "Ad Loaded");
                                  popupbinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                                  popupbinding.tvPositive.setVisibility(View.VISIBLE);
                              }
                          }
        );*/


        try {
            AdLoader adLoader = new AdLoader.Builder(this, (google != null) ? google.getJsonMemberNative() : "")
                    .forUnifiedNativeAd(unifiedNativeAd -> {
                        // Show the ad.
                        Log.d(TAG, "showPopup: popup native loded");
                        popupbinding.tvPositive.setVisibility(View.VISIBLE);
                        popupbinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                        UnifiedNativeAdView adView =
                                (UnifiedNativeAdView) getLayoutInflater()
                                        .inflate(R.layout.native_google_popup, null);

                        ImageView imageView = adView.findViewById(R.id.ad_media);
                        Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).circleCrop().transform(new RoundedCorners(20)).into(imageView);
                        // Set other ad assets.
                        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                        adView.setBodyView(adView.findViewById(R.id.ad_body));
                        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));

                        adView.setPriceView(adView.findViewById(R.id.ad_price));
                        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
                        adView.setStoreView(adView.findViewById(R.id.ad_store));
                        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

                        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                        ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                        // check before trying to display them.
                        if (unifiedNativeAd.getBody() == null) {
                            adView.getBodyView().setVisibility(View.INVISIBLE);
                        } else {
                            adView.getBodyView().setVisibility(View.VISIBLE);
                            ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                        }

                        if (unifiedNativeAd.getCallToAction() == null) {
                            adView.getCallToActionView().setVisibility(View.INVISIBLE);
                        } else {
                            adView.getCallToActionView().setVisibility(View.VISIBLE);
                            ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                        }


                        if (unifiedNativeAd.getPrice() == null) {
                            adView.getPriceView().setVisibility(View.INVISIBLE);
                        } else {
                            adView.getPriceView().setVisibility(View.VISIBLE);
                            ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                        }

                        if (unifiedNativeAd.getStore() == null) {
                            adView.getStoreView().setVisibility(View.INVISIBLE);
                        } else {
                            adView.getStoreView().setVisibility(View.VISIBLE);
                            ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                        }

                        if (unifiedNativeAd.getStarRating() == null) {
                            adView.getStarRatingView().setVisibility(View.INVISIBLE);
                        } else {
                            ((RatingBar) adView.getStarRatingView())
                                    .setRating(unifiedNativeAd.getStarRating().floatValue());
                            adView.getStarRatingView().setVisibility(View.VISIBLE);
                        }


                        adView.setNativeAd(unifiedNativeAd);
                        popupbinding.flAdplaceholderbanner.removeAllViews();
                        popupbinding.flAdplaceholderbanner.addView(adView);


                    })
                    .withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {
                            showgoogle2native();
                            Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());

                            // Handle the failure by logging, altering the UI, and so on.
                        }

                        private void showgoogle2native() {
                            AdLoader adLoader = new AdLoader.Builder(VideoActivity.this, (google2 != null) ? google2.getJsonMemberNative() : "")
                                    .forUnifiedNativeAd(unifiedNativeAd -> {
                                        // Show the ad.
                                        Log.d(TAG, "showPopup: popup native loded");
                                        popupbinding.tvPositive.setVisibility(View.VISIBLE);
                                        popupbinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                                        UnifiedNativeAdView adView =
                                                (UnifiedNativeAdView) getLayoutInflater()
                                                        .inflate(R.layout.native_google_popup, null);

                                        ImageView imageView = adView.findViewById(R.id.ad_media);
                                        Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).circleCrop().transform(new RoundedCorners(20)).into(imageView);
                                        // Set other ad assets.
                                        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                                        adView.setBodyView(adView.findViewById(R.id.ad_body));
                                        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));

                                        adView.setPriceView(adView.findViewById(R.id.ad_price));
                                        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
                                        adView.setStoreView(adView.findViewById(R.id.ad_store));
                                        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

                                        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                                        ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                                        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                                        // check before trying to display them.
                                        if (unifiedNativeAd.getBody() == null) {
                                            adView.getBodyView().setVisibility(View.INVISIBLE);
                                        } else {
                                            adView.getBodyView().setVisibility(View.VISIBLE);
                                            ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                                        }

                                        if (unifiedNativeAd.getCallToAction() == null) {
                                            adView.getCallToActionView().setVisibility(View.INVISIBLE);
                                        } else {
                                            adView.getCallToActionView().setVisibility(View.VISIBLE);
                                            ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                                        }


                                        if (unifiedNativeAd.getPrice() == null) {
                                            adView.getPriceView().setVisibility(View.INVISIBLE);
                                        } else {
                                            adView.getPriceView().setVisibility(View.VISIBLE);
                                            ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                                        }

                                        if (unifiedNativeAd.getStore() == null) {
                                            adView.getStoreView().setVisibility(View.INVISIBLE);
                                        } else {
                                            adView.getStoreView().setVisibility(View.VISIBLE);
                                            ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
                                        }

                                        if (unifiedNativeAd.getStarRating() == null) {
                                            adView.getStarRatingView().setVisibility(View.INVISIBLE);
                                        } else {
                                            ((RatingBar) adView.getStarRatingView())
                                                    .setRating(unifiedNativeAd.getStarRating().floatValue());
                                            adView.getStarRatingView().setVisibility(View.VISIBLE);
                                        }


                                        adView.setNativeAd(unifiedNativeAd);
                                        popupbinding.flAdplaceholderbanner.removeAllViews();
                                        popupbinding.flAdplaceholderbanner.addView(adView);

                                    })
                                    .withAdListener(new AdListener() {
                                        @Override
                                        public void onAdFailedToLoad(LoadAdError adError) {
                                            shownativefb();
                                            Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());
                                            popupbinding.flAdplaceholderbanner.setVisibility(View.GONE);
                                            // Handle the failure by logging, altering the UI, and so on.
                                        }
                                    })
                                    .withNativeAdOptions(new NativeAdOptions.Builder()
                                            // Methods in the NativeAdOptions.Builder class can be
                                            // used here to specify individual options settings.
                                            .build())
                                    .build();
                            adLoader.loadAd(new AdRequest.Builder().build());
                        }

                        private void shownativefb() {
                            sessionManager = new SessionManager(VideoActivity.this);
                            AdvertisementRoot.Facebook facebook = sessionManager.getAdsKeys().getFacebook();

                            NativeAd nativeAd = new NativeAd(VideoActivity.this, (facebook != null) ? facebook.getJsonMemberNative() : " ");
                            NativeAdListener nativeAdListener = new NativeAdListener() {
                                @Override
                                public void onMediaDownloaded(Ad ad) {
                                    // Native ad finished downloading all assets
                                    Log.e("TAG", "Native ad finished downloading all assets.");
                                }

                                @Override
                                public void onError(Ad ad, AdError adError) {
                                    popupbinding.itemBannerownad.lytad.setVisibility(View.VISIBLE);
                                    popupbinding.tvPositive.setVisibility(View.VISIBLE);
                                    popupbinding.nativeAdFb.setVisibility(View.GONE);
                                    // Native ad failed to load
                                    Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
                                }

                                @Override
                                public void onAdLoaded(Ad ad) {
                                    if (nativeAd == null || nativeAd != ad) {
                                        return;
                                    }
                                    inflateAd(nativeAd);
                                    // Native ad is loaded and ready to be displayed
                                    Log.d("TAG", "Native ad is loaded and ready to be displayed!");

                                }

                                private void inflateAd(NativeAd nativeAd) {
                                    popupbinding.nativeAdFb.setVisibility(View.VISIBLE);
                                    nativeAd.unregisterView();

                                    // Add the Ad view into the ad container.

                                    NativeAdLayout nativeAdLayout = popupbinding.nativeAdFb;

                                    LayoutInflater inflater = LayoutInflater.from(VideoActivity.this);
                                    // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                                    LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_fb_popup, nativeAdLayout, false);
                                    nativeAdLayout.addView(adView);

                                    // Add the AdOptionsView
                      /*  LinearLayout adChoicesContainer = itemView.findViewById(R.id.ad_choices_container);
                        AdOptionsView adOptionsView = new AdOptionsView(context, nativeAd, nativeAdLayout);
                        adChoicesContainer.removeAllViews();
                        adChoicesContainer.addView(adOptionsView, 0);*/

                                    // Create native UI using the ad metadata.
                                    com.facebook.ads.MediaView nativeAdIcon = adView.findViewById(R.id.native_ad_media);
                                    TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
                                    /*MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);*/
                                    /* TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);*/
                                    TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
                                    /* TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);*/
                                    TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

                                    // Set the Text.
                                    nativeAdTitle.setText(nativeAd.getAdvertiserName());
                                    nativeAdBody.setText(nativeAd.getAdBodyText());
                                    /*nativeAdSocialContext.setText(nativeAd.getAdSocialContext());*/
                                    nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                                    nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                                    // Create a list of clickable views
                                    List<View> clickableViews = new ArrayList<>();
                                    clickableViews.add(nativeAdTitle);
                                    clickableViews.add(nativeAdCallToAction);

                                    // Register the Title and CTA button to listen for clicks.
                                    nativeAd.registerViewForInteraction(
                                            adView, nativeAdIcon, clickableViews);
                                }

                                @Override
                                public void onAdClicked(Ad ad) {

                                }

                                @Override
                                public void onLoggingImpression(Ad ad) {

                                }
                            };

                            nativeAd.loadAd(
                                    nativeAd.buildLoadAdConfig()
                                            .withAdListener(nativeAdListener)
                                            .build());

                        }

                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder()
                            // Methods in the NativeAdOptions.Builder class can be
                            // used here to specify individual options settings.
                            .build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());
        } catch (Exception o) {
            Log.d(TAG, "showPopup: trycatch " + o.getMessage());
        }

        dialog.setCancelable(false);
        Glide.with(getApplicationContext())
                .load(new SessionManager(VideoActivity.this).getStringValue(Const.IMAGE_URL) + thumbImage).placeholder(R.drawable.adduser)
                .circleCrop()
                .into(popupbinding.imagepopup);
        popupbinding.tv1.setText("Hello Dear, " + sessionManager.getUser().getFname());
        popupbinding.textview.setText("To countinue with " + binding.tvName.getText().toString() + ",\n watch this Ads");

        popupbinding.tvPositive.setOnClickListener(v -> showAds());
        popupbinding.tvCencel.setOnClickListener(v -> {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            showAds();
            finish();
        });

        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onresume: fad " + isFirstAdShowed);

        if (!isFirstAdShowed) {


            Anylitecs.addUser(this);
            if (player != null) {
                player.setVolume(1.0f);
            }
            Log.d(TAG, "onResume: 1");
            binding.lytOwnAds.setVisibility(View.GONE);
            binding.lytOwnInter.setVisibility(View.GONE);
            binding.lytOwnAds.setVisibility(View.GONE);
            binding.videoView.setVisibility(View.VISIBLE);
            Log.d(TAG, "googleorfbrunning   onremused  " + googleorfbrunning);
            Log.d(TAG, "googleor     paused onremused" + paused);
            if (paused) {
                if (player != null) {
                    player.setPlayWhenReady(false);
                }
                start = false;
                showAds = false;
                time = 0;
                if (dialog != null) {
                    showPopup();

                }
            } else {
                if (player != null) {
                    player.setPlayWhenReady(true);
                }
                start = true;
                showAds = false;
                time = 0;
            }
        }
        isFirstAdShowed = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: fad " + isFirstAdShowed);
        if (!isFirstAdShowed) {


            Anylitecs.removeSesson(this);
            binding.lytOwnAds.setVisibility(View.GONE);
            binding.lytOwnInter.setVisibility(View.GONE);

            dialog.dismiss();
            if (player != null) {
                Log.d(TAG, "onPause: " + player.getVolume());
                player.setVolume(0);
                position = player.getCurrentPosition();
                player.setPlayWhenReady(false);
            }
            start = false;
            Log.d(TAG, "googleorfbrunning   onpaused  " + googleorfbrunning);
            Log.d(TAG, "googleor     paused onpaused" + paused);
            if (adPlayer != null) {
                adPosition = adPlayer.getCurrentPosition();
                adPlayer.setPlayWhenReady(false);
            }

            paused = !googleorfbrunning;
            Log.d(TAG, "onPause: ");
            time = 0;


            binding.lytOwnAds.setVisibility(View.GONE);
            binding.videoView.setVisibility(View.VISIBLE);
            videoAdShow = false;
            binding.tvTime.setVisibility(View.GONE);
            binding.btnClose.setVisibility(View.VISIBLE);
            binding.btnClose.setOnClickListener(v -> closeOwnAds());
            if (adPlayer != null) {
                adPlayer.release();
                adPlayer = null;
            }

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (player != null) {
            player.release();
        }
        if (Util.SDK_INT < 24) {
            if (player != null) {
                position = player.getCurrentPosition();
                player.setPlayWhenReady(false);
            }
            start = false;
        }
        if (Util.SDK_INT >= 24) {
            if (adPlayer != null) {
                adPlayer.setPlayWhenReady(false);
            }
            start = false;
        }
    }

    @Override
    public void onBackPressed() {
        if (!showAds) {
            showAds = true;
            if (player != null) {
                player.setPlayWhenReady(false);
            }
            start = false;
        } else {
            return;
        }
        if (binding.lytOwnAds.getVisibility() == View.GONE) {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                initAds();
            } else if (mInterstitialAd3.isLoaded()) {
                mInterstitialAd3.show();
                initAds();
            } else if (interstitialAdfb.isAdLoaded()) {
                interstitialAdfb.show();
                initAds();
            } else if (ownLoded) {

                binding.lytOwnInter.setVisibility(View.VISIBLE);
                sendImpression(this, adid);
                binding.lytOwnInter.setOnClickListener(v -> {

                    Intent intent = new Intent(VideoActivity.this, WebActivity.class);
                    intent.putExtra("ADID", String.valueOf(adid));
                    intent.putExtra(WEBSITE, ownWebUrl);
                    intent.putExtra("type", "ads");
                    startActivity(intent);
                });
                binding.imgCloseInter.setOnClickListener(v -> finish());
            } else {
                if (player != null) {
                    player.release();
                }
                finish();
                if (player != null) {
                    player.release();
                }
            }
        }


    }

    public void onEarnActivity(View view) {
        Intent intent = new Intent(VideoActivity.this, EarnActivity.class);
        startActivity(intent);
    }

    public void onclickShare(View view) {

        final String appLink = "\nhttps://play.google.com/store/apps/details?id=" + getPackageName();
        Intent sendInt = new Intent(Intent.ACTION_SEND);
        sendInt.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sendInt.putExtra(Intent.EXTRA_TEXT, "Humile - live video chat  Download Now  " + appLink);
        sendInt.setType("text/plain");
        startActivity(Intent.createChooser(sendInt, "Share"));
    }

    public void onClickchat(View view) {
        showAds();
        startActivity(new Intent(this, ChatActivity.class).putExtra("image", thumbImage).putExtra("name", binding.tvName.getText().toString()));
    }
}