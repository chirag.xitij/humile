package com.livevideocall.livegirl.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.JsonObject;
import com.greedygame.core.adview.modals.AdRequestErrors;
import com.greedygame.core.interstitial.general.GGInterstitialAd;
import com.greedygame.core.interstitial.general.GGInterstitialEventsListener;
import com.hbb20.CountryCodePicker;
import com.livevideocall.livegirl.Anylitecs;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.databinding.ActivityNewUserBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.models.User;
import com.livevideocall.livegirl.models.UserRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.livevideocall.livegirl.activity.EarnActivity.sendImpression;

public class NewUserActivity extends AppCompatActivity {

    private static final String TAG = "newuseract";
    private static final CharSequence REQUIRED = "Required";
    private static final String WEBSITE = "WEBSITE";
    ActivityNewUserBinding binding;
    boolean emptymobile = false;
    boolean emptyFname = false;
    boolean emptyLname = false;
    boolean emptyusername = false;
    boolean emptyemail = false;
    boolean emptycountry = false;
    InterstitialAdListener interstitialAdListener;
    AdView adView;
    boolean fetched;
    private String gender = "male";
    private SessionManager sessonManager;
    private String adid;
    private com.facebook.ads.InterstitialAd interstitialAdfb;
    private InterstitialAd mInterstitialAd;
    private com.facebook.ads.AdListener adListener;
    private com.facebook.ads.AdView adViewfb;
    private String ownAdRewardUrl = "";
    private String ownAdBannerUrl = "";
    private String ownAdInstarUrl = "";
    private String ownWebUrl = "";
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Facebook facebook;
    private String countrycode = "";
    private GGInterstitialAd mAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        MainActivity.setStatusBarGradiant(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_user);
        sessonManager = new SessionManager(this);
        getIntentData();
        initListnear();
        radiolistnear();
        loadAds();
    }

    private void loadAds() {
        MobileAds.initialize(this, initializationStatus -> {
        });
        AudienceNetworkAds.initialize(this);

        getOwnAds();
        fetched = true;
        if (sessonManager.getBooleanValue(Const.ADS_Downloded)) {

            if (Boolean.TRUE.equals(sessonManager.getAdsKeys().getGoogle().isShow())) {
                google = sessonManager.getAdsKeys().getGoogle();
            }
            if (Boolean.TRUE.equals(sessonManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessonManager.getAdsKeys().getFacebook();
            }

            setBanner();

            mAd = new GGInterstitialAd(this, (google != null) ? google.getInterstitial() : "");
            /*mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId((google != null) ? google.getInterstitial() : "");
            mInterstitialAd.loadAd(new AdRequest.Builder().build());*/
            interAdListnear();

            fbInterAdListnear();
            interstitialAdfb = new com.facebook.ads.InterstitialAd(this, (facebook != null) ? facebook.getInterstitial() : "");
            interstitialAdfb.loadAd(
                    interstitialAdfb.buildLoadAdConfig()
                            .withAdListener(interstitialAdListener)
                            .build());

            fbBannerAdListnear();
            adViewfb = new com.facebook.ads.AdView(this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
            LinearLayout adContainer = findViewById(R.id.banner_container);
            adContainer.addView(adViewfb);
            adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(adListener).build());
        } else {
            Toast.makeText(this, "ads not downloded ", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Anylitecs.addUser(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Anylitecs.removeSesson(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Anylitecs.removeSesson(this);
        Log.d(TAG, "onPause: ");
    }

    private void initListnear() {
        binding.etReferCode.setOnEditorActionListener((v, actionId, event) -> true);

        binding.ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                // Toast.makeText(NewUserActivity.this, "Updated " + binding.ccp.getSelectedCountryCode(), Toast.LENGTH_SHORT).show();
                countrycode = binding.ccp.getSelectedCountryCode();
            }
        });
    }

    private void radiolistnear() {
        gender = "male";
        binding.btnmale.setOnClickListener(v -> {
            binding.btnfemale.setChecked(false);
            binding.btnmale.setChecked(true);
            gender = "male";
        });
        binding.btnfemale.setOnClickListener(v -> {
            binding.btnfemale.setChecked(true);
            binding.btnmale.setChecked(false);
            gender = "female";
        });
    }

    private void getIntentData() {
        Intent intent = getIntent();

        if (intent != null) {
            String type = intent.getStringExtra("type");
            String data = intent.getStringExtra("data");
            String name = intent.getStringExtra("name");
            if (type != null && !type.equals("")) {
                if (type.equals(LoginActivity.EMAIL)) {
                    binding.etEmail.setText(data);
                    binding.etEmail.setEnabled(false);
                } else {

                    if (data != null) {
                        //  binding.etCountry.setText(data.substring(1, 3));
                        binding.etMobile.setText(data.substring(3));
                        binding.etMobile.setEnabled(false);
                    }
                    if (name != null && !name.equals("")) {
                        binding.etFname.setText(name);
                    }
                }
            }
        }
    }

    public void onClickCountinue(View view) {
        emptymobile = false;
        emptyFname = false;

        emptyusername = false;
        emptyemail = false;
        emptycountry = false;

        String country = binding.ccp.getSelectedCountryCode();
        String mobile = binding.etMobile.getText().toString();
        String firstname = binding.etFname.getText().toString();
        String email = binding.etEmail.getText().toString();
        String username = binding.etUsername.getText().toString();
        String refercode = binding.etReferCode.getText().toString();


        if (country.equals("")) {
            emptycountry = true;
            Toast.makeText(this, "Select Country First", Toast.LENGTH_SHORT).show();
        }
        if (mobile.equals("")) {
            emptymobile = true;
            binding.etMobile.setError(REQUIRED);
        }
        Log.d(TAG, "onClickCountinue: " + mobile.length());
        if (mobile.length() != 10) {
            binding.etMobile.setError("Should be 10 digit");
            return;
        }
        if (firstname.equals("")) {
            emptyFname = true;
            binding.etFname.setError(REQUIRED);
        }

        if (email.equals("")) {
            emptyemail = true;
            binding.etEmail.setError(REQUIRED);
        }
        if (!email.contains("@gmail.com")) {
            emptyemail = true;
            binding.etEmail.setError("Enter Valid Email");
            return;
        }
        if (username.equals("")) {
            emptyusername = true;
            binding.etUsername.setError(REQUIRED);
        }

        if (!emptycountry && !emptyFname && !emptymobile && !emptyemail && !emptyusername) {
            binding.pd.setVisibility(View.VISIBLE);
            String mobilenumber = "+" + country + mobile;


            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("fname", firstname);
            jsonObject.addProperty("email", email);
            jsonObject.addProperty("mobile", mobilenumber);
            jsonObject.addProperty("gender", gender);
            jsonObject.addProperty("username", username);
            jsonObject.addProperty("referralCode", refercode);

            Call<UserRoot> call = RetrofitBuilder.create(this).signUpUser(jsonObject);
            call.enqueue(new Callback<UserRoot>() {
                @Override
                public void onResponse(Call<UserRoot> call, Response<UserRoot> response) {

                    if (response.code() == 200) {

                        if (response.body() != null && response.body().getStatus() != null && response.body().getStatus() == 200) {
                            User user = response.body().getData();
                            sessonManager.saveUser(user);
                            sessonManager.saveBooleanValue(Const.IS_LOGIN, true);

                            startActivity(new Intent(NewUserActivity.this, MainActivity.class));
                            finishAffinity();
                        }
                        if (response.body() != null && response.body().getStatus() != null && response.body().getStatus() == 403) {

                            String msg = response.body().getMessage();
                            if (msg.equals("Email already Exist!")) {
                                binding.etEmail.setEnabled(true);
                                binding.etEmail.setError(msg);
                            } else if (msg.equals("Mobile No already Exist!")) {
                                binding.etMobile.setEnabled(true);
                                binding.etMobile.setError(msg);
                            } else if (msg.equals("Username already Exist!")) {

                                binding.etUsername.setError(msg);
                            } else if (msg.equals("ReferralCode is Not Exist!")) {

                                binding.etReferCode.setError(msg);
                            } else {
                                Toast.makeText(NewUserActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                    binding.pd.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<UserRoot> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t);
                }
            });

        }

    }


    private void getOwnAds() {
        RetrofitBuilder.create(this).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {

                }
            }

            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {
///mm
            }
        });
    }

    private void setBanner() {
        LinearLayout adContainer = findViewById(R.id.banner_container);

        adView = new AdView(this);

        adView.setAdUnitId((google != null) ? google.getBanner() : "");
        adContainer.addView(adView);
        AdRequest adRequest =
                new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();
        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);


        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Log.d(TAG, "onAdFailedToLoad: banner " + loadAdError);

                adContainer.removeAllViews();
                setFbBanner();
            }

            private void setFbBanner() {
                adViewfb = new com.facebook.ads.AdView(NewUserActivity.this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);

                adContainer.addView(adViewfb);
                adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.d(TAG, "onError: fb " + adError.getErrorMessage());
                        binding.imgOwnAd.setVisibility(View.VISIBLE);
                        sendImpression(NewUserActivity.this, adid);
                        binding.imgOwnAd.setOnClickListener(v -> {
                            Intent intent = new Intent(NewUserActivity.this, WebActivity.class);
                            intent.putExtra("ADID", String.valueOf(adid));
                            intent.putExtra(WEBSITE, ownWebUrl);
                            intent.putExtra("type", "ads");
                            startActivity(intent);
                        });

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
//mm
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
//mm
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
//mm
                    }
                }).build());
            }
        });

        /*GGAdview bannerUnit = new GGAdview(this);
        bannerUnit.setUnitId((google != null) ? google.getBanner() : " ");  //Replace with your Ad Unit ID here
        bannerUnit.setAdsMaxHeight(250);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 200);
        adContainer.addView(bannerUnit, layoutParams);
        bannerUnit.loadAd(new AdLoadCallback() {

            @Override
            public void onAdLoadFailed(@NotNull AdRequestErrors adRequestErrors) {

                setFbBanner();
            }

            private void setFbBanner() {
                adViewfb = new com.facebook.ads.AdView(NewUserActivity.this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);

                adContainer.addView(adViewfb);
                adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.d(TAG, "onError: fb " + adError.getErrorMessage());
                        binding.imgOwnAd.setVisibility(View.VISIBLE);
                        sendImpression(NewUserActivity.this, adid);
                        binding.imgOwnAd.setOnClickListener(v -> {
                            Intent intent = new Intent(NewUserActivity.this, WebActivity.class);
                            intent.putExtra("ADID", String.valueOf(adid));
                            intent.putExtra(WEBSITE, ownWebUrl);
                            intent.putExtra("type", "ads");
                            startActivity(intent);
                        });

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
//mm
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
//mm
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
//mm
                    }
                }).build());
            }


            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onReadyForRefresh() {

            }

            @Override
            public void onUiiOpened() {

            }

            @Override
            public void onUiiClosed() {

            }

        });*/

    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void interAdListnear() {
        mAd.loadAd(new GGInterstitialEventsListener() {
                       @Override
                       public void onAdLoaded() {
                           Log.d("GGADS", "Ad Loaded");
                       }

                       @Override
                       public void onAdLeftApplication() {
                           Log.d("GGADS", "Ad Left Application");
                       }

                       @Override
                       public void onAdClosed() {
                           Log.d("GGADS", "Ad Closed");
                           finish();
                           //Interstitial ad will be automatically refreshed by SDKX when     closed
                       }

                       @Override
                       public void onAdOpened() {
                           Log.d("GGADS", "Ad Opened");
                       }

                       @Override
                       public void onAdLoadFailed(AdRequestErrors cause) {
                           Log.d("GGADS", "Ad Load Failed " + cause);
                       }
                   }
        );

/*
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
//mm
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                finish();
                // Code to be executed when the interstitial ad is closed.
            }
        });
*/


    }

    private void fbInterAdListnear() {
        interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                finish();
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {


                // Interstitial ad is loaded and ready to be displayed
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                // Show the ad
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        };
    }

    private void fbBannerAdListnear() {
        adListener = new com.facebook.ads.AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback

            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
            }
        };
    }

    @Override
    public void onBackPressed() {

        if (mAd.isAdLoaded()) {
            mAd.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else {

            binding.lytOwnInter.setVisibility(View.VISIBLE);
            sendImpression(this, adid);
            binding.lytOwnInter.setOnClickListener(v -> {
                Intent intent = new Intent(NewUserActivity.this, WebActivity.class);
                intent.putExtra("ADID", String.valueOf(adid));
                intent.putExtra(WEBSITE, ownWebUrl);
                intent.putExtra("type", "ads");
                startActivity(intent);
            });
            binding.imgCloseInter.setOnClickListener(v -> finish());
        }


    }
}