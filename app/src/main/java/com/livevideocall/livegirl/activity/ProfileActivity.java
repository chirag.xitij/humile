package com.livevideocall.livegirl.activity;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.gson.JsonObject;
import com.livevideocall.livegirl.Anylitecs;
import com.livevideocall.livegirl.BuildConfig;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.databinding.ActivityProfileBinding;
import com.livevideocall.livegirl.databinding.ShowPopupCoinadBinding;
import com.livevideocall.livegirl.databinding.ShowPopupErrorBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.models.ProfileRoot;
import com.livevideocall.livegirl.models.ReferRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "profileact";
    private static final String WEBSITE = "WEBSITE";
    ActivityProfileBinding binding;
    SessionManager sessionManager;
    InterstitialAdListener interstitialAdListener;
    AdView adView;
    boolean fetched;
    private String userId;
    private ProfileRoot.Data user;
    private String adid;
    private com.facebook.ads.InterstitialAd interstitialAdfb;
    private InterstitialAd mInterstitialAd;
    private com.facebook.ads.AdListener adListener;
    private com.facebook.ads.AdView adViewfb;
    private String ownAdRewardUrl = "";
    private String ownAdBannerUrl = "";
    private String ownAdInstarUrl = "";
    private String ownWebUrl = "";
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Facebook facebook;
    private boolean ownLoded = false;
    private boolean instastialAd = false;
    private Dialog dialog;
    private int time = 0;
    private AdvertisementRoot.Google2 google2;
    private InterstitialAd mInterstitialAd2;
    private int adsDiffTime;
    private ShowPopupCoinadBinding popupbinding;
    private ShowPopupErrorBinding popUpShowOnActivityBinding;
    private boolean isFromPopup = false;
    private OwnAdsRoot.DataItem ownAds;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.purplepink));
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(binding.getRoot().getWindowToken(), 0);
        sessionManager = new SessionManager(this);


        dialog = new Dialog(ProfileActivity.this, R.style.customStyle);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        popupbinding = DataBindingUtil.inflate(inflater, R.layout.show_popup_coinad, null, false);


        LayoutInflater inflater2 = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        popUpShowOnActivityBinding = DataBindingUtil.inflate(inflater2, R.layout.show_popup_error, null, false);


        Log.d(TAG, "onCreate: login yes" + sessionManager.getBooleanValue(Const.IS_LOGIN));
        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            userId = sessionManager.getUser().get_id();
            getData();
            initListnar();
        }
        loadAds();

    }

    private void initListnar() {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(binding.getRoot().getWindowToken(), 0);


        binding.tvsubmit.setOnClickListener(v -> {
            if (!binding.etRefer.getText().toString().equals("")) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("referralCode", binding.etRefer.getText().toString());
                Call<ReferRoot> call = RetrofitBuilder.create(this).enterReferCode(userId, jsonObject);
                call.enqueue(new Callback<ReferRoot>() {
                    @Override
                    public void onResponse(Call<ReferRoot> call, Response<ReferRoot> response) {
                        if (response.code() == 200) {
                            if (response.body().getStatus() == 200) {


                                adsDiffTime = Integer.parseInt(sessionManager.getStringValue(Const.ADS_TIME).equals("") ? "30" : sessionManager.getStringValue(Const.ADS_TIME));
                                showpopupCoinAd();
                                getData();


                                /* Toast.makeText(ProfileActivity.this, "Coin Added Successfully", Toast.LENGTH_SHORT).show();*/

                            } else {
                                adsDiffTime = Integer.parseInt(sessionManager.getStringValue(Const.ADS_TIME).equals("") ? "30" : sessionManager.getStringValue(Const.ADS_TIME));

                                Showpopup24Hours();
                                /* Toast.makeText(ProfileActivity.this, "Please Try after 24 hours", Toast.LENGTH_SHORT).show();*/

                            }

                        }
                        binding.etRefer.setText("");
                    }


                    @Override
                    public void onFailure(Call<ReferRoot> call, Throwable t) {
                        Log.d(TAG, "onFailure: " + t.getMessage());
                        binding.etRefer.setText("");
                    }
                });
            }
            binding.etRefer.setText("");
        });
    }

    private void showpopupCoinAd() {
        if (dialog.isShowing()) {
            time = 0;
            return;
        }
        sessionManager = new SessionManager(ProfileActivity.this);
        if (dialog != null && popupbinding != null) {
            dialog.setContentView(popupbinding.getRoot());
        }


        AdLoader adLoader = new AdLoader.Builder(ProfileActivity.this, (google != null) ? google.getJsonMemberNative() : "")
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    Log.d(TAG, "showPopup: popup native loded");
                    popupbinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) getLayoutInflater()
                                    .inflate(R.layout.native_google_popup, null);

                    ImageView imageView = adView.findViewById(R.id.ad_media);
                    Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).circleCrop().transform(new RoundedCorners(20)).into(imageView);
                    // Set other ad assets.
                    adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                    adView.setBodyView(adView.findViewById(R.id.ad_body));
                    adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));



                    // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                    ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                    // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                    // check before trying to display them.
                    if (unifiedNativeAd.getBody() == null) {
                        adView.getBodyView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getBodyView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                    }

                    if (unifiedNativeAd.getCallToAction() == null) {
                        adView.getCallToActionView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getCallToActionView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                    }




                    adView.setNativeAd(unifiedNativeAd);
                    popupbinding.flAdplaceholderbanner.removeAllViews();
                    popupbinding.flAdplaceholderbanner.addView(adView);


                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());
                        showgoogle2();
                        // Handle the failure by logging, altering the UI, and so on.
                    }

                    private void showgoogle2() {
                        AdLoader adLoader = new AdLoader.Builder(ProfileActivity.this, (google2 != null) ? google2.getJsonMemberNative() : "")
                                .forUnifiedNativeAd(unifiedNativeAd -> {
                                    // Show the ad.
                                    Log.d(TAG, "showPopup: popup native loded");
                                    popupbinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                                    UnifiedNativeAdView adView =
                                            (UnifiedNativeAdView) getLayoutInflater()
                                                    .inflate(R.layout.native_google_popup, null);

                                    ImageView imageView = adView.findViewById(R.id.ad_media);
                                    Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).circleCrop().transform(new RoundedCorners(20)).into(imageView);
                                    // Set other ad assets.
                                    adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                                    adView.setBodyView(adView.findViewById(R.id.ad_body));
                                    adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));


                                    // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                                    ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                                    // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                                    // check before trying to display them.
                                    if (unifiedNativeAd.getBody() == null) {
                                        adView.getBodyView().setVisibility(View.INVISIBLE);
                                    } else {
                                        adView.getBodyView().setVisibility(View.VISIBLE);
                                        ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                                    }

                                    if (unifiedNativeAd.getCallToAction() == null) {
                                        adView.getCallToActionView().setVisibility(View.INVISIBLE);
                                    } else {
                                        adView.getCallToActionView().setVisibility(View.VISIBLE);
                                        ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                                    }




                                    adView.setNativeAd(unifiedNativeAd);
                                    popupbinding.flAdplaceholderbanner.removeAllViews();
                                    popupbinding.flAdplaceholderbanner.addView(adView);


                                })
                                .withAdListener(new AdListener() {
                                    @Override
                                    public void onAdFailedToLoad(LoadAdError adError) {
                                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());

                                        popupbinding.flAdplaceholderbanner.setVisibility(View.GONE);
                                        showfbnative();
                                        // Handle the failure by logging, altering the UI, and so on.
                                    }

                                    private void showfbnative() {
                                        NativeAd nativeAd = new NativeAd(ProfileActivity.this, (facebook != null) ? facebook.getJsonMemberNative() : " ");
                                        NativeAdListener nativeAdListener = new NativeAdListener() {
                                            @Override
                                            public void onMediaDownloaded(Ad ad) {
                                                // Native ad finished downloading all assets
                                                Log.e("TAG", "Native ad finished downloading all assets.");
                                            }

                                            @Override
                                            public void onError(Ad ad, AdError adError) {
                                                popupbinding.nativeAdFb.setVisibility(View.GONE);
                                                popupbinding.itemBannerownad.lytad.setVisibility(View.VISIBLE);


                                                // Native ad failed to load
                                                Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
                                            }

                                            @Override
                                            public void onAdLoaded(Ad ad) {
                                                if (nativeAd == null || nativeAd != ad) {
                                                    return;
                                                }
                                                inflateAd(nativeAd);
                                                // Native ad is loaded and ready to be displayed
                                                Log.d("TAG", "Native ad is loaded and ready to be displayed!");

                                            }

                                            private void inflateAd(NativeAd nativeAd) {
                                                popupbinding.nativeAdFb.setVisibility(View.VISIBLE);
                                                nativeAd.unregisterView();

                                                // Add the Ad view into the ad container.

                                                NativeAdLayout nativeAdLayout = popupbinding.nativeAdFb;

                                                LayoutInflater inflater = LayoutInflater.from(ProfileActivity.this);
                                                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                                                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_fb_popup, nativeAdLayout, false);
                                                nativeAdLayout.addView(adView);


                                                // Create native UI using the ad metadata.
                                                com.facebook.ads.MediaView nativeAdIcon = adView.findViewById(R.id.native_ad_media);
                                                TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
                                                /*MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);*/
                                                /* TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);*/
                                                TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
                                                /* TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);*/
                                                TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

                                                // Set the Text.
                                                nativeAdTitle.setText(nativeAd.getAdvertiserName());
                                                nativeAdBody.setText(nativeAd.getAdBodyText());
                                                /*nativeAdSocialContext.setText(nativeAd.getAdSocialContext());*/
                                                nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                                                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                                                // Create a list of clickable views
                                                List<View> clickableViews = new ArrayList<>();
                                                clickableViews.add(nativeAdTitle);
                                                clickableViews.add(nativeAdCallToAction);

                                                // Register the Title and CTA button to listen for clicks.
                                                nativeAd.registerViewForInteraction(
                                                        adView, nativeAdIcon, clickableViews);
                                            }

                                            @Override
                                            public void onAdClicked(Ad ad) {

                                            }

                                            @Override
                                            public void onLoggingImpression(Ad ad) {

                                            }
                                        };

                                        nativeAd.loadAd(
                                                nativeAd.buildLoadAdConfig()
                                                        .withAdListener(nativeAdListener)
                                                        .build());

                                    }


                                })
                                .withNativeAdOptions(new NativeAdOptions.Builder()
                                        // Methods in the NativeAdOptions.Builder class can be
                                        // used here to specify individual options settings.
                                        .build())
                                .build();
                        adLoader.loadAd(new AdRequest.Builder().build());
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
        dialog.setCancelable(false);
        Glide.with(getApplicationContext())
                .load(sessionManager.getStringValue(Const.PROFILE_IMAGE)).error(R.drawable.bg_whitebtnround)
                .circleCrop()
                .into(popupbinding.imagepopup);
        popupbinding.tv1.setText("Congratulation! , " + sessionManager.getUser().getFname());
        popupbinding.textview.setText("500 coins credited in your account");
        popupbinding.tvContinue.setOnClickListener(v -> {
            isFromPopup = true;
            if (dialog.isShowing()) {
                dialog.dismiss();
                dialog.cancel();
            }
            showAds();
        });

        popupbinding.tvCencel.setOnClickListener(v -> {

            if (dialog.isShowing()) {
                dialog.dismiss();
                dialog.cancel();
            }
            showAds();

        });

        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }

    }

    private void Showpopup24Hours() {
        if (dialog.isShowing()) {
            time = 0;
            return;
        }

        sessionManager = new SessionManager(ProfileActivity.this);
        if (dialog != null && popUpShowOnActivityBinding != null) {
            dialog.setContentView(popUpShowOnActivityBinding.getRoot());
        }


        AdLoader adLoader = new AdLoader.Builder(ProfileActivity.this, (google != null) ? google.getJsonMemberNative() : "")
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    Log.d(TAG, "showPopup: popup native loded");

                    popUpShowOnActivityBinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) getLayoutInflater()
                                    .inflate(R.layout.native_google_popup, null);

                    ImageView imageView = adView.findViewById(R.id.ad_media);
                    Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).circleCrop().transform(new RoundedCorners(20)).into(imageView);
                    // Set other ad assets.
                    adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                    adView.setBodyView(adView.findViewById(R.id.ad_body));
                    adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));


                    // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                    ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                    // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                    // check before trying to display them.
                    if (unifiedNativeAd.getBody() == null) {
                        adView.getBodyView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getBodyView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                    }

                    if (unifiedNativeAd.getCallToAction() == null) {
                        adView.getCallToActionView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getCallToActionView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                    }




                    adView.setNativeAd(unifiedNativeAd);
                    popUpShowOnActivityBinding.flAdplaceholderbanner.removeAllViews();
                    popUpShowOnActivityBinding.flAdplaceholderbanner.addView(adView);


                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());
                        showgoogle2native();
                        // Handle the failure by logging, altering the UI, and so on.
                    }

                    private void showgoogle2native() {
                        AdLoader adLoader = new AdLoader.Builder(ProfileActivity.this, (google2 != null) ? google2.getJsonMemberNative() : "")
                                .forUnifiedNativeAd(unifiedNativeAd -> {
                                    // Show the ad.
                                    Log.d(TAG, "showPopup: popup native loded");

                                    popUpShowOnActivityBinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                                    UnifiedNativeAdView adView =
                                            (UnifiedNativeAdView) getLayoutInflater()
                                                    .inflate(R.layout.native_google_popup, null);

                                    ImageView imageView = adView.findViewById(R.id.ad_media);
                                    Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).circleCrop().transform(new RoundedCorners(20)).into(imageView);
                                    // Set other ad assets.
                                    adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                                    adView.setBodyView(adView.findViewById(R.id.ad_body));
                                    adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));


                                    // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                                    ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                                    // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                                    // check before trying to display them.
                                    if (unifiedNativeAd.getBody() == null) {
                                        adView.getBodyView().setVisibility(View.INVISIBLE);
                                    } else {
                                        adView.getBodyView().setVisibility(View.VISIBLE);
                                        ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                                    }

                                    if (unifiedNativeAd.getCallToAction() == null) {
                                        adView.getCallToActionView().setVisibility(View.INVISIBLE);
                                    } else {
                                        adView.getCallToActionView().setVisibility(View.VISIBLE);
                                        ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                                    }




                                    adView.setNativeAd(unifiedNativeAd);
                                    popUpShowOnActivityBinding.flAdplaceholderbanner.removeAllViews();
                                    popUpShowOnActivityBinding.flAdplaceholderbanner.addView(adView);


                                })
                                .withAdListener(new AdListener() {
                                    @Override
                                    public void onAdFailedToLoad(LoadAdError adError) {
                                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());
                                        showfbnative();
                                        popUpShowOnActivityBinding.flAdplaceholderbanner.setVisibility(View.GONE);
                                        // Handle the failure by logging, altering the UI, and so on.
                                    }

                                    private void showfbnative() {
                                        NativeAd nativeAd = new NativeAd(ProfileActivity.this, (facebook != null) ? facebook.getJsonMemberNative() : " ");
                                        NativeAdListener nativeAdListener = new NativeAdListener() {
                                            @Override
                                            public void onMediaDownloaded(Ad ad) {
                                                // Native ad finished downloading all assets
                                                Log.e("TAG", "Native ad finished downloading all assets.");
                                            }

                                            @Override
                                            public void onError(Ad ad, AdError adError) {

                                                popUpShowOnActivityBinding.nativeAdFb.setVisibility(View.GONE);
                                                popUpShowOnActivityBinding.itemBannerownad.lytad.setVisibility(View.VISIBLE);
                                                // Native ad failed to load
                                                Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
                                            }

                                            @Override
                                            public void onAdLoaded(Ad ad) {
                                                if (nativeAd == null || nativeAd != ad) {
                                                    return;
                                                }
                                                inflateAd(nativeAd);
                                                // Native ad is loaded and ready to be displayed
                                                Log.d("TAG", "Native ad is loaded and ready to be displayed!");

                                            }

                                            private void inflateAd(NativeAd nativeAd) {
                                                popUpShowOnActivityBinding.nativeAdFb.setVisibility(View.VISIBLE);
                                                nativeAd.unregisterView();

                                                // Add the Ad view into the ad container.

                                                NativeAdLayout nativeAdLayout = popUpShowOnActivityBinding.nativeAdFb;

                                                LayoutInflater inflater = LayoutInflater.from(ProfileActivity.this);
                                                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                                                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_fb_popup, nativeAdLayout, false);
                                                nativeAdLayout.addView(adView);

                                                // Create native UI using the ad metadata.
                                                ImageView nativeAdIcon = adView.findViewById(R.id.ad_logo);
                                                com.facebook.ads.MediaView nativeAdmedia = adView.findViewById(R.id.ad_media);
                                                TextView nativeAdTitle = adView.findViewById(R.id.ad_headline);
                                                /*MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);*/
                                                /* TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);*/
                                                TextView nativeAdBody = adView.findViewById(R.id.ad_body);
                                                /* TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);*/
                                                TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

                                                // Set the Text.
                                                nativeAdTitle.setText(nativeAd.getAdvertiserName());
                                                nativeAdBody.setText(nativeAd.getAdBodyText());

                                                /*nativeAdSocialContext.setText(nativeAd.getAdSocialContext());*/
                                                nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                                                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                                                // Create a list of clickable views
                                                List<View> clickableViews = new ArrayList<>();
                                                clickableViews.add(nativeAdTitle);
                                                clickableViews.add(nativeAdCallToAction);

                                                // Register the Title and CTA button to listen for clicks.
                                                nativeAd.registerViewForInteraction(
                                                        adView, nativeAdmedia, nativeAdIcon, clickableViews);
                                            }

                                            @Override
                                            public void onAdClicked(Ad ad) {

                                            }

                                            @Override
                                            public void onLoggingImpression(Ad ad) {

                                            }
                                        };

                                        nativeAd.loadAd(
                                                nativeAd.buildLoadAdConfig()
                                                        .withAdListener(nativeAdListener)
                                                        .build());
                                    }


                                })
                                .withNativeAdOptions(new NativeAdOptions.Builder()
                                        // Methods in the NativeAdOptions.Builder class can be
                                        // used here to specify individual options settings.
                                        .build())
                                .build();
                        adLoader.loadAd(new AdRequest.Builder().build());
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
        dialog.setCancelable(false);
        Glide.with(getApplicationContext())
                .load(sessionManager.getStringValue(Const.PROFILE_IMAGE)).error(R.drawable.bg_whitebtnround)
                .circleCrop()
                .into(popUpShowOnActivityBinding.imagepopup);

        popUpShowOnActivityBinding.tv1.setText("Sorry ! , " + sessionManager.getUser().getFname());
        popUpShowOnActivityBinding.textview.setText("Try After 24 Hours");
        popUpShowOnActivityBinding.tvContinue.setOnClickListener(v -> {
            isFromPopup = true;
            if (dialog.isShowing()) {
                dialog.dismiss();

            }
            showAds();
        });
        popUpShowOnActivityBinding.tvCencel.setOnClickListener(v -> {
            if (dialog.isShowing()) {
                dialog.dismiss();

            }
            showAds();

        });

        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }

    }

    private void showAds() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else if (ownLoded) {

            binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
            binding.itemIntrustial.closeBtn.setOnClickListener(v -> binding.itemIntrustial.rlInstastial.setVisibility(View.GONE));

           /* binding.lytOwnInter.setVisibility(View.VISIBLE);
            sendImpression(this, adid);
            binding.lytOwnInter.setOnClickListener(v -> {
                Intent intent = new Intent(ProfileActivity.this, WebActivity.class);
                intent.putExtra("ADID", String.valueOf(adid));
                intent.putExtra(WEBSITE, ownWebUrl);
                intent.putExtra("type", "ads");
                startActivity(intent);
            });
            binding.imgCloseInter.setOnClickListener(v -> finish());*/
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Anylitecs.addUser(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Anylitecs.removeSesson(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Anylitecs.removeSesson(this);
        Log.d(TAG, "onPause: ");
    }

    private void loadAds() {
        MobileAds.initialize(this, initializationStatus -> {
        });
        AudienceNetworkAds.initialize(this);

        getOwnAds();
        fetched = true;
        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {

            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
            }

            setBanner();
            setNative();

            /* mAd = new GGInterstitialAd(this, (google != null) ? google.getInterstitial() : "");*/

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId((google != null) ? google.getInterstitial() : "");
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            interAdListnear();

            mInterstitialAd2 = new InterstitialAd(this);
            mInterstitialAd2.setAdUnitId((google2 != null) ? google2.getInterstitial() : "");
            mInterstitialAd2.loadAd(new AdRequest.Builder().build());
            interAdListnear2();

            fbInterAdListnear();
            interstitialAdfb = new com.facebook.ads.InterstitialAd(this, (facebook != null) ? facebook.getInterstitial() : "");
            interstitialAdfb.loadAd(
                    interstitialAdfb.buildLoadAdConfig()
                            .withAdListener(interstitialAdListener)
                            .build());

            fbBannerAdListnear();
            adViewfb = new com.facebook.ads.AdView(this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
            LinearLayout adContainer = findViewById(R.id.banner_container);
            adContainer.addView(adViewfb);
            adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(adListener).build());
        } else {
            Toast.makeText(this, "ads not downloded ", Toast.LENGTH_SHORT).show();
        }


        String coin = sessionManager.getStringValue(Const.REWARD_COINS);
        //String text="Refer your friends and get more "+coin+" coins";


        String text1 = "Refer your friends and get more ";

        String text2 = String.valueOf(coin);

        String text3 = " coins";


        binding.tvrefertext1.setText(text1);
        binding.tvrefertext2.setText(text2);
        binding.tvrefertext3.setText(text3);

    }

    private void interAdListnear2() {
        mInterstitialAd2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
//mm
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                if (isFromPopup) {
                    isFromPopup = false;
                } else {
                    finish();
                }
                loadAds();
                // Code to be executed when the interstitial ad is closed.
            }
        });

    }

    private void setNative() {
        AdLoader adLoader = new AdLoader.Builder(this, (google != null) ? google.getJsonMemberNative() : "")
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    Log.d(TAG, "showPopup: popup native loded");
                    binding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) getLayoutInflater()
                                    .inflate(R.layout.native_profile, null);

                    ImageView imageView = adView.findViewById(R.id.ad_media);
                    Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).transform(new RoundedCorners(20)).into(imageView);
                    // Set other ad assets.
                    adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                    adView.setBodyView(adView.findViewById(R.id.ad_body));
                    adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
                    adView.setIconView(adView.findViewById(R.id.ad_app_icon));


                    // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                    ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                    // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                    // check before trying to display them.

                    if (unifiedNativeAd.getBody() == null) {
                        adView.getBodyView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getBodyView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                    }

                    if (unifiedNativeAd.getCallToAction() == null) {
                        adView.getCallToActionView().setVisibility(View.INVISIBLE);
                    } else {
                        adView.getCallToActionView().setVisibility(View.VISIBLE);
                        ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                    }

                    Log.d(TAG, "setNative: logo  " + unifiedNativeAd.getIcon());
                    if (unifiedNativeAd.getIcon() == null) {
                        adView.getIconView().setVisibility(View.GONE);
                    } else {
                        ((ImageView) adView.getIconView()).setImageDrawable(
                                unifiedNativeAd.getIcon().getDrawable());
                        adView.getIconView().setVisibility(View.VISIBLE);
                    }


                    adView.setNativeAd(unifiedNativeAd);
                    binding.flAdplaceholderbanner.removeAllViews();
                    binding.flAdplaceholderbanner.addView(adView);


                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        setgoogle2native();
                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());

                        // Handle the failure by logging, altering the UI, and so on.
                    }

                    private void setgoogle2native() {
                        AdLoader adLoader = new AdLoader.Builder(ProfileActivity.this, (google2 != null) ? google2.getJsonMemberNative() : "")
                                .forUnifiedNativeAd(unifiedNativeAd -> {
                                    // Show the ad.
                                    Log.d(TAG, "showPopup: popup native loded");
                                    binding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                                    UnifiedNativeAdView adView =
                                            (UnifiedNativeAdView) getLayoutInflater()
                                                    .inflate(R.layout.native_profile, null);

                                    ImageView imageView = adView.findViewById(R.id.ad_media);
                                    Glide.with(getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).transform(new RoundedCorners(20)).into(imageView);
                                    // Set other ad assets.
                                    adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
                                    adView.setBodyView(adView.findViewById(R.id.ad_body));
                                    adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
                                    adView.setIconView(adView.findViewById(R.id.ad_app_icon));


                                    // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
                                    ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


                                    // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
                                    // check before trying to display them.

                                    if (unifiedNativeAd.getBody() == null) {
                                        adView.getBodyView().setVisibility(View.INVISIBLE);
                                    } else {
                                        adView.getBodyView().setVisibility(View.VISIBLE);
                                        ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
                                    }

                                    if (unifiedNativeAd.getCallToAction() == null) {
                                        adView.getCallToActionView().setVisibility(View.INVISIBLE);
                                    } else {
                                        adView.getCallToActionView().setVisibility(View.VISIBLE);
                                        ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
                                    }

                                    Log.d(TAG, "setNative: logo  " + unifiedNativeAd.getIcon());
                                    if (unifiedNativeAd.getIcon() == null) {
                                        adView.getIconView().setVisibility(View.GONE);
                                    } else {
                                        ((ImageView) adView.getIconView()).setImageDrawable(
                                                unifiedNativeAd.getIcon().getDrawable());
                                        adView.getIconView().setVisibility(View.VISIBLE);
                                    }


                                    adView.setNativeAd(unifiedNativeAd);
                                    binding.flAdplaceholderbanner.removeAllViews();
                                    binding.flAdplaceholderbanner.addView(adView);


                                })
                                .withAdListener(new AdListener() {
                                    @Override
                                    public void onAdFailedToLoad(LoadAdError adError) {
                                        setfbnative();
                                        binding.flAdplaceholderbanner.setVisibility(View.GONE);
                                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());

                                    }

                                    private void setfbnative() {

                                        NativeAd nativeAd = new NativeAd(ProfileActivity.this, (facebook != null) ? facebook.getJsonMemberNative() : " ");
                                        NativeAdListener nativeAdListener = new NativeAdListener() {
                                            @Override
                                            public void onMediaDownloaded(Ad ad) {
                                                // Native ad finished downloading all assets
                                                Log.e("TAG", "Native ad finished downloading all assets.");
                                            }

                                            @Override
                                            public void onError(Ad ad, AdError adError) {
                                                binding.nativeAdFb.setVisibility(View.GONE);
                                                if (ownLoded) {
                                                    Anylitecs.sendImpression(ProfileActivity.this, ownAds.getId());
                                                    binding.itemNative.rlOnNative.setVisibility(View.VISIBLE);
                                                }

                                                // Native ad failed to load
                                                Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
                                            }

                                            @Override
                                            public void onAdLoaded(Ad ad) {
                                                if (nativeAd == null || nativeAd != ad) {
                                                    return;
                                                }
                                                inflateAd(nativeAd);
                                                // Native ad is loaded and ready to be displayed
                                                Log.d("TAG", "Native ad is loaded and ready to be displayed!");

                                            }

                                            private void inflateAd(NativeAd nativeAd) {
                                                binding.nativeAdFb.setVisibility(View.VISIBLE);
                                                nativeAd.unregisterView();
                                                // Add the Ad view into the ad container.
                                                NativeAdLayout nativeAdLayout = binding.nativeAdFb;

                                                LayoutInflater inflater = LayoutInflater.from(ProfileActivity.this);
                                                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                                                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_fb_profile, nativeAdLayout, false);
                                                nativeAdLayout.addView(adView);

                                                ImageView nativeAdIcon = adView.findViewById(R.id.ad_app_icon);
                                                com.facebook.ads.MediaView nativemediaview = adView.findViewById(R.id.ad_media);
                                                TextView nativeAdTitle = adView.findViewById(R.id.ad_headline);
                                                /*MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);*/
                                                /* TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);*/
                                                TextView nativeAdBody = adView.findViewById(R.id.ad_body);
                                                /* TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);*/
                                                TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

                                                // Set the Text.
                                                nativeAdTitle.setText(nativeAd.getAdHeadline());
                                                nativeAdBody.setText(nativeAd.getAdBodyText());
                                                /*nativeAdSocialContext.setText(nativeAd.getAdSocialContext());*/
                                                nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                                                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());
                                                // Create a list of clickable views
                                                List<View> clickableViews = new ArrayList<>();
                                                clickableViews.add(nativeAdTitle);
                                                clickableViews.add(nativeAdCallToAction);
                                                // Register the Title and CTA button to listen for clicks.
                                                nativeAd.registerViewForInteraction(
                                                        adView, nativemediaview, nativeAdIcon, clickableViews);
                                            }

                                            @Override
                                            public void onAdClicked(Ad ad) {

                                            }

                                            @Override
                                            public void onLoggingImpression(Ad ad) {

                                            }
                                        };

                                        nativeAd.loadAd(
                                                nativeAd.buildLoadAdConfig()
                                                        .withAdListener(nativeAdListener)
                                                        .build());

                                    }

                                })
                                .withNativeAdOptions(new NativeAdOptions.Builder()
                                        // Methods in the NativeAdOptions.Builder class can be
                                        // used here to specify individual options settings.
                                        .build())
                                .build();
                        adLoader.loadAd(new AdRequest.Builder().build());
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }


    private void getData() {
        Log.d(TAG, "getData: data req send");
        binding.pd.setVisibility(View.VISIBLE);
        Call<ProfileRoot> call = RetrofitBuilder.create(this).getUser(userId);
        call.enqueue(new Callback<ProfileRoot>() {
            @Override
            public void onResponse(Call<ProfileRoot> call, Response<ProfileRoot> response) {
                Log.d(TAG, "onResponse: " + response.code());
                Log.d(TAG, "onResponse: " + response.body().getStatus());
                if (response.code() == 200 && response.body().getStatus() == 200 && response.body().getData() != null) {
                    user = response.body().getData();
                    binding.setUser(user);

                    binding.username.setText("@".concat(user.getUsername()));
                    String fname = user.getFname().substring(0, 1).toUpperCase().concat(user.getFname().substring(1));

                    binding.tvName.setText(fname);
                    setUserImage();

                    binding.tvcopy.setOnClickListener(v -> {
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("refercode", binding.tvrefercode.getText().toString());
                        clipboard.setPrimaryClip(clip);
                        Toast.makeText(ProfileActivity.this, "Copied", Toast.LENGTH_SHORT).show();
                    });
                }

                binding.pd.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ProfileRoot> call, Throwable t) {
                binding.pd.setVisibility(View.GONE);
                Log.d(TAG, "onFailure: " + t.toString());
            }
        });

    }

    private void setUserImage() {
        Log.d(TAG, "setUserImage: " + sessionManager.getStringValue(Const.PROFILE_IMAGE));
        Glide.with(getApplicationContext())
                .load(sessionManager.getStringValue(Const.PROFILE_IMAGE)).error(R.drawable.bg_whitebtnround)
                .placeholder(R.drawable.bg_whitebtnround)
                .circleCrop()
                .into(binding.imgprofile);
        if (sessionManager.getStringValue(Const.PROFILE_IMAGE).equals("null")) {
            Log.d(TAG, "setUserImage:22 " + sessionManager.getStringValue(Const.PROFILE_IMAGE));
            binding.profilechar.setVisibility(View.VISIBLE);
            binding.profilechar.setText(String.valueOf(user.getFname().charAt(0)).toUpperCase());
        }
    }

    private void getOwnAds() {
        RetrofitBuilder.create(this).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                    ownLoded = true;
                    ownAds = response.body().getData().get(0);

                    String[] c1 = ownAds.getColor().split("f");
                    String color = c1[0];

                    //TODO banner ads
                    binding.itemBannerownad.tvtitle.setText(ownAds.getName());
                    binding.itemBannerownad.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemBannerownad.imglogo);
                    binding.itemBannerownad.tvbtn.setText(ownAds.getBtnText());
                    binding.itemBannerownad.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(ProfileActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemBannerownad.tvdes.setText(ownAds.getDescription());

                    //TODO itempopup
                    popupbinding.itemBannerownad.tvtitle.setText(ownAds.getName());
                    popupbinding.itemBannerownad.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(popupbinding.itemBannerownad.imglogo);
                    popupbinding.itemBannerownad.tvbtn.setText(ownAds.getBtnText());
                    popupbinding.itemBannerownad.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(ProfileActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    popupbinding.itemBannerownad.tvdes.setText(ownAds.getDescription());

                    //TODO itempopup2
                    popUpShowOnActivityBinding.itemBannerownad.tvtitle.setText(ownAds.getName());
                    popUpShowOnActivityBinding.itemBannerownad.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(popUpShowOnActivityBinding.itemBannerownad.imglogo);
                    popUpShowOnActivityBinding.itemBannerownad.tvbtn.setText(ownAds.getBtnText());
                    popUpShowOnActivityBinding.itemBannerownad.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(ProfileActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    popUpShowOnActivityBinding.itemBannerownad.tvdes.setText(ownAds.getDescription());

                    //TODO native ads

                    binding.itemNative.adHeadline.setText(ownAds.getName());
                    binding.itemNative.llColor.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemNative.adAppIcon);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getImage()).centerCrop().into(binding.itemNative.adMedia);
                    binding.itemNative.adCallToAction.setText(ownAds.getBtnText());
                    binding.itemNative.adCallToAction.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(ProfileActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemNative.adBody.setText(ownAds.getDescription());

                    //TODO instastial ads
                    Log.d(TAG, "onResponse: " + BuildConfig.TMDB_API_KEY + ownAds.getImage());

                    binding.itemIntrustial.tvTitle.setText(ownAds.getName());
                    binding.itemIntrustial.rlInstastial.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemIntrustial.imgLogo);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getImage()).centerCrop().into(binding.itemIntrustial.ivBg);
                    binding.itemIntrustial.goWeb.setText(ownAds.getBtnText());
                    binding.itemIntrustial.goWeb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(ProfileActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemIntrustial.tvBody.setText(ownAds.getDescription());

                }
            }

            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {
                binding.itemIntrustial.rlInstastial.setVisibility(View.GONE);
                binding.itemBannerownad.lytad.setVisibility(View.GONE);
                binding.itemNative.rlOnNative.setVisibility(View.GONE);

            }

        });
    }

    private void setBanner() {
        LinearLayout adContainer = findViewById(R.id.banner_container);

        adView = new AdView(this);
        if (google != null) {
            Log.d(TAG, "setBanner: " + google.getBanner());
        } else {
            Log.d(TAG, "setBanner: google is null");
        }

        adView.setAdUnitId((google != null) ? google.getBanner() : " ");
        adContainer.addView(adView);
        AdRequest adRequest =
                new AdRequest.Builder()
                        .build();
        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);


        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Log.d(TAG, "onAdFailedToLoad: banner " + loadAdError);


                adContainer.removeAllViews();
                setgooglebanner2();
            }

            private void setgooglebanner2() {
                LinearLayout adContainer = findViewById(R.id.banner_container);

                adView = new AdView(ProfileActivity.this);
                if (google2 != null) {
                    Log.d(TAG, "setBanner: " + google2.getBanner());
                } else {
                    Log.d(TAG, "setBanner: google is null");
                }

                adView.setAdUnitId((google2 != null) ? google2.getBanner() : " ");
                adContainer.addView(adView);
                AdRequest adRequest =
                        new AdRequest.Builder()
                                .build();
                AdSize adSize = getAdSize();
                adView.setAdSize(adSize);
                adView.loadAd(adRequest);


                adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        Log.d(TAG, "onAdFailedToLoad: banner " + loadAdError);
                        adContainer.removeAllViews();
                        setFbBanner();
                    }
                });
            }

            private void setFbBanner() {
                adViewfb = new com.facebook.ads.AdView(ProfileActivity.this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);

                adContainer.addView(adViewfb);
                adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.d(TAG, "onError: fb " + adError.getErrorMessage());
                        if (ownLoded) {
                            Anylitecs.sendImpression(ProfileActivity.this, ownAds.getId());
                            binding.itemBannerownad.lytad.setVisibility(View.VISIBLE);
                        }

                       /* binding.imgOwnAd.setVisibility(View.VISIBLE);
                        sendImpression(ProfileActivity.this, adid);
                        binding.imgOwnAd.setOnClickListener(v -> {
                            Intent intent = new Intent(ProfileActivity.this, WebActivity.class);
                            intent.putExtra("ADID", String.valueOf(adid));
                            intent.putExtra(WEBSITE, ownWebUrl);
                            intent.putExtra("type", "ads");
                            startActivity(intent);
                        });*/

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        Log.d(TAG, "onAdLoaded: facebbok");
//mm
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
//mm
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
//mm
                    }
                }).build());
            }

        });
    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void interAdListnear() {

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
//mm
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                if (isFromPopup) {
                    isFromPopup = false;

                } else {
                    finish();
                }
                loadAds();
                // Code to be executed when the interstitial ad is closed.
            }
        });


    }

    private void fbInterAdListnear() {
        interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                if (isFromPopup) {
                    isFromPopup = false;

                } else {
                    finish();
                }
                loadAds();
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {


                // Interstitial ad is loaded and ready to be displayed
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                // Show the ad
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        };
    }

    private void fbBannerAdListnear() {
        adListener = new com.facebook.ads.AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback

            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
            }
        };
    }


    @Override
    public void onBackPressed() {

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else if (ownLoded) {
            Anylitecs.sendImpression(ProfileActivity.this, ownAds.getId());
            binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
            binding.itemIntrustial.closeBtn.setOnClickListener(v -> finish());

           /* binding.lytOwnInter.setVisibility(View.VISIBLE);
            sendImpression(this, adid);
            binding.lytOwnInter.setOnClickListener(v -> {
                Intent intent = new Intent(ProfileActivity.this, WebActivity.class);
                intent.putExtra("ADID", String.valueOf(adid));
                intent.putExtra(WEBSITE, ownWebUrl);
                intent.putExtra("type", "ads");
                startActivity(intent);
            });
            binding.imgCloseInter.setOnClickListener(v -> finish());*/
        } else {
            finish();
        }


    }

    public void onClickBack(View view) {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else if (ownLoded) {
            Anylitecs.sendImpression(ProfileActivity.this, ownAds.getId());
            binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
            binding.itemIntrustial.closeBtn.setOnClickListener(v -> finish());

            /*binding.lytOwnInter.setVisibility(View.VISIBLE);
            sendImpression(this, adid);
            binding.lytOwnInter.setOnClickListener(v -> {
                Intent intent = new Intent(ProfileActivity.this, WebActivity.class);
                intent.putExtra("ADID", String.valueOf(adid));
                intent.putExtra(WEBSITE, ownWebUrl);
                intent.putExtra("type", "ads");
                startActivity(intent);
            });
            binding.imgCloseInter.setOnClickListener(v -> finish());*/
        } else {
            finish();
        }
    }

    public void onclickShare(View view) {
        final String appLink = "\nhttps://play.google.com/store/apps/details?id=" + getPackageName();
        Intent sendInt = new Intent(Intent.ACTION_SEND);
        sendInt.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sendInt.putExtra(Intent.EXTRA_TEXT, "Humile - live video chat  Download Now  " + appLink);
        sendInt.setType("text/plain");
        startActivity(Intent.createChooser(sendInt, "Share"));
    }

    public void onClickCoin(View view) {
        startActivity(new Intent(this, EarnActivity.class));
    }
}