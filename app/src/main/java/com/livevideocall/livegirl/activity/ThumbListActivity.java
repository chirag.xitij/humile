package com.livevideocall.livegirl.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.gson.Gson;
import com.greedygame.core.adview.GGAdview;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.adapters.AdapterVideos;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.CountryRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.models.ThumbRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.livevideocall.livegirl.activity.EarnActivity.sendImpression;

public class ThumbListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String WEBSITE = "WEBSITE";
    RecyclerView recyclerView;
    AdView adView;
    ProgressBar pd;
    SwipeRefreshLayout swipeRefreshLayout;
    int count = 2;
    private CountryRoot.Datum model;
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Facebook facebook;
    private FragmentActivity context;
    private String ownAdBannerUrl;
    private com.facebook.ads.AdView adViewfb;
    private ImageView imgOwnAd;
    private String adid;
    private String ownWebUrl;
    private AdapterVideos adapterVideos;
    private AdvertisementRoot.Google2 google2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thumb_list);

        Intent intent = getIntent();
        String cid = intent.getStringExtra("cid");
        if (cid != null && !cid.equals("")) {
            CountryRoot.Datum datum = new Gson().fromJson(cid, CountryRoot.Datum.class);
            if (datum != null) {
                model = datum;
                initView();
            }
        }

    }

    private void initView() {
        recyclerView = findViewById(R.id.rvVideos);
        imgOwnAd = findViewById(R.id.imgOwnAd);
        pd = findViewById(R.id.pd);
        swipeRefreshLayout = findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(this);


        pd.setVisibility(View.VISIBLE);
        initMain();
    }

    private void initMain() {


        if (model != null && model.get_id() != null) {
            Call<ThumbRoot> call = RetrofitBuilder.create(this).getThumbs(model.get_id());
            call.enqueue(new Callback<ThumbRoot>() {
                @Override
                public void onResponse(Call<ThumbRoot> call, Response<ThumbRoot> response) {

                    if (response.code() == 200 && !response.body().getData().isEmpty()) {


                        adapterVideos = new AdapterVideos(model.get_id());
                        adapterVideos.addData(response.body().getData());
                        recyclerView.setAdapter(adapterVideos);


//                        for (int i = 0; i < response.body().getData().size(); i++) {
//                            if (i != 0 && i % 2 == 0) {
                        setNativeLogic();
                        Log.d("TAG", "onResponse: natvie fetch");
//                            }
//                        }


                        Log.d("TAG", "onResponse:ccccc  size " + response.body().getData().size());
                    }

                    swipeRefreshLayout.setRefreshing(false);
                    pd.setVisibility(View.GONE);
                    findViewById(R.id.pd).setVisibility(View.GONE);
                }

                private void setNativeLogic() {
                    GGAdview bannerUnit = new GGAdview(context);
                    bannerUnit.setUnitId((google != null) ? google.getJsonMemberNative() : " ");  //Replace with your Ad Unit ID here
                    // bannerUnit.setUnitId("AD_UNIT_ID_HERE");
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
/*
                    bannerUnit.loadAd(new AdLoadCallback() {
                                          @Override
                                          public void onReadyForRefresh() {
                                              Log.d("GGADS1", "Ad Ready for refresh");
                                          }

                                          @Override
                                          public void onUiiClosed() {
                                              Log.d("GGADS1", "Uii closed");
                                          }

                                          @Override
                                          public void onUiiOpened() {
                                              Log.d("GGADS1", "Uii Opened");
                                          }

                                          @Override
                                          public void onAdLoadFailed(AdRequestErrors cause) {
                                              Log.d("GGADS1", "Ad Load Failed " + cause);
                                          }

                                          @Override
                                          public void onAdLoaded() {
                                              Log.d("GGADS1", "Ad Loaded");
                                              adapterVideos.addAdsOBJ(bannerUnit, count);

                                              if (count <= adapterVideos.data.size()) {
                                                  count += 3;
                                                  setNativeLogic();
                                              }
                                              //setNativeLogic();
                                          }
                                      }
                    );
*/


                }

                @Override
                public void onFailure(Call<ThumbRoot> call, Throwable t) {
//ll
                }
            });

        }
        getOwnAds();
        SessionManager sessionManager = new SessionManager(this);
        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {


            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
            }
            setBanner();
        }
    }

    private void setBanner() {
        LinearLayout adContainer = findViewById(R.id.banner_container);
        adView = new AdView(this);

        adView.setAdUnitId((google != null) ? google.getBanner() : "");
        adContainer.addView(adView);
        AdRequest adRequest =
                new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();
        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);


        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Log.d("TAG", "onAdFailedToLoad: banner " + loadAdError);
                adContainer.removeAllViews();
                setgooglebanner2();
            }


       /* GGAdview bannerUnit = new GGAdview(this);
        bannerUnit.setUnitId((google != null) ? google.getBanner() : " ");  //Replace with your Ad Unit ID here
        bannerUnit.setAdsMaxHeight(250);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 200);
        adContainer.addView(bannerUnit, layoutParams);
        bannerUnit.loadAd(new AdLoadCallback() {

            @Override
            public void onAdLoadFailed(@NotNull AdRequestErrors adRequestErrors) {

                setFbBanner();
            }

            private void setFbBanner() {
                adViewfb = new com.facebook.ads.AdView(EarnActivity.this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);

                adContainer.addView(adViewfb);
                adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.d(TAG, "onError: fb " + adError.getErrorMessage());
                        binding.imgOwnAd.setVisibility(View.VISIBLE);
                        sendImpression(EarnActivity.this, adid);
                        binding.imgOwnAd.setOnClickListener(v -> {
                            Intent intent = new Intent(EarnActivity.this, WebActivity.class);
                            intent.putExtra("ADID", String.valueOf(adid));
                            intent.putExtra(WEBSITE, ownWebUrl);
                            intent.putExtra("type", "ads");
                            startActivity(intent);
                        });

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
//mm
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
//mm
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
//mm
                    }
                }).build());
            }


            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onReadyForRefresh() {

            }

            @Override
            public void onUiiOpened() {
            }

            @Override
            public void onUiiClosed() {

            }

        });*/

            private void setgooglebanner2() {
                LinearLayout adContainer = findViewById(R.id.banner_container);
                adView = new AdView(ThumbListActivity.this);

                adView.setAdUnitId((google2 != null) ? google2.getBanner() : "");
                adContainer.addView(adView);
                AdRequest adRequest =
                        new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                                .build();
                AdSize adSize = getAdSize();
                adView.setAdSize(adSize);
                adView.loadAd(adRequest);


                adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        Log.d("TAG", "onAdFailedToLoad: banner " + loadAdError);
                        adContainer.removeAllViews();
                        setFbBanner();
                    }
                });

            }

            private void setFbBanner() {
                adViewfb = new com.facebook.ads.AdView(ThumbListActivity.this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);

                adContainer.addView(adViewfb);
                adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.d("TAG", "onError: fb " + adError.getErrorMessage());
                        imgOwnAd.setVisibility(View.VISIBLE);
                        sendImpression(ThumbListActivity.this, adid);
                        imgOwnAd.setOnClickListener(v -> {
                            Intent intent = new Intent(ThumbListActivity.this, WebActivity.class);
                            intent.putExtra("ADID", String.valueOf(adid));
                            intent.putExtra(WEBSITE, ownWebUrl);
                            intent.putExtra("type", "ads");
                            startActivity(intent);
                        });

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
//mm
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
//mm
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
//mm
                    }
                }).build());
            }

        });
    }


    private AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void getOwnAds() {
        RetrofitBuilder.create(ThumbListActivity.this).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {


                }
            }

            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {
///mm
            }
        });
    }

    @Override
    public void onRefresh() {

    }
}