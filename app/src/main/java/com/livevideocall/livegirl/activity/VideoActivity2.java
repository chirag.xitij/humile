package com.livevideocall.livegirl.activity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.livevideocall.livegirl.Anylitecs;
import com.livevideocall.livegirl.BuildConfig;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.adapters.BottomViewPagerAdapter;
import com.livevideocall.livegirl.adapters.CommentAdapter;
import com.livevideocall.livegirl.adapters.EmojiAdapter;
import com.livevideocall.livegirl.databinding.ActivityVideo2Binding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.CommentRoot;
import com.livevideocall.livegirl.models.EmojiIconRoot;
import com.livevideocall.livegirl.models.EmojicategoryRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.models.ThumbRoot;
import com.livevideocall.livegirl.models.UserRoot;
import com.livevideocall.livegirl.models.VideoRoot;
import com.livevideocall.livegirl.popup.DefaultPopup;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import jp.wasabeef.glide.transformations.BlurTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.animation.ValueAnimator.INFINITE;
import static com.google.android.exoplayer2.Player.STATE_BUFFERING;
import static com.google.android.exoplayer2.Player.STATE_ENDED;
import static com.google.android.exoplayer2.Player.STATE_IDLE;
import static com.google.android.exoplayer2.Player.STATE_READY;

public class VideoActivity2 extends AppCompatActivity {
    private static final String TAG = "vid2===";
    private static final int SHEET_OPEN = 1;
    private static final int SHEET_CLOSE = 2;
    private final List<EmojicategoryRoot.Datum> finelCategories = new ArrayList<>();
    private final CommentAdapter commentAdapter = new CommentAdapter();
    ActivityVideo2Binding binding;
    SessionManager sessionManager;
    int count = 0;
    Random rand = new Random();
    private String userId;
    private int adsDiffTime;
    private ThumbRoot.Datum girl;
    private EmojicategoryRoot.Datum tempobj;
    private EmojiAdapter emojiAdapter;
    private int rendtemp = 100;
    private Animation animZoomIn;
    private boolean cleartext = false;
    private String countryid;
    private String videoURL;
    private SimpleExoPlayer player;
    private List<CommentRoot.Datum> comments = new ArrayList<>();
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Google2 google2;
    private AdvertisementRoot.Facebook facebook;
    private InterstitialAd mInterstitialAd1;
    private InterstitialAd mInterstitialAd2;
    private com.facebook.ads.InterstitialAd interstitialAdfb;
    private int time = 0;
    private boolean startTimer = false;
    private boolean isPopupShowing = false;
    private boolean isFirstTime = true;
    private boolean isAdShowing = false;
    private boolean ownLoded = false;
    private OwnAdsRoot.DataItem ownAds;
    private boolean isFromClose = false;
    private boolean isFromCoin = false;
    private boolean keyboardOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_video2);
        sessionManager = new SessionManager(this);
        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            userId = sessionManager.getUser().get_id();
            binding.tvusercoins.setText(String.valueOf(sessionManager.getUser().getCoin()));

        }

        getOwnAds();
        getAdsCredential();
        initSettings();
        Intent intent = getIntent();
        if (intent != null) {
            String cid = intent.getStringExtra("cid");
            String model = intent.getStringExtra("model");
            if (model != null && !model.equals("")) {
                ThumbRoot.Datum object = new Gson().fromJson(model, ThumbRoot.Datum.class);
                if (object != null) {
                    girl = object;
                    Log.d(TAG, "getIntentdata:image " + object.getImage());
                    Log.d(TAG, "getIntentdata:name " + object.getName());
                    String s = String.valueOf(object.getName().charAt(0)).toUpperCase();
                    binding.tvName.setText(s.concat(object.getName().substring(1)));
                    Glide.with(getApplicationContext())
                            .load(new SessionManager(VideoActivity2.this).getStringValue(Const.IMAGE_URL) + girl.getImage())
                            .transform(new BlurTransformation(25), new CenterCrop())
                            .circleCrop()
                            .into(binding.imgthumb);
                    Glide.with(getApplicationContext())
                            .load(new SessionManager(VideoActivity2.this).getStringValue(Const.IMAGE_URL) + girl.getImage())
                            .circleCrop()
                            .into(binding.imgprofile);

                    initView();
                    initListnear();
                    addRandomCoins();
                    if (cid != null && !cid.equals("")) {
                        countryid = cid;
                        getVideo(cid);
                        getComments(cid);
                    }
                }

            }
        }


    }

    private void addRandomCoins() {

        Handler handler2 = new Handler();
        Runnable runnable2 = new Runnable() {

            @Override
            public void run() {
                try {
                    int randInt1 = rand.nextInt((100 - 10) + 1) + 10;

                    rendtemp = rendtemp + randInt1;


                    double coin1;
                    if (rendtemp >= 1000) {
                        coin1 = (double) rendtemp / 1000;
                        DecimalFormat df = new DecimalFormat("#.##");

                        binding.tvCoin.setText(df.format(coin1).concat("K"));
                    } else {
                        coin1 = rendtemp;
                        binding.tvCoin.setText(String.valueOf((int) coin1));
                    }
                    Log.d(TAG, "run: coin " + coin1);
                    Log.d(TAG, "run: rendm  " + rendtemp);
                    handler2.postDelayed(this, 5000);

                } catch (IllegalStateException ed) {
                    ed.printStackTrace();
                }
            }
        };

        handler2.postDelayed(runnable2, 5000);

    }

    private void getOwnAds() {
        ownLoded = false;
        RetrofitBuilder.create(this).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                    ownLoded = true;
                    ownAds = response.body().getData().get(0);
                    String[] c1 = ownAds.getColor().split("f");
                    String color = c1[0];
                    binding.itemIntrustial.rlInstastial.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    binding.itemIntrustial.tvTitle.setText(ownAds.getName());
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemIntrustial.imgLogo);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getImage()).centerCrop().into(binding.itemIntrustial.ivBg);
                    binding.itemIntrustial.goWeb.setText(ownAds.getBtnText());
                    binding.itemIntrustial.goWeb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendImpression(VideoActivity2.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemIntrustial.tvBody.setText(ownAds.getDescription());

                }
            }

            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {
//nn
            }
        });

    }

    private void getAdsCredential() {
        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
            }
            mInterstitialAd1 = new InterstitialAd(this);
            mInterstitialAd1.setAdUnitId((google != null) ? google.getInterstitial() : "");
            mInterstitialAd2 = new InterstitialAd(this);
            mInterstitialAd2.setAdUnitId((google2 != null) ? google2.getInterstitial() : "");
            interstitialAdfb = new com.facebook.ads.InterstitialAd(this, (facebook != null) ? facebook.getInterstitial() : "");
            setGoogle1();
        }


    }

    private void setGoogle1() {

        mInterstitialAd1.loadAd(new AdRequest.Builder().build());
        mInterstitialAd1.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Log.d(TAG, "onAdLoaded: google1");
            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d(TAG, "onAdFailedToLoad: google1 " + adError.toString());
                setGoogle2();

//nn
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
                startTimer = false;
                isAdShowing = true;
                getAdsCredential();
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "onAdClosed google 1 closed in : ");
                if (isFromClose) {
                    isFromClose = false;
                    finish();
                } else if (isFromCoin) {
                    isFromCoin = false;
                    startActivity(new Intent(VideoActivity2.this, EarnActivity.class));
                }

            }
        });

    }

    private void setGoogle2() {

        mInterstitialAd2.loadAd(new AdRequest.Builder().build());
        mInterstitialAd2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Log.d(TAG, "onAdLoaded: google2");
            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d(TAG, "onAdFailedToLoad: google2 " + adError.toString());
                setFacebookAds();
//nn
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
                startTimer = false;
                isAdShowing = true;
                getAdsCredential();
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "onAdClosed google 2 closed in : ");
                if (isFromClose) {
                    isFromClose = false;
                    finish();
                } else if (isFromCoin) {
                    isFromCoin = false;
                    startActivity(new Intent(VideoActivity2.this, EarnActivity.class));
                }

            }
        });

    }

    private void setFacebookAds() {

        interstitialAdfb.loadAd(
                interstitialAdfb.buildLoadAdConfig()
                        .withAdListener(new InterstitialAdListener() {
                            @Override
                            public void onInterstitialDisplayed(Ad ad) {
                                Log.d(TAG, "onInterstitialDisplayed: facebook");
                                startTimer = false;
                                isAdShowing = true;
                                getAdsCredential();
                            }

                            @Override
                            public void onInterstitialDismissed(Ad ad) {
                                Log.d(TAG, "onInterstitialDismissed: facebook");
                                if (isFromClose) {
                                    isFromClose = false;
                                    finish();
                                } else if (isFromCoin) {
                                    isFromCoin = false;
                                    startActivity(new Intent(VideoActivity2.this, EarnActivity.class));
                                }

                            }

                            @Override
                            public void onError(Ad ad, AdError adError) {
                                Log.d(TAG, "onError: facebook" + adError.getErrorMessage());
                                if (player != null) {
                                    Log.d(TAG, "onResume: inplayer");
                                    if (!isPopupShowing) {
                                        player.setPlayWhenReady(true);
                                    }
                                    startTimer = true;
                                }
                            }

                            @Override
                            public void onAdLoaded(Ad ad) {
                                Log.d(TAG, "onAdLoaded: facebook");
                            }

                            @Override
                            public void onAdClicked(Ad ad) {

                            }

                            @Override
                            public void onLoggingImpression(Ad ad) {

                            }
                        })
                        .build());
    }

    private void getComments(String cid) {
        Call<CommentRoot> call = RetrofitBuilder.create(this).getComment(cid);
        call.enqueue(new Callback<CommentRoot>() {
            @Override
            public void onResponse(Call<CommentRoot> call, Response<CommentRoot> response) {
                Log.d(TAG, "onResponse: " + response.code());
                if (response.code() == 200 && !response.body().getData().isEmpty()) {
                    comments.clear();
                    comments = response.body().getData();
                    final Timer timer = new Timer();

                    timer.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            runOnUiThread(() -> {
                                if (count < comments.size()) {
                                    commentAdapter.additem(comments.get(count));
                                    binding.rvComments.scrollToPosition(count);
                                    count++;
                                } else {
                                    count = 0;
                                    timer.cancel();
                                    getComments(countryid);
                                }


                                int randInt1 = rand.nextInt((1500 - 1100) + 1) + 1100;
                                binding.tvviews.setText(String.valueOf(randInt1));
                            });


                        }
                    }, 0, 5000);


                }
            }

            @Override
            public void onFailure(Call<CommentRoot> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.toString());
            }
        });
    }


    private void getVideo(String cid) {
        binding.pd.setVisibility(View.VISIBLE);
        Call<VideoRoot> call = RetrofitBuilder.create(this).getVideo(cid);
        call.enqueue(new Callback<VideoRoot>() {
            @Override
            public void onResponse(Call<VideoRoot> call, Response<VideoRoot> response) {
                if (response.code() == 200 && response.body() != null) {

                    if (response.body().getVideo() != null) {
                        videoURL = response.body().getVideo().getVideo();
                        Log.d(TAG, "onResponse: " + videoURL);

                        setvideoURL();

                    }
                }
            }

            @Override
            public void onFailure(Call<VideoRoot> call, Throwable t) {
                binding.pd.setVisibility(View.GONE);
            }
        });
    }

    private void setvideoURL() {
        player = new SimpleExoPlayer.Builder(this).build();
        binding.playerview.setPlayer(player);
        binding.playerview.setShowBuffering(true);
        Log.d(TAG, "setvideoURL: " + videoURL);
        Uri uri = Uri.parse(videoURL);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, "exoplayer-codderlab");
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
        player.prepare(mediaSource, false, false);
        if (!isPopupShowing) {
            player.setPlayWhenReady(true);
        }
        //startTimer=true;
        player.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case STATE_BUFFERING:
                        Log.d(TAG, "buffer: " + uri);
                        break;
                    case STATE_ENDED:
                        player.setRepeatMode(Player.REPEAT_MODE_ALL);
                        Log.d(TAG, "end: " + uri);
                        break;
                    case STATE_IDLE:
                        Log.d(TAG, "idle: " + uri);
//                        if (player != null) {
//                            player.release();
//                        }
                        //  finish();
                        // getVideo(countryid);
                        break;

                    case STATE_READY:
                        binding.lytthumb.setVisibility(View.GONE);
                        binding.pd.setVisibility(View.GONE);

                        Log.d(TAG, "ready: " + uri);

                        break;
                    default:
                        break;
                }
            }
        });

        setTimer();
    }

    Handler handler = new Handler();
    Runnable runnable;

    private void setTimer() {

        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d(TAG, "run: " + time);
                    if (startTimer) {
                        if (time == adsDiffTime) {


                            if (player != null) {
                                player.setPlayWhenReady(false);
                            }
                            if (!isPopupShowing) {

                                showPopup();
                            }

                            Log.d(TAG, "run: popup showed");
                            startTimer = false;
                            time = 0;
                        } else {
                            time++;
                        }
                    } else {
                        time = 0;
                    }
                    handler.postDelayed(this, 1000);

                } catch (IllegalStateException ed) {
                    Log.d(TAG, "run: " + ed.toString());
                    ed.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 1000);

    }

    private void showPopup() {
        if (player != null) {
            player.setPlayWhenReady(false);

        }
        if (!isFinishing()) {
            DefaultPopup defaultPopup = new DefaultPopup(this, binding.tvName.getText().toString(), girl.getImage());
            defaultPopup.sendOwnAd(ownAds);
            isPopupShowing = true;
            defaultPopup.setOnPopupClickListnear(new DefaultPopup.OnPopupClickListnear() {
                @Override
                public void onPostitiveClick() {
                    defaultPopup.dismiss();
                    isPopupShowing = false;
                    showInterStrial();
                }

                @Override
                public void onCloseClick() {
                    defaultPopup.dismiss();
                    isPopupShowing = false;
                    isFromClose = true;
                    showInterStrial();
                }
            });
        }
    }

    private void showInterStrial() {
        Log.d(TAG, "showInterStrial: ");
        if (mInterstitialAd1.isLoaded()) {
            mInterstitialAd1.show();
        } else if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else if (ownLoded && ownAds != null) {
            startTimer = false;
            if (player != null) {
                player.setPlayWhenReady(false);
            }
            Anylitecs.sendImpression(VideoActivity2.this, ownAds.getId());
            binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
            binding.itemIntrustial.closeBtn.setOnClickListener(v -> {
                startTimer = false;
                isAdShowing = true;
                getAdsCredential();
                getOwnAds();
                binding.itemIntrustial.rlInstastial.setVisibility(View.GONE);
                if (isFromClose) {
                    isFromClose = false;
                    finish();
                } else if (isFromCoin) {
                    isFromCoin = false;
                    startActivity(new Intent(VideoActivity2.this, EarnActivity.class));
                }

            });
        } else {
            isFirstTime = true;
            Log.d(TAG, "showInterStrial: no one is loded");

            if (!isAdShowing) {
                Log.d(TAG, "showInterStrial: isadshowing");
                isAdShowing = false;
            }
            if (isFromCoin) {
                Log.d(TAG, "showInterStrial: isfromcoin");
                isFromCoin = false;
                startActivity(new Intent(VideoActivity2.this, EarnActivity.class));

                return;
            } else if (isFromClose) {
                finish();
                return;
            } else if (isFirstTime) {
                Log.d(TAG, "showInterStrial: firsttime");
                if (player != null) {
                    Log.d(TAG, "onResume: inplayer");
                    if (!isPopupShowing) {
                        player.setPlayWhenReady(true);
                    }
                    startTimer = true;
                }
            }


        }
    }

    @Override
    protected void onPause() {
        super.onPause();


        Log.d(TAG, "onPause: ");
        if (player != null) {
            player.setVolume(0);
            Log.d(TAG, "onPause: in player");
            player.setPlayWhenReady(false);
            startTimer = false;
        }
        Log.d(TAG, "ffgonpause: ");
        Log.d(TAG, "ffgonpause: ispopup showwing " + isPopupShowing);
        Log.d(TAG, "ffgonpause: isfirsttime showwing " + isFirstTime);
        Log.d(TAG, "ffgonpause: isadshowing  " + isAdShowing);
        if (isAdShowing) {
            isAdShowing = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (player != null) {
            player.setVolume(1);
        }
        Log.d(TAG, "ffgonResume: ");
        Log.d(TAG, "ffgonResume: ispopup showwing " + isPopupShowing);
        Log.d(TAG, "ffgonResume: isfirsttime showwing " + isFirstTime);
        Log.d(TAG, "ffgonResume: isadshowing  " + isAdShowing);

        if (isFirstTime) {
            isFirstTime = false;
            startTimer = false;
            showPopup();
            return;
        } else {
            if (!isAdShowing) {
                if (!isPopupShowing) {
                    showPopup();
                }
            } else {
                if (player != null) {
                    Log.d(TAG, "onResume: inplayer");
                    if (!isPopupShowing) {
                        player.setPlayWhenReady(true);
                    }
                    startTimer = true;
                }
            }

        }
        if (!isAdShowing) {
            isAdShowing = false;
        }

    }


    private void initSettings() {
        adsDiffTime = Integer.parseInt(sessionManager.getStringValue(Const.ADS_TIME).equals("") ? "30" : sessionManager.getStringValue(Const.ADS_TIME));
        animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoomin);

    }

    private void initView() {
        binding.rvComments.setAdapter(commentAdapter);

        ObjectAnimator animation = ObjectAnimator.ofFloat(binding.imggift2, "rotationY", 0.0f, 360f);
        animation.setDuration(5000);
        animation.setRepeatCount(INFINITE);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.start();


        updetUI(SHEET_CLOSE);
        setUI();
        getGiftsCategories();
    }

    private void initListnear() {

        binding.etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//ll
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    //ll
                } else {
                    keyboardOpen = true;
                    cleartext = false;
                    binding.lytbuttons.setVisibility(View.GONE);
                    binding.btnsend.setVisibility(View.VISIBLE);
                    binding.etComment.setVisibility(View.VISIBLE);
                    binding.lytShare.setVisibility(View.GONE);
                    binding.lytbuttons.setVisibility(View.GONE);
                    binding.btnsend.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//ll
            }
        });
        binding.btnsend.setOnClickListener(v -> {
            binding.btnsend.setVisibility(View.VISIBLE);
            binding.lytbuttons.setVisibility(View.VISIBLE);
            if (!binding.etComment.getText().toString().equals("")) {

                CommentRoot.Datum datum = new CommentRoot.Datum();
                datum.setComment(binding.etComment.getText().toString().trim());
                datum.setName(sessionManager.getUser().getFname());
                commentAdapter.additem(datum);
                binding.rvComments.setAdapter(commentAdapter);
                binding.rvComments.scrollToPosition(commentAdapter.getItemCount());

                setUI();
                cleartext = true;


            }
        });
        binding.etComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (EditorInfo.IME_ACTION_SEND == actionId) {
                    if (!binding.etComment.getText().toString().equals("")) {

                        CommentRoot.Datum datum = new CommentRoot.Datum();
                        datum.setComment(binding.etComment.getText().toString().trim());
                        datum.setName(sessionManager.getUser().getFname());
                        commentAdapter.additem(datum);
                        binding.rvComments.setAdapter(commentAdapter);
                        binding.rvComments.scrollToPosition(commentAdapter.getItemCount());

                        setUI();
                        cleartext = true;


                    }
                }
                return true;
            }
        });
        binding.imggift2.setOnClickListener(v -> {
            binding.bottomPage.tvUsereCoin.setText(String.valueOf(sessionManager.getUser().getCoin()));
            binding.tvusercoins.setText(String.valueOf(sessionManager.getUser().getCoin()));
            updetUI(SHEET_OPEN);
        });
        binding.playerview.setOnClickListener(v -> updetUI(SHEET_CLOSE));
        binding.bottomPage.btnclose.setOnClickListener(v -> updetUI(SHEET_CLOSE));


    }

    @Override
    public void onBackPressed() {
        if (keyboardOpen) {
            setUI();
        } else {
            isFromClose = true;
            showInterStrial();
        }


    }

    private View createCustomView(EmojicategoryRoot.Datum datum) {
        Log.d(TAG, "settabLayout: " + datum.getName());
        Log.d(TAG, "settabLayout: " + datum.getIcon());
        View v = LayoutInflater.from(this).inflate(R.layout.custom_tabgift, null);
        TextView tv = v.findViewById(R.id.tvTab);
        tv.setText(datum.getName());
        ImageView img = v.findViewById(R.id.imagetab);

        Glide.with(getApplicationContext())
                .load(new SessionManager(VideoActivity2.this).getStringValue(Const.IMAGE_URL) + datum.getIcon())
                .placeholder(R.drawable.ic_gift)
                .into(img);
        return v;

    }

    private void getGiftsCategories() {
        Call<EmojicategoryRoot> call = RetrofitBuilder.create(this).getCategories();
        call.enqueue(new Callback<EmojicategoryRoot>() {
            @Override
            public void onResponse(Call<EmojicategoryRoot> call, Response<EmojicategoryRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {

                    List<EmojicategoryRoot.Datum> categories = response.body().getData();

                    for (int i = 0; i < categories.size(); i++) {
                        if (Boolean.TRUE.equals(categories.get(i).getIsTop())) {
                            tempobj = categories.get(i);
                        } else {
                            finelCategories.add(categories.get(i));
                        }
                    }
                    if (tempobj != null) {
                        finelCategories.add(0, tempobj);
                    }

                    setGiftList();


                    BottomViewPagerAdapter bottomViewPagerAdapter = new BottomViewPagerAdapter(finelCategories);
                    binding.bottomPage.viewpager.setAdapter(bottomViewPagerAdapter);
                    settabLayout(finelCategories);
                    binding.bottomPage.viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.bottomPage.tablayout));
                    binding.bottomPage.tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            binding.bottomPage.viewpager.setCurrentItem(tab.getPosition());
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {
                            //ll
                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {
                            //ll
                        }
                    });
                    bottomViewPagerAdapter.setEmojiListnerViewPager((bitmap, coin) -> {
                        sendGift(bitmap, coin);
                        updetUI(SHEET_CLOSE);
                    });
                }
            }

            private void settabLayout(List<EmojicategoryRoot.Datum> categories) {
                binding.bottomPage.tablayout.setTabGravity(TabLayout.GRAVITY_FILL);
                for (int i = 0; i < categories.size(); i++) {

                    binding.bottomPage.tablayout.addTab(binding.bottomPage.tablayout.newTab().setCustomView(createCustomView(categories.get(i))));

                }
            }

            @Override
            public void onFailure(Call<EmojicategoryRoot> call, Throwable t) {
//ll
            }
        });
    }

    private void setGiftList() {
        Call<EmojiIconRoot> call1 = RetrofitBuilder.create(this).getEmojiByCategory(finelCategories.get(0).get_id());
        call1.enqueue(new Callback<EmojiIconRoot>() {
            private void onEmojiClick(Bitmap bitmap, Long coin) {
                sendGift(bitmap, coin);
            }

            @Override
            public void onResponse(Call<EmojiIconRoot> call, Response<EmojiIconRoot> response) {
                Log.d(TAG, "onResponse: emoji yes" + response.code());
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {

                    emojiAdapter = new EmojiAdapter(response.body().getData());
                    binding.rvEmogi.setAdapter(emojiAdapter);

                    emojiAdapter.setOnEmojiClickListnear(this::onEmojiClick);


                }
            }

            @Override
            public void onFailure(Call<EmojiIconRoot> call, Throwable t) {
//ll
            }
        });
    }

    private void sendGift(Bitmap bitmap, Long coin) {
        Log.d(TAG, "sendGift: ");

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", userId);
        jsonObject.addProperty("coin", coin);
        Call<UserRoot> call = RetrofitBuilder.create(this).lessCoin(jsonObject);
        call.enqueue(new Callback<UserRoot>() {
            @Override
            public void onResponse(Call<UserRoot> call, Response<UserRoot> response) {
                if (response.code() == 200) {

                    if (response.body().getStatus() == 200) {

                        rendtemp = (int) (rendtemp + coin);


                        double coin1;
                        if (rendtemp >= 1000) {
                            coin1 = (double) rendtemp / 1000;
                            DecimalFormat df = new DecimalFormat("#.##");

                            binding.tvCoin.setText(df.format(coin1).concat("K"));
                        } else {
                            coin1 = rendtemp;
                            binding.tvCoin.setText(String.valueOf((int) coin1));
                        }
                        binding.tvusercoins.setText(String.valueOf(response.body().getData().getCoin()));
                        binding.bottomPage.tvUsereCoin.setText(String.valueOf(response.body().getData().getCoin()));

                        Log.d(TAG, "onResponse: success coin minused");
                        sessionManager.saveUser(response.body().getData());

                        binding.imgAnimation.setImageBitmap(bitmap);
                        binding.imgAnimation.setVisibility(View.VISIBLE);
                        binding.imgAnimation.startAnimation(animZoomIn);
                        new Handler().postDelayed(() -> {
                            binding.imgAnimation.setImageBitmap(null);
                            binding.imgAnimation.setVisibility(View.GONE);
                        }, 2000);
                    } else if (response.body().getStatus() == 422) {
                        Toast.makeText(VideoActivity2.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserRoot> call, Throwable t) {
                Log.d(TAG, "onFailure: 452 " + t.toString());
            }
        });


    }

    private void setUI() {
        binding.etComment.setText(null);
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(binding.getRoot().getWindowToken(), 0);
        binding.btnsend.setVisibility(View.VISIBLE);
        binding.lytbuttons.setVisibility(View.VISIBLE);

        binding.etComment.setVisibility(View.VISIBLE);
        binding.lytShare.setVisibility(View.VISIBLE);
        keyboardOpen = false;
    }

    private void updetUI(int state) {
        if (state == SHEET_OPEN) {
            binding.bottomPage.lyt2.setVisibility(View.VISIBLE);

            binding.rvComments.setVisibility(View.GONE);
            binding.rvEmogi.setVisibility(View.GONE);
            binding.lytbottom.setVisibility(View.GONE);
            binding.lytShare.setVisibility(View.GONE);
            binding.lytusercoin.setVisibility(View.GONE);
        } else {
            binding.bottomPage.lyt2.setVisibility(View.GONE);
            binding.rvComments.setVisibility(View.VISIBLE);
            binding.rvEmogi.setVisibility(View.VISIBLE);
            binding.lytbottom.setVisibility(View.VISIBLE);
            binding.lytShare.setVisibility(View.VISIBLE);
            binding.lytusercoin.setVisibility(View.VISIBLE);
        }
    }

    public void onClickchat(View view) {
        startActivity(new Intent(this, ChatActivity.class).putExtra("image", girl.getImage()).putExtra("name", binding.tvName.getText().toString()));

    }

    public void onClickClose(View view) {
        isFromClose = true;
        showInterStrial();
    }

    public void onclickShare(View view) {
        final String appLink = "\nhttps://play.google.com/store/apps/details?id=" + getPackageName();
        Intent sendInt = new Intent(Intent.ACTION_SEND);
        sendInt.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sendInt.putExtra(Intent.EXTRA_TEXT, "Humile - live video chat  Download Now  " + appLink);
        sendInt.setType("text/plain");
        startActivity(Intent.createChooser(sendInt, "Share"));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
        if (player != null) {
            player.release();
            player = null;
        }
    }

    public void onClickCoin(View view) {
        isFromCoin = true;
        showInterStrial();
    }
}