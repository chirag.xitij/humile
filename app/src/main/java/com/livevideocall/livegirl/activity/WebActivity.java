package com.livevideocall.livegirl.activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.JsonObject;
import com.greedygame.core.interstitial.general.GGInterstitialAd;
import com.livevideocall.livegirl.Anylitecs;
import com.livevideocall.livegirl.BuildConfig;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.databinding.ActivityWebBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.HitAdsRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebActivity extends AppCompatActivity {


    private static final String TAG = "webact";
    private static final String WEBSITE = "WEBSITE";
    WebView wv1;
    MaterialProgressBar progressBar;
    boolean loadingFinished = true;
    boolean redirect = false;
    ImageView imgBack;
    ImageView imgRefresh;
    ActivityWebBinding binding;
    InterstitialAdListener interstitialAdListener;
    AdView adView;
    private String url;
    private String adid;
    private com.facebook.ads.InterstitialAd interstitialAdfb;
    private InterstitialAd mInterstitialAd;
    private com.facebook.ads.AdListener adListener;
    private com.facebook.ads.AdView adViewfb;
    private String ownAdRewardUrl = "";
    private String ownAdBannerUrl = "";
    private String ownAdInstarUrl = "";

    private String ownWebUrl = "";

    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Facebook facebook;
    private boolean showAds = false;
    private SessionManager sessionManager;
    private GGInterstitialAd mAd;
    private AdvertisementRoot.Google2 google2;
    private InterstitialAd mInterstitialAd2;
    private boolean ownLoded = false;
    private OwnAdsRoot.DataItem ownAds;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        MainActivity.setStatusBarGradiant(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web);
        sessionManager = new SessionManager(this);


        progressBar = findViewById(R.id.progressbar);
        TextView textView = findViewById(R.id.tvwebsite);


        progressBar.setIndeterminate(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setIndeterminateTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorPrimary)));
        }

        imgBack = findViewById(R.id.imgBack);
        imgRefresh = findViewById(R.id.imgrefresh);
        wv1 = findViewById(R.id.webview);
        wv1.setWebViewClient(new WebViewClient());
        Intent intent = getIntent();
        url = intent.getStringExtra(WEBSITE);

        String title = intent.getStringExtra("title");
        if (title != null && !title.equals("")) {
            textView.setText(title);
        } else {
            String website = getbaseWebsite(url);
            textView.setText(website);
        }


        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        String type = intent.getStringExtra("type");
        if (type != null && !type.equals("") && type.equals("ads")) {
            String adid1 = intent.getStringExtra("ADID");
            if (adid1 != null && !adid1.equals("")) {
                sendHit(adid1);
            }
        }
        loadUrl();
        loadAds();
    }

    private void sendHit(String adid) {


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ad_id", adid);
        jsonObject.addProperty("country", new SessionManager(this).getStringValue(Const.Country));
        Call<HitAdsRoot> call = RetrofitBuilder.create(this).sendImpression(jsonObject);
        call.enqueue(new Callback<HitAdsRoot>() {
            @Override
            public void onResponse(Call<HitAdsRoot> call, Response<HitAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200) {
                    Log.d(TAG, "onResponse: impressonsended");
                }
            }

            @Override
            public void onFailure(Call<HitAdsRoot> call, Throwable t) {
                Log.d(TAG, "onFailure: error 421" + t.toString());
            }
        });


    }

    private void loadUrl() {
        if (url != null) {
            wv1.loadUrl(url);

            wv1.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
                    if (!loadingFinished) {
                        redirect = true;
                    }

                    loadingFinished = false;
                    view.loadUrl(urlNewString);
                    return true;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap facIcon) {
                    loadingFinished = false;
                    //SHOW LOADING IF IT ISNT ALREADY VISIBLE
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    if (!redirect) {
                        loadingFinished = true;
                        progressBar.setVisibility(View.GONE);
                    }

                    if (loadingFinished && !redirect) {
                        //HIDE LOADING IT HAS FINISHED
                        progressBar.setVisibility(View.GONE);
                    } else {
                        redirect = false;
                        progressBar.setVisibility(View.GONE);
                    }

                }
            });

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Anylitecs.addUser(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Anylitecs.removeSesson(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Anylitecs.removeSesson(this);
        Log.d(TAG, "onPause: ");
    }

    private String getbaseWebsite(String url) {
        try {
            String[] s = url.split("//");
            String[] s1 = s[1].split("/");


            return s1[0];
        } catch (Exception o) {
            return "";
        }
    }

    public void onBackClick(View view) {
        if (!showAds) {
            showAds = true;

        } else {
            return;
        }
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else if (ownLoded) {
            Anylitecs.sendImpression(WebActivity.this, ownAds.getId());
            binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
            binding.itemIntrustial.closeBtn.setOnClickListener(v -> finish());
        } else {
            finish();
        }
    }

    public void onClickRefresh(View view) {
        loadUrl();
    }

    @Override
    public void onBackPressed() {
        if (!showAds) {
            showAds = true;

        } else {
            return;
        }
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else if (ownLoded) {
            Anylitecs.sendImpression(WebActivity.this, ownAds.getId());
            binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
            binding.itemIntrustial.closeBtn.setOnClickListener(v -> finish());
        } else {
            finish();
        }
    }

    private void loadAds() {
        MobileAds.initialize(this, initializationStatus -> {
        });
        AudienceNetworkAds.initialize(this);

        getOwnAds();

        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {

            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
            }
            setBanner();

//            mAd = new GGInterstitialAd(this, (google != null) ? google.getInterstitial() : "");
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId((google != null) ? google.getInterstitial() : "");
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            interAdListnear();

            mInterstitialAd2 = new InterstitialAd(this);
            mInterstitialAd2.setAdUnitId((google2 != null) ? google2.getInterstitial() : "");
            mInterstitialAd2.loadAd(new AdRequest.Builder().build());
            interAdListnear2();

            fbInterAdListnear();
            interstitialAdfb = new com.facebook.ads.InterstitialAd(this, (facebook != null) ? facebook.getInterstitial() : "");
            interstitialAdfb.loadAd(
                    interstitialAdfb.buildLoadAdConfig()
                            .withAdListener(interstitialAdListener)
                            .build());

            fbBannerAdListnear();
            adViewfb = new com.facebook.ads.AdView(this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
            LinearLayout adContainer = findViewById(R.id.banner_container);
            adContainer.addView(adViewfb);
            adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(adListener).build());
        } else {
            Toast.makeText(this, "ads not downloded ", Toast.LENGTH_SHORT).show();
        }
    }

    private void interAdListnear2() {
        mInterstitialAd2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d(TAG, "onAdFailedToLoad: " + adError.toString());
                //nn
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                finish();
                // Code to be executed when the interstitial ad is closed.
            }
        });
    }

    private void interAdListnear() {
        /*mAd.loadAd(new GGInterstitialEventsListener() {
                       @Override
                       public void onAdLoaded() {
                           Log.d("GGADS", "Ad Loaded");
                       }

                       @Override
                       public void onAdLeftApplication() {
                           Log.d("GGADS", "Ad Left Application");
                       }

                       @Override
                       public void onAdClosed() {
                           Log.d("GGADS", "Ad Closed");
                           //Interstitial ad will be automatically refreshed by SDKX when     closed
                       }

                       @Override
                       public void onAdOpened() {
                           Log.d("GGADS", "Ad Opened");
                       }

                       @Override
                       public void onAdLoadFailed(AdRequestErrors cause) {
                           Log.d("GGADS", "Ad Load Failed " + cause);
                       }
                   }
        );*/

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d(TAG, "onAdFailedToLoad: " + adError.toString());
                //nn
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                finish();
                // Code to be executed when the interstitial ad is closed.
            }
        });


    }

    private void fbInterAdListnear() {
        interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                finish();
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {


                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");

            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        };
    }

    private void getOwnAds() {
        RetrofitBuilder.create(this).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                    ownLoded = true;
                    ownAds = response.body().getData().get(0);

                    String[] c1 = ownAds.getColor().split("f");
                    String color = c1[0];
                    //TODO banner ads

                    binding.itemBannerownad.tvtitle.setText(ownAds.getName());
                    binding.itemBannerownad.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemBannerownad.imglogo);
                    binding.itemBannerownad.tvbtn.setText(ownAds.getBtnText());
                    binding.itemBannerownad.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(WebActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemBannerownad.tvdes.setText(ownAds.getDescription());

                    //TODO instastial ads

                    binding.itemIntrustial.rlInstastial.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    binding.itemIntrustial.tvTitle.setText(ownAds.getName());
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemIntrustial.imgLogo);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getImage()).centerCrop().into(binding.itemIntrustial.ivBg);
                    binding.itemIntrustial.goWeb.setText(ownAds.getBtnText());
                    binding.itemIntrustial.goWeb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(WebActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemIntrustial.tvBody.setText(ownAds.getDescription());

                }
            }

            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {
                binding.itemBannerownad.lytad.setVisibility(View.GONE);
                binding.itemIntrustial.rlInstastial.setVisibility(View.GONE);
///mm
            }
        });
    }

    private void setBanner() {
        LinearLayout adContainer = findViewById(R.id.banner_container);
        adView = new AdView(this);

        adView.setAdUnitId((google != null) ? google.getBanner() : "");
        adContainer.addView(adView);
        AdRequest adRequest =
                new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();
        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);


        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Log.d(TAG, "onAdFailedToLoad: banner " + loadAdError);
                adContainer.removeAllViews();
                setgooglebanner2();
            }


       /* GGAdview bannerUnit = new GGAdview(this);
        bannerUnit.setUnitId((google != null) ? google.getBanner() : " ");  //Replace with your Ad Unit ID here
        bannerUnit.setAdsMaxHeight(250);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 200);
        adContainer.addView(bannerUnit, layoutParams);
        bannerUnit.loadAd(new AdLoadCallback() {

            @Override
            public void onAdLoadFailed(@NotNull AdRequestErrors adRequestErrors) {

                setFbBanner();
            }

            private void setFbBanner() {
                adViewfb = new com.facebook.ads.AdView(EarnActivity.this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);

                adContainer.addView(adViewfb);
                adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.d(TAG, "onError: fb " + adError.getErrorMessage());
                        binding.imgOwnAd.setVisibility(View.VISIBLE);
                        sendImpression(EarnActivity.this, adid);
                        binding.imgOwnAd.setOnClickListener(v -> {
                            Intent intent = new Intent(EarnActivity.this, WebActivity.class);
                            intent.putExtra("ADID", String.valueOf(adid));
                            intent.putExtra(WEBSITE, ownWebUrl);
                            intent.putExtra("type", "ads");
                            startActivity(intent);
                        });

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
//mm
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
//mm
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
//mm
                    }
                }).build());
            }


            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onReadyForRefresh() {

            }

            @Override
            public void onUiiOpened() {
            }

            @Override
            public void onUiiClosed() {

            }

        });*/

            private void setgooglebanner2() {
                LinearLayout adContainer = findViewById(R.id.banner_container);
                adView = new AdView(WebActivity.this);

                adView.setAdUnitId((google2 != null) ? google2.getBanner() : "");
                adContainer.addView(adView);
                AdRequest adRequest =
                        new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                                .build();
                AdSize adSize = getAdSize();
                adView.setAdSize(adSize);
                adView.loadAd(adRequest);


                adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        Log.d(TAG, "onAdFailedToLoad: banner " + loadAdError);
                        adContainer.removeAllViews();
                        setFbBanner();
                    }
                });

            }

            private void setFbBanner() {
                adViewfb = new com.facebook.ads.AdView(WebActivity.this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);

                adContainer.addView(adViewfb);
                adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.d(TAG, "onError: fb " + adError.getErrorMessage());
                        Anylitecs.sendImpression(WebActivity.this, ownAds.getId());
                        binding.itemBannerownad.lytad.setVisibility(View.VISIBLE);
                        /*binding.imgOwnAd.setVisibility(View.VISIBLE);
                        sendImpression(WebActivity.this, adid);
                        binding.imgOwnAd.setOnClickListener(v -> {
                            Intent intent = new Intent(WebActivity.this, WebActivity.class);
                            intent.putExtra("ADID", String.valueOf(adid));
                            intent.putExtra(WEBSITE, ownWebUrl);
                            intent.putExtra("type", "ads");
                            startActivity(intent);
                        });*/

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
//mm
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
//mm
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
//mm
                    }
                }).build());
            }

        });

    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }


    private void fbBannerAdListnear() {
        adListener = new com.facebook.ads.AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
            }
        };
    }


}