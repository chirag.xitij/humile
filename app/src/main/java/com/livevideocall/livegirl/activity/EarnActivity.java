package com.livevideocall.livegirl.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.RewardedVideoAd;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.gson.JsonObject;
import com.livevideocall.livegirl.Anylitecs;
import com.livevideocall.livegirl.BuildConfig;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.databinding.ActivityEarnBinding;
import com.livevideocall.livegirl.databinding.PopUpShowAdsBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.HitAdsRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.models.ProfileRoot;
import com.livevideocall.livegirl.models.UserRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;
import com.livevideocall.livegirl.retrofit.RetrofitService;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EarnActivity extends AppCompatActivity {
    private static final String TAG = "adsactivity";
    private static final String WEBSITE = "WEBSITE";

    ActivityEarnBinding binding;
    InterstitialAdListener interstitialAdListener;
    AdView adView;
    RetrofitService service;
    boolean ownLoded = false;
    VideoView videoView;
    private String adid;
    private RewardedVideoAd rewardedVideoAd;
    private RewardedAd rewardedAd;
    private SessionManager sessionManager;
    private InterstitialAd mInterstitialAd;
    private com.facebook.ads.InterstitialAd interstitialAdfb;
    private com.facebook.ads.AdListener adListener;
    private com.facebook.ads.AdView adViewfb;
    private String ownAdRewardUrl = "";
    private String ownAdBannerUrl = "";
    private String ownAdInstarUrl = "";
    private String userId;
    private String ownWebUrl = "";
    private AlertDialog aleart;
    private boolean fetched;
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Facebook facebook;
    private MediaPlayer mp;
    private boolean isreawrdfbreward = false;
    private AdvertisementRoot.Google2 google2;
    private InterstitialAd mInterstitialAd2;
    private String nativeAd1failure1 = "";
    private String nativeAd1failure2 = "";
    private String nativeAd1failure3 = "";
    private OwnAdsRoot.DataItem ownAds;
    private boolean showadscoin = false;
    private Dialog dialog;
    private PopUpShowAdsBinding popupbinding;
    private boolean finish = false;
//    private GGInterstitialAd mAd;

    public static void sendImpression(Context context, String adid) {


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ad_id", adid);
        jsonObject.addProperty("country", new SessionManager(context).getStringValue(Const.Country));
        Call<HitAdsRoot> call = RetrofitBuilder.create(context).sendImpression(jsonObject);
        call.enqueue(new Callback<HitAdsRoot>() {
            @Override
            public void onResponse(Call<HitAdsRoot> call, Response<HitAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200) {
                    Log.d(TAG, "onResponse: impressonsended");
                }
            }

            @Override
            public void onFailure(Call<HitAdsRoot> call, Throwable t) {
                Log.d(TAG, "onFailure: error 421" + t.toString());
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        MainActivity.setStatusBarGradiant(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_earn);

        service = RetrofitBuilder.create(this);
        sessionManager = new SessionManager(this);
        chkConnection2();
        initMain();
        loadad();

        binding.btnClose.setBackgroundResource(R.drawable.bg_whitebtnround);
        binding.btnVolume.setBackgroundResource(R.drawable.bg_whitebtnround);
    }

    private void chkConnection2() {
        aleart = new androidx.appcompat.app.AlertDialog.Builder(EarnActivity.this).
                setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Internet Connection Alert")
                .setMessage("Please Turn on Internet Connection")
                .setPositiveButton("Close", (dialog, which) -> finishAffinity()).create();
        aleart.setCancelable(false);
        final Handler handler = new Handler();
        final Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(() -> {
                    try {
                        if (isConnected()) {
                            aleart.dismiss();
                            if (!fetched) {
                                initMain();
                                loadad();
                            }
                        } else {
                            if (!aleart.isShowing()) {
                                fetched = false;
                                aleart.show();
                            }

                        }

                    } catch (Exception e) {
                        //mm
                    }
                });
            }

            private boolean isConnected() {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                return networkInfo != null && networkInfo.isConnected();
            }

        };
        timer.schedule(doAsynchronousTask, 0, 1000);
    }

    private void initMain() {
        MobileAds.initialize(this, initializationStatus -> {
        });
        AudienceNetworkAds.initialize(this);

        getOwnAds();
        fetched = true;
        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {

            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
            }

            setBanner();


            if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                userId = sessionManager.getUser().get_id();
                getUser();
            }

            setGoogleAds1();

//          mAd = new GGInterstitialAd(this, (google != null) ? google.getInterstitial() : "");

        } else {
            Toast.makeText(this, "ads not downloded ", Toast.LENGTH_SHORT).show();
        }


    }

    private void loadad() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId((google != null) ? google.getInterstitial() : "");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        interAdListnear();

        mInterstitialAd2 = new InterstitialAd(this);
        mInterstitialAd2.setAdUnitId((google2 != null) ? google2.getInterstitial() : "");
        mInterstitialAd2.loadAd(new AdRequest.Builder().build());
        interAdListnear2();


        fbInterAdListnear();
        interstitialAdfb = new com.facebook.ads.InterstitialAd(this, (facebook != null) ? facebook.getInterstitial() : "");
        interstitialAdfb.loadAd(
                interstitialAdfb.buildLoadAdConfig()
                        .withAdListener(interstitialAdListener)
                        .build());

        fbBannerAdListnear();
        adViewfb = new com.facebook.ads.AdView(this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
        LinearLayout adContainer = findViewById(R.id.banner_container);
        adContainer.addView(adViewfb);
        adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(adListener).build());
    }


    private void setGoogleAds1() {
        nativeAd1failure2 = "google1";
        nativeAd1failure1 = "google1";
        nativeAd1failure3 = "google1";
        setnative((google != null) ? google.getJsonMemberNative() : "");
        setnative2((google != null) ? google.getJsonMemberNative() : "");
        setnative3((google != null) ? google.getJsonMemberNative() : "");
    }

    private void setnative(String adid) {
        AdLoader adLoader = new AdLoader.Builder(this, adid)
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    Log.d(TAG, "showPopup: popup native loded");
                    binding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) getLayoutInflater()
                                    .inflate(R.layout.native_google_popup, null);

                    populatead(adView, unifiedNativeAd);
                    binding.flAdplaceholderbanner.removeAllViews();
                    binding.flAdplaceholderbanner.addView(adView);
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());

                        facebookAd1();

                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void facebookAd1() {
        NativeAd nativeAd = new NativeAd(this, (facebook != null) ? facebook.getJsonMemberNative() : " ");
        NativeAdListener nativeAdListener = new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
                Log.e("TAG", "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                binding.nativeAdFb.setVisibility(View.GONE);
                if (ownLoded) {
                    binding.itemNative1.lytad.setVisibility(View.VISIBLE);
                }


                // Native ad failed to load
                Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                inflateAd(nativeAd);
                // Native ad is loaded and ready to be displayed
                Log.d("TAG", "Native ad is loaded and ready to be displayed!");

            }

            private void inflateAd(NativeAd nativeAd) {
                binding.nativeAdFb.setVisibility(View.VISIBLE);
                nativeAd.unregisterView();

                // Add the Ad view into the ad container.

                NativeAdLayout nativeAdLayout = binding.nativeAdFb;

                LayoutInflater inflater = LayoutInflater.from(EarnActivity.this);
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_fb_popup, nativeAdLayout, false);
                nativeAdLayout.addView(adView);


                // Create native UI using the ad metadata.
                com.facebook.ads.MediaView nativeAdIcon = adView.findViewById(R.id.ad_media);
                TextView nativeAdTitle = adView.findViewById(R.id.ad_headline);
                ImageView imageView = adView.findViewById(R.id.ad_logo);
                /*MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);*/
                /* TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);*/
                TextView nativeAdBody = adView.findViewById(R.id.ad_body);
                /* TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);*/
                TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

                // Set the Text.
                nativeAdTitle.setText(nativeAd.getAdvertiserName());
                nativeAdBody.setText(nativeAd.getAdBodyText());
                /*nativeAdSocialContext.setText(nativeAd.getAdSocialContext());*/
                nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                // Create a list of clickable views
                List<View> clickableViews = new ArrayList<>();
                clickableViews.add(nativeAdTitle);
                clickableViews.add(nativeAdCallToAction);

                // Register the Title and CTA button to listen for clicks.
                nativeAd.registerViewForInteraction(
                        adView, nativeAdIcon, imageView, clickableViews);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        };

        nativeAd.loadAd(
                nativeAd.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build());
    }


    private void setnative2(String adid) {
        AdLoader adLoader = new AdLoader.Builder(this, adid)
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    Log.d(TAG, "showPopup: popup native loded");
                    binding.flAdplaceholderbanner2.setVisibility(View.VISIBLE);
                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) getLayoutInflater()
                                    .inflate(R.layout.native_google_popup, null);

                    populatead(adView, unifiedNativeAd);
                    binding.flAdplaceholderbanner2.removeAllViews();
                    binding.flAdplaceholderbanner2.addView(adView);
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        Log.d(TAG, "onAdFailedToLoad: popup nativewff  " + adError.getMessage());


                        Log.d(TAG, "onAdFailedToLoad: popup nativew which " + nativeAd1failure2);

                        facebookAd2();
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()

                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void facebookAd2() {
        NativeAd nativeAd = new NativeAd(this, (facebook != null) ? facebook.getJsonMemberNative() : " ");
        NativeAdListener nativeAdListener = new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
                Log.e("TAG", "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                binding.nativeAdFb.setVisibility(View.GONE);
                if (ownLoded) {
                    Anylitecs.sendImpression(EarnActivity.this, ownAds.getId());
                    binding.itemNative2.lytad.setVisibility(View.VISIBLE);
                }


                // Native ad failed to load
                Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                inflateAd(nativeAd);
                // Native ad is loaded and ready to be displayed
                Log.d("TAG", "Native ad is loaded and ready to be displayed!");

            }

            private void inflateAd(NativeAd nativeAd) {
                binding.nativeAdFb2.setVisibility(View.VISIBLE);
                nativeAd.unregisterView();

                // Add the Ad view into the ad container.

                NativeAdLayout nativeAdLayout = binding.nativeAdFb2;

                LayoutInflater inflater = LayoutInflater.from(EarnActivity.this);
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_fb_popup, nativeAdLayout, false);
                nativeAdLayout.addView(adView);


                // Create native UI using the ad metadata.
                com.facebook.ads.MediaView nativeAdIcon = adView.findViewById(R.id.ad_media);
                TextView nativeAdTitle = adView.findViewById(R.id.ad_headline);
                ImageView imageView = adView.findViewById(R.id.ad_logo);
                /*MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);*/
                /* TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);*/
                TextView nativeAdBody = adView.findViewById(R.id.ad_body);
                /* TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);*/
                TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

                // Set the Text.
                nativeAdTitle.setText(nativeAd.getAdvertiserName());
                nativeAdBody.setText(nativeAd.getAdBodyText());
                /*nativeAdSocialContext.setText(nativeAd.getAdSocialContext());*/
                nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                // Create a list of clickable views
                List<View> clickableViews = new ArrayList<>();
                clickableViews.add(nativeAdTitle);
                clickableViews.add(nativeAdCallToAction);

                // Register the Title and CTA button to listen for clicks.
                nativeAd.registerViewForInteraction(
                        adView, nativeAdIcon, imageView, clickableViews);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        };

        nativeAd.loadAd(
                nativeAd.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build());
    }

    private void setnative3(String adid) {
        AdLoader adLoader = new AdLoader.Builder(this, adid)
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    Log.d(TAG, "showPopup: popup native loded");
                    binding.flAdplaceholderbanner3.setVisibility(View.VISIBLE);
                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) getLayoutInflater()
                                    .inflate(R.layout.native_google_popup, null);

                    populatead(adView, unifiedNativeAd);
                    binding.flAdplaceholderbanner3.removeAllViews();
                    binding.flAdplaceholderbanner3.addView(adView);
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {
                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());


                        facebookAd3();
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()

                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void facebookAd3() {
        NativeAd nativeAd = new NativeAd(this, (facebook != null) ? facebook.getJsonMemberNative() : " ");
        NativeAdListener nativeAdListener = new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
                Log.e("TAG", "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                binding.nativeAdFb3.setVisibility(View.GONE);
                if (ownLoded) {
                    Anylitecs.sendImpression(EarnActivity.this, ownAds.getId());
                    binding.itemNative3.lytad.setVisibility(View.VISIBLE);
                }


                // Native ad failed to load
                Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                inflateAd(nativeAd);
                // Native ad is loaded and ready to be displayed
                Log.d("TAG", "Native ad is loaded and ready to be displayed!");

            }

            private void inflateAd(NativeAd nativeAd) {
                binding.nativeAdFb3.setVisibility(View.VISIBLE);
                nativeAd.unregisterView();

                // Add the Ad view into the ad container.

                NativeAdLayout nativeAdLayout = binding.nativeAdFb3;

                LayoutInflater inflater = LayoutInflater.from(EarnActivity.this);
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_fb_popup, nativeAdLayout, false);
                nativeAdLayout.addView(adView);


                // Create native UI using the ad metadata.
                com.facebook.ads.MediaView nativeAdIcon = adView.findViewById(R.id.ad_media);
                ImageView imageView = adView.findViewById(R.id.ad_logo);
                TextView nativeAdTitle = adView.findViewById(R.id.ad_headline);
                /*MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);*/
                /* TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);*/
                TextView nativeAdBody = adView.findViewById(R.id.ad_body);
                /* TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);*/
                TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

                // Set the Text.
                nativeAdTitle.setText(nativeAd.getAdvertiserName());
                nativeAdBody.setText(nativeAd.getAdBodyText());
                /*nativeAdSocialContext.setText(nativeAd.getAdSocialContext());*/
                nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                // Create a list of clickable views
                List<View> clickableViews = new ArrayList<>();
                clickableViews.add(nativeAdTitle);
                clickableViews.add(nativeAdCallToAction);

                // Register the Title and CTA button to listen for clicks.
                nativeAd.registerViewForInteraction(
                        adView, nativeAdIcon, imageView, clickableViews);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        };

        nativeAd.loadAd(
                nativeAd.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build());
    }

    private void populatead(UnifiedNativeAdView adView, UnifiedNativeAd unifiedNativeAd) {
        adView.setIconView(adView.findViewById(R.id.ad_media));
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
        }

        if (unifiedNativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }

        Log.d(TAG, "setNative: logo  " + unifiedNativeAd.getIcon());

        if (unifiedNativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    unifiedNativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(unifiedNativeAd);
    }

    private void interAdListnear2() {
        mInterstitialAd2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d(TAG, "onAdFailedToLoad: fail");
//mm
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                if (showadscoin) {
                    showadscoin = false;
                    addMoney(sessionManager.getStringValue(Const.REWARD_COINS));
                } else {
                    finish();
                }
                loadad();
                // Code to be executed when the interstitial ad is closed.
            }
        });
    }

    private void getUser() {
        Call<ProfileRoot> call = RetrofitBuilder.create(this).getUser(sessionManager.getUser().get_id());
        call.enqueue(new Callback<ProfileRoot>() {
            @Override
            public void onResponse(Call<ProfileRoot> call, Response<ProfileRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && response.body().getData() != null) {
                    binding.tvcoin.setText(String.valueOf(response.body().getData().getCoin()));
                }
            }

            @Override
            public void onFailure(Call<ProfileRoot> call, Throwable t) {
//ll
            }
        });
    }

    public void addMoney(String coin) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", userId);
        jsonObject.addProperty("coin", coin);
        Call<UserRoot> call = RetrofitBuilder.create(this).addCoin(jsonObject);
        call.enqueue(new Callback<UserRoot>() {
            @Override
            public void onResponse(Call<UserRoot> call, Response<UserRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && response.body().getData() != null) {
                    binding.tvcoin.setText(String.valueOf(response.body().getData().getCoin()));
                    sessionManager.saveUser(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<UserRoot> call, Throwable t) {
//ll
            }
        });
    }

    private void getOwnAds() {
        RetrofitBuilder.create(this).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                    ownLoded = true;
                    ownAds = response.body().getData().get(0);
                    String[] c1 = ownAds.getColor().split("f");
                    String color = c1[0];
                    Log.d(TAG, "onResponse: own ad getted " + ownAds.getTitle());
                    binding.itemBannerownad.tvtitle.setText(ownAds.getName());
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemBannerownad.imglogo);
                    binding.itemBannerownad.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    binding.itemBannerownad.tvbtn.setText(ownAds.getBtnText());
                    binding.itemBannerownad.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(EarnActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemBannerownad.tvdes.setText(ownAds.getDescription());


                    binding.itemNative1.tvtitle.setText(ownAds.getName());
                    binding.itemNative1.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemNative1.imglogo);
                    binding.itemNative1.tvbtn.setText(ownAds.getBtnText());
                    binding.itemNative1.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(EarnActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemNative1.tvdes.setText(ownAds.getDescription());


                    binding.itemNative2.tvtitle.setText(ownAds.getName());
                    binding.itemNative2.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemNative2.imglogo);
                    binding.itemNative2.tvbtn.setText(ownAds.getBtnText());
                    binding.itemNative2.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(EarnActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemNative2.tvdes.setText(ownAds.getDescription());


                    binding.itemNative3.tvtitle.setText(ownAds.getName());
                    binding.itemNative3.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemNative3.imglogo);
                    binding.itemNative3.tvbtn.setText(ownAds.getBtnText());
                    binding.itemNative3.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(EarnActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemNative3.tvdes.setText(ownAds.getDescription());

                    //TODO istastial

                    binding.itemIntrustial.tvTitle.setText(ownAds.getName());
                    binding.itemIntrustial.rlInstastial.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemIntrustial.imgLogo);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getImage()).centerCrop().into(binding.itemIntrustial.ivBg);
                    binding.itemIntrustial.goWeb.setText(ownAds.getBtnText());
                    binding.itemIntrustial.goWeb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(EarnActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemIntrustial.tvBody.setText(ownAds.getDescription());
                }
            }

            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {
                binding.itemIntrustial.rlInstastial.setVisibility(View.GONE);
                binding.itemBannerownad.lytad.setVisibility(View.GONE);


///mm
            }
        });
    }

    private void fbBannerAdListnear() {
        adListener = new com.facebook.ads.AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback

            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
            }
        };
    }

    private void setOwnAds() {
        sendImpression(this, adid);
        binding.lytOwnAds.setVisibility(View.VISIBLE);

        videoView = binding.videoView;
        try {
            videoView.setVideoURI(Uri.parse(new SessionManager(EarnActivity.this).getStringValue(Const.IMAGE_URL) + ownAdRewardUrl));
        } catch (Exception e) {
            e.printStackTrace();
        }
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            boolean isMute = false;


            @Override
            public void onPrepared(MediaPlayer m) {
                mp = m;

                new CountDownTimer(5000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        binding.tvTime.setText(String.valueOf((int) (millisUntilFinished / 1000)));
                        binding.btnVolume.setOnClickListener(v -> toggleVolume());
                    }

                    @Override
                    public void onFinish() {
                        binding.tvTime.setVisibility(View.GONE);
                        binding.btnClose.setVisibility(View.VISIBLE);
                        binding.btnClose.setOnClickListener(v -> closeOwnAds());
                        addMoney(sessionManager.getStringValue(Const.REWARD_COINS));
                    }
                }.start();
                videoView.start();
            }

            private void closeOwnAds() {

                if (mp != null) {
                    mp.release();
                    mp = null;
                }

                binding.lytOwnAds.setVisibility(View.GONE);

            }

            private void toggleVolume() {
                if (isMute) {
                    binding.btnVolume.setImageDrawable(ContextCompat.getDrawable(EarnActivity.this, R.drawable.ic_round_volume_off_24));
                    mp.setVolume(10000, 10000);
                    isMute = false;
                } else {
                    mp.setVolume(0, 0);
                    binding.btnVolume.setImageDrawable(ContextCompat.getDrawable(EarnActivity.this, R.drawable.ic_round_volume_down_24));
                    isMute = true;
                }
            }
        });
        videoView.setOnErrorListener((mp, what, extra) -> {
            Log.d(TAG, "onError:videoeee ");
            if (mp != null) {
                mp.release();
                mp = null;
            }
            binding.lytOwnAds.setVisibility(View.GONE);
            return false;
        });

        binding.videoView.setOnClickListener(v -> {
            Intent intent = new Intent(EarnActivity.this, WebActivity.class);
            intent.putExtra("ADID", String.valueOf(adid));
            intent.putExtra(WEBSITE, ownWebUrl);
            intent.putExtra("type", "ads");
            startActivity(intent);
        });

    }

    public void onClickBack(View view) {
        finish = true;
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else if (mInterstitialAd2.isLoaded()) {
                mInterstitialAd2.show();
            } else if (interstitialAdfb.isAdLoaded()) {
                interstitialAdfb.show();
            } else if (ownLoded) {
                Anylitecs.sendImpression(EarnActivity.this, ownAds.getId());
                binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
                binding.itemIntrustial.closeBtn.setOnClickListener(v -> finish());
               /* binding.lytOwnInter.setVisibility(View.VISIBLE);
                sendImpression(this, adid);
                binding.lytOwnInter.setOnClickListener(v -> {
                    Intent intent = new Intent(EarnActivity.this, WebActivity.class);
                    intent.putExtra("ADID", String.valueOf(adid));
                    intent.putExtra(WEBSITE, ownWebUrl);
                    intent.putExtra("type", "ads");
                    startActivity(intent);
                });
                binding.imgCloseInter.setOnClickListener(v -> finish());*/
            } else {
                finish();
            }


    }

    @Override
    public void onBackPressed() {
        finish = true;
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else if (mInterstitialAd2.isLoaded()) {
                mInterstitialAd2.show();
            } else if (interstitialAdfb.isAdLoaded()) {
                interstitialAdfb.show();
            } else if (ownLoded) {
                Anylitecs.sendImpression(EarnActivity.this, ownAds.getId());
                binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
                binding.itemIntrustial.closeBtn.setOnClickListener(v -> finish());
                /*binding.lytOwnInter.setVisibility(View.VISIBLE);
                sendImpression(this, adid);
                binding.lytOwnInter.setOnClickListener(v -> {
                    Intent intent = new Intent(EarnActivity.this, WebActivity.class);
                    intent.putExtra("ADID", String.valueOf(adid));
                    intent.putExtra(WEBSITE, ownWebUrl);
                    intent.putExtra("type", "ads");
                    startActivity(intent);
                });
                binding.imgCloseInter.setOnClickListener(v -> finish());*/
            } else {
                finish();
            }

    }


    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void setBanner() {
        LinearLayout adContainer = findViewById(R.id.banner_container);
        adView = new AdView(this);
        adContainer.setVisibility(View.VISIBLE);
        adView.setAdUnitId((google != null) ? google.getBanner() : "");
        adContainer.addView(adView);
        AdRequest adRequest =
                new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();
        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);


        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Log.d(TAG, "onAdFailedToLoad: banner " + loadAdError);
                adContainer.removeAllViews();
                setgooglebanner2();
            }

            private void setgooglebanner2() {
                LinearLayout adContainer = findViewById(R.id.banner_container);
                adView = new AdView(EarnActivity.this);

                adContainer.setVisibility(View.VISIBLE);
                adView.setAdUnitId((google2 != null) ? google2.getBanner() : "");
                adContainer.addView(adView);
                AdRequest adRequest =
                        new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                                .build();
                AdSize adSize = getAdSize();
                adView.setAdSize(adSize);
                adView.loadAd(adRequest);


                adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        Log.d(TAG, "onAdFailedToLoad: banner " + loadAdError);
                        adContainer.removeAllViews();
                        setFbBanner();
                    }
                });

            }

            private void setFbBanner() {
                adViewfb = new com.facebook.ads.AdView(EarnActivity.this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);

                adContainer.addView(adViewfb);
                adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.d(TAG, "onError: fb " + adError.getErrorMessage());
                        if (ownLoded) {
                            Anylitecs.sendImpression(EarnActivity.this, ownAds.getId());
                            binding.itemBannerownad.lytad.setVisibility(View.VISIBLE);
                        }

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        adContainer.setVisibility(View.VISIBLE);
//mm
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
//mm
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
//mm
                    }
                }).build());
            }

        });
    }


    @Override
    protected void onDestroy() {
        Anylitecs.removeSesson(this);
        if (rewardedVideoAd != null) {
            rewardedVideoAd.destroy();
            rewardedVideoAd = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Anylitecs.removeSesson(this);
        Log.d(TAG, "onPause: ");
    }

    public void onclickvid1(View view) {
        showadscoin = true;
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();

        } else if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else if (ownLoded) {
            Anylitecs.sendImpression(EarnActivity.this, ownAds.getId());
            binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
            binding.itemIntrustial.closeBtn.setOnClickListener(v -> {
                binding.itemIntrustial.rlInstastial.setVisibility(View.GONE);
                addMoney(sessionManager.getStringValue(Const.REWARD_COINS));
            });
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Anylitecs.addUser(this);
    }

    private void showAdsgoogle() {

        ProgressDialog progress;
        progress = new ProgressDialog(this);
        progress.setMessage("showing ads");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setProgress(0);
        progress.show();
        progress.setCancelable(false);


        Activity activityContext = EarnActivity.this;

        rewardedAd = new RewardedAd(this,
                (google != null) ? google.getReward() : "");
        RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {

            @Override
            public void onRewardedAdLoaded() {
                // Ad successfully loaded.
                progress.dismiss();
                rewardedAd.show(activityContext, new RewardedAdCallback() {
                    @Override
                    public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                        Log.d("TAG", "onUserEarnedReward: rewared");
                        addMoney(sessionManager.getStringValue(Const.REWARD_COINS));
                    }

                });
                Log.d("TAG", "onRewardedAdLoaded: loded");
            }

            @Override
            public void onRewardedAdFailedToLoad(LoadAdError adError) {
                // Ad failed to load.
                progress.dismiss();
                if (interstitialAdfb.isAdLoaded()) {
                    isreawrdfbreward = true;
                    interstitialAdfb.show();
                } else {
                    setOwnAds();
                }
                Log.d("TAG", "onRewardedAdFailedToLoad: faill " + adError.toString());
            }

        };
        rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);


    }


    private void interAdListnear() {
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d(TAG, "onAdFailedToLoad: fail");
//mm
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                if (showadscoin) {
                    showadscoin = false;
                    addMoney(sessionManager.getStringValue(Const.REWARD_COINS));
                } else {
                    finish();
                }
                loadad();
                // Code to be executed when the interstitial ad is closed.
            }

        });


    }


    private void fbInterAdListnear() {
        interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                if (showadscoin) {
                    showadscoin = false;
                    addMoney(sessionManager.getStringValue(Const.REWARD_COINS));
                } else {
                    finish();
                }
                loadad();
//
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {


                // Interstitial ad is loaded and ready to be displayed
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                // Show the ad
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        };
    }

}