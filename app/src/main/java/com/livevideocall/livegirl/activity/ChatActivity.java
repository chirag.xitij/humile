package com.livevideocall.livegirl.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.livevideocall.livegirl.Anylitecs;
import com.livevideocall.livegirl.BuildConfig;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.adapters.ChatAdapter;
import com.livevideocall.livegirl.adapters.QuestionAdapter;
import com.livevideocall.livegirl.databinding.ActivityChatBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.ModelChat;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.retrofit.Const;
import com.livevideocall.livegirl.retrofit.RetrofitBuilder;
import com.livevideocall.livegirl.retrofit.RetrofitService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChatActivity extends AppCompatActivity {
    private static final int SELECT_PICTURE = 100;
    private static final String TAG = "chatact";
    private static final String WEBSITE = "WEBSITE";

    ActivityChatBinding binding;
    ChatAdapter chatAdapter = new ChatAdapter();
    List<String> messages = new ArrayList<>();
    List<String> photos = new ArrayList<>();
    Random random = new Random();
    InterstitialAdListener interstitialAdListener;
    AdView adView;
    RetrofitService service;
    boolean fetched = false;
    private String adid;
    private SessionManager sessionManager;
    private InterstitialAd mInterstitialAd;
    private com.facebook.ads.InterstitialAd interstitialAdfb;
    private com.facebook.ads.AdListener adListener;
    private com.facebook.ads.AdView adViewfb;
    private String ownAdRewardUrl = "";
    private String ownAdBannerUrl = "";
    private String ownAdInstarUrl = "";
    private String ownWebUrl = "";
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Facebook facebook;
    private boolean ownLoded = false;
    private boolean enable = true;
    private AdvertisementRoot.Google2 google2;
    private InterstitialAd mInterstitialAd2;
    private OwnAdsRoot.DataItem ownAds;
//    private GGInterstitialAd mAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.chatprimary));
        }
        MainActivity.setStatusBarGradiant(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);
        sessionManager = new SessionManager(this);
        initAds();


        getInatentData();
        initListner();
        prepareRobot();
        setQuestions();
        binding.rvchats.setAdapter(chatAdapter);


    }

    private void initAds() {
        MobileAds.initialize(this, initializationStatus -> {
        });
        AudienceNetworkAds.initialize(this);

        getOwnAds();
        fetched = true;
        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {

            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
            }

            setBanner();

            /* mAd = new GGInterstitialAd(this, (google != null) ? google.getInterstitial() : "");*/
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId((google != null) ? google.getInterstitial() : "");
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            interAdListnear();

            mInterstitialAd2 = new InterstitialAd(this);
            mInterstitialAd2.setAdUnitId((google2 != null) ? google2.getInterstitial() : "");
            mInterstitialAd2.loadAd(new AdRequest.Builder().build());
            interAdListnear2();

            fbInterAdListnear();
            interstitialAdfb = new com.facebook.ads.InterstitialAd(this, (facebook != null) ? facebook.getInterstitial() : "");
            interstitialAdfb.loadAd(
                    interstitialAdfb.buildLoadAdConfig()
                            .withAdListener(interstitialAdListener)
                            .build());


            adViewfb = new com.facebook.ads.AdView(this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
            LinearLayout adContainer = findViewById(R.id.banner_container);
            adContainer.addView(adViewfb);
            adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(adListener).build());
        } else {
            Toast.makeText(this, "ads not downloded ", Toast.LENGTH_SHORT).show();
        }
    }

    private void interAdListnear2() {

        mInterstitialAd2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
//mm
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                finish();
                // Code to be executed when the interstitial ad is closed.
            }
        });


    }

    private void getOwnAds() {
        RetrofitBuilder.create(this).getOwnAds().enqueue(new Callback<OwnAdsRoot>() {
            @Override
            public void onResponse(Call<OwnAdsRoot> call, Response<OwnAdsRoot> response) {
                if (response.code() == 200 && response.body().getStatus() == 200 && !response.body().getData().isEmpty()) {
                    ownLoded = true;
                    ownAds = response.body().getData().get(0);
                    String[] c1 = ownAds.getColor().split("f");
                    String color = c1[0];
                    //TODO banner

                    binding.itemBannerownad.tvtitle.setText(ownAds.getName());
                    binding.itemBannerownad.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemBannerownad.imglogo);
                    binding.itemBannerownad.tvbtn.setText(ownAds.getBtnText());
                    binding.itemBannerownad.tvbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(ChatActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemBannerownad.tvdes.setText(ownAds.getDescription());

                    //TODO istastial

                    binding.itemIntrustial.tvTitle.setText(ownAds.getName());
                    binding.itemIntrustial.rlInstastial.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(binding.itemIntrustial.imgLogo);
                    Glide.with(getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getImage()).centerCrop().into(binding.itemIntrustial.ivBg);
                    binding.itemIntrustial.goWeb.setText(ownAds.getBtnText());
                    binding.itemIntrustial.goWeb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Anylitecs.sendhit(ChatActivity.this, ownAds.getId());
                            String url = ownAds.getWebsite();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });
                    binding.itemIntrustial.tvBody.setText(ownAds.getDescription());

                }
            }

            @Override
            public void onFailure(Call<OwnAdsRoot> call, Throwable t) {

                binding.itemIntrustial.rlInstastial.setVisibility(View.GONE);
                binding.itemBannerownad.lytad.setVisibility(View.GONE);
///mm
            }
        });
    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void setBanner() {
        LinearLayout adContainer = findViewById(R.id.banner_container);
        adView = new AdView(this);

        adView.setAdUnitId((google != null) ? google.getBanner() : "");
        adContainer.addView(adView);
        AdRequest adRequest =
                new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();
        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);


        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Log.d(TAG, "onAdFailedToLoad: banner " + loadAdError);
                adContainer.removeAllViews();
                setgooglebanner2();
            }


       /* GGAdview bannerUnit = new GGAdview(this);
        bannerUnit.setUnitId((google != null) ? google.getBanner() : " ");  //Replace with your Ad Unit ID here
        bannerUnit.setAdsMaxHeight(250);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 200);
        adContainer.addView(bannerUnit, layoutParams);
        bannerUnit.loadAd(new AdLoadCallback() {

            @Override
            public void onAdLoadFailed(@NotNull AdRequestErrors adRequestErrors) {

                setFbBanner();
            }

            private void setFbBanner() {
                adViewfb = new com.facebook.ads.AdView(EarnActivity.this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);

                adContainer.addView(adViewfb);
                adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.d(TAG, "onError: fb " + adError.getErrorMessage());
                        binding.imgOwnAd.setVisibility(View.VISIBLE);
                        sendImpression(EarnActivity.this, adid);
                        binding.imgOwnAd.setOnClickListener(v -> {
                            Intent intent = new Intent(EarnActivity.this, WebActivity.class);
                            intent.putExtra("ADID", String.valueOf(adid));
                            intent.putExtra(WEBSITE, ownWebUrl);
                            intent.putExtra("type", "ads");
                            startActivity(intent);
                        });

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
//mm
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
//mm
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
//mm
                    }
                }).build());
            }


            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onReadyForRefresh() {

            }

            @Override
            public void onUiiOpened() {
            }

            @Override
            public void onUiiClosed() {

            }

        });*/

            private void setgooglebanner2() {
                LinearLayout adContainer = findViewById(R.id.banner_container);
                adView = new AdView(ChatActivity.this);

                adView.setAdUnitId((google2 != null) ? google2.getBanner() : "");
                adContainer.addView(adView);
                AdRequest adRequest =
                        new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                                .build();
                AdSize adSize = getAdSize();
                adView.setAdSize(adSize);
                adView.loadAd(adRequest);


                adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        Log.d(TAG, "onAdFailedToLoad: banner " + loadAdError);
                        adContainer.removeAllViews();
                        setFbBanner();
                    }
                });

            }

            private void setFbBanner() {
                adViewfb = new com.facebook.ads.AdView(ChatActivity.this, (facebook != null) ? facebook.getBanner() : "", com.facebook.ads.AdSize.BANNER_HEIGHT_50);

                adContainer.addView(adViewfb);
                adViewfb.loadAd(adViewfb.buildLoadAdConfig().withAdListener(new com.facebook.ads.AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        Log.d(TAG, "onError: fb " + adError.getErrorMessage());
                        if (ownLoded) {
                            Anylitecs.sendImpression(ChatActivity.this, ownAds.getId());
                            binding.itemBannerownad.lytad.setVisibility(View.VISIBLE);
                        }
                        /*binding.imgOwnAd.setVisibility(View.VISIBLE);
                        sendImpression(ChatActivity.this, adid);
                        binding.imgOwnAd.setOnClickListener(v -> {
                            Intent intent = new Intent(ChatActivity.this, WebActivity.class);
                            intent.putExtra("ADID", String.valueOf(adid));
                            intent.putExtra(WEBSITE, ownWebUrl);
                            intent.putExtra("type", "ads");
                            startActivity(intent);
                        });*/

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
//mm
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
//mm
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
//mm
                    }
                }).build());
            }

        });
    }


    private void interAdListnear() {
        /*mAd.loadAd(new GGInterstitialEventsListener() {
                       @Override
                       public void onAdLoaded() {
                           Log.d("GGADS", "Ad Loaded");
                       }

                       @Override
                       public void onAdLeftApplication() {
                           Log.d("GGADS", "Ad Left Application");
                       }

                       @Override
                       public void onAdClosed() {
                           Log.d("GGADS", "Ad Closed");
                           finish();
                           //Interstitial ad will be automatically refreshed by SDKX when     closed
                       }

                       @Override
                       public void onAdOpened() {
                           Log.d("GGADS", "Ad Opened");
                       }

                       @Override
                       public void onAdLoadFailed(AdRequestErrors cause) {
                           Log.d("GGADS", "Ad Load Failed " + cause);
                       }
                   }
        );*/

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
//mm
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                finish();
                // Code to be executed when the interstitial ad is closed.
            }
        });


    }

    private void fbInterAdListnear() {
        interstitialAdListener = new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                finish();
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {


                // Interstitial ad is loaded and ready to be displayed
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                // Show the ad
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        };
    }

    private void setQuestions() {

        List<Questions> questions = new ArrayList<>();
        questions.add(new Questions("Hii", "Heyy"));
        questions.add(new Questions("Send Me your Pic", "IMAGE"));
        questions.add(new Questions("Boyfriend?", "No, Do you like me?"));
        questions.add(new Questions("How Are You?", "I am fine"));
        questions.add(new Questions("Do you like me??", "Yeh of Course"));
        questions.add(new Questions("You are gorgeous", "Awww Thankyou so much"));
        questions.add(new Questions("What is Your name?", binding.tvName.getText().toString()));


        Collections.shuffle(questions);


        QuestionAdapter questionAdapter = new QuestionAdapter(questions, questions1 -> {
            if (!enable) {

                return;
            } else {
                enable = false;

                ModelChat modelChat = new ModelChat(true, questions1.getQuestion(), null, null);
                chatAdapter.addData(modelChat);
                binding.rvchats.scrollToPosition(chatAdapter.getItemCount() - 1);

                int rep = random.nextInt(5);
                Log.d(TAG, "setQuestions: " + rep);
                if (rep == 2 || rep == 4) {
                    new Handler().postDelayed(() -> binding.tvtyping.setVisibility(View.VISIBLE), 2000);

                    if (questions1.getAnswer().equals("IMAGE")) {
                        new Handler().postDelayed(() -> {
                            int i = random.nextInt(photos.size() - 1);
                            String photo = photos.get(i);
                            ModelChat modelChat2 = new ModelChat(false, "", null, photo);
                            chatAdapter.addData(modelChat2);
                            binding.rvchats.scrollToPosition(chatAdapter.getItemCount() - 1);
                            enable = true;
                            binding.tvtyping.setVisibility(View.GONE);
                        }, 3000);

                    } else {
                        new Handler().postDelayed(() -> {


                            ModelChat modelChat2 = new ModelChat(false, questions1.getAnswer(), null, null);
                            chatAdapter.addData(modelChat2);
                            binding.rvchats.scrollToPosition(chatAdapter.getItemCount() - 1);
                            enable = true;
                            binding.tvtyping.setVisibility(View.GONE);
                        }, 3000);

                    }
                }

                new Handler().postDelayed(() -> {
                    enable = true;
                }, 3000);
            }
        });
        binding.rvquestions.setAdapter(questionAdapter);
    }

    private void prepareRobot() {
        messages.add("What's your name ?");
        messages.add("Hey do you want chat with me ?");
        messages.add("Which color would you like ?");
        messages.add("Hmm");
        messages.add("What ?");
        messages.add("Are you kidding with me?");
        messages.add("Do  you have  girlfriend?");
        messages.add("hey Dude I am boring now \n so you can chat with me");
        messages.add("I am busy right now \n talk to you later");
        messages.add("Send me your instagram id");
        messages.add("I am " + binding.tvName.getText().toString() + " who are you ?");
        messages.add("Send me your Number");

        messages.add("Am i looking Sexy??");
        messages.add("wow");
        messages.add("woo...");
        messages.add("wooo");
        messages.add("nice");
        messages.add("cool");
        messages.add("mmm..");
        messages.add("oh");
        messages.add("oooh..");
        messages.add("mmmmm...");


        messages.add("First i need to understand u r my type or not");
        messages.add("Let's give some time here to know each other then maybe we exchange");
        messages.add("I don't know anything about u first communicate here only");
        messages.add("Give some time to develop this relation then may be we share personal");
        messages.add("No personal here first we should know each other");
        messages.add("May be some time later when we know each other");
        messages.add("No personal only fun chat here only");
        messages.add("I can't exchange my details sorry first let's talk here only");
        messages.add("Talk here only i can't share my personal");
        messages.add("No means no that's it");


        photos.add("g1");
        photos.add("g3");
        photos.add("g4");
        photos.add("g5");

        photos.add("g13");

        photos.add("g21");
        photos.add("g22");

        photos.add("g24");
        photos.add("g25");
        photos.add("g26");


    }

    private void initListner() {
        binding.btnsend.setOnClickListener(v -> {
            binding.btnsend.setEnabled(false);
            String msg = binding.etChat.getText().toString();
            if (msg.equals("")) {
                Toast.makeText(this, "Enter Comment First", Toast.LENGTH_SHORT).show();
            } else {
                ModelChat modelChat = new ModelChat(true, msg, null, "g1");
                chatAdapter.addData(modelChat);
                binding.rvchats.scrollToPosition(chatAdapter.getItemCount() - 1);
                binding.etChat.setText("");

                int rep = random.nextInt(5);

                if (rep == 2 || rep == 4) {
                    new Handler().postDelayed(() -> binding.tvtyping.setVisibility(View.VISIBLE), 2000);


                    new Handler().postDelayed(() -> {
                        String message;
                        if (msg.contains("hi") || msg.contains("Hi") || msg.contains("HI")) {
                            message = "hii";
                        } else {
                            int i = random.nextInt(messages.size() - 1);
                            message = messages.get(i);
                        }

                        ModelChat modelChat2 = new ModelChat(false, message, null, null);
                        chatAdapter.addData(modelChat2);
                        binding.rvchats.scrollToPosition(chatAdapter.getItemCount() - 1);
                        binding.btnsend.setEnabled(true);
                        binding.tvtyping.setVisibility(View.GONE);
                    }, 3000);
                }

            }

            new Handler().postDelayed(() -> {
                enable = true;
                binding.btnsend.setEnabled(true);
            }, 3000);
        });
    }

    private void getInatentData() {
        Intent intent = getIntent();
        String image = intent.getStringExtra("image");
        if (image != null && !image.equals("")) {
            Glide.with(getApplicationContext())
                    .load(new SessionManager(ChatActivity.this).getStringValue(Const.IMAGE_URL) + image).placeholder(R.drawable.adduser)
                    .circleCrop()
                    .into(binding.imgprofile);

        }
        String name = intent.getStringExtra("name");
        if (name != null && !name.equals("")) {
            binding.tvName.setText(name);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Anylitecs.removeSesson(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Anylitecs.removeSesson(this);
        Log.d(TAG, "onPause: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Anylitecs.addUser(this);
    }

    public void onClickCamara(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == SELECT_PICTURE && data != null) {
            Uri selectedImageUri = data.getData();

            ModelChat modelChat = new ModelChat(true, "", selectedImageUri, null);
            chatAdapter.addData(modelChat);
            binding.rvchats.scrollToPosition(chatAdapter.getItemCount() - 1);

            int rep = random.nextInt(5);

            if (rep == 2 || rep == 4) {
                new Handler().postDelayed(() -> binding.tvtyping.setVisibility(View.VISIBLE), 2000);

                new Handler().postDelayed(() -> {
                    int i = random.nextInt(photos.size() - 1);
                    String photo = photos.get(i);
                    ModelChat modelChat2 = new ModelChat(false, "", null, photo);
                    chatAdapter.addData(modelChat2);
                    binding.rvchats.scrollToPosition(chatAdapter.getItemCount() - 1);

                    binding.tvtyping.setVisibility(View.GONE);
                }, 3000);
            }

        }

    }

    public void onClickBack(View view) {

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else if (ownLoded) {
            Anylitecs.sendImpression(ChatActivity.this, ownAds.getId());
            binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
           /* binding.lytOwnInter.setVisibility(View.VISIBLE);
            sendImpression(this, adid);
            binding.lytOwnInter.setOnClickListener(v -> {
                Intent intent = new Intent(ChatActivity.this, WebActivity.class);
                intent.putExtra("ADID", String.valueOf(adid));
                intent.putExtra(WEBSITE, ownWebUrl);
                intent.putExtra("type", "ads");
                startActivity(intent);
            });
            binding.imgCloseInter.setOnClickListener(v -> finish());*/
            binding.itemIntrustial.closeBtn.setOnClickListener(v -> finish());
        } else {
            finish();
        }


    }

    @Override
    public void onBackPressed() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else if (mInterstitialAd2.isLoaded()) {
            mInterstitialAd2.show();
        } else if (interstitialAdfb.isAdLoaded()) {
            interstitialAdfb.show();
        } else if (ownLoded) {
            Anylitecs.sendImpression(ChatActivity.this, ownAds.getId());
            binding.itemIntrustial.rlInstastial.setVisibility(View.VISIBLE);
           /* binding.lytOwnInter.setVisibility(View.VISIBLE);
            sendImpression(this, adid);
            binding.lytOwnInter.setOnClickListener(v -> {
                Intent intent = new Intent(ChatActivity.this, WebActivity.class);
                intent.putExtra("ADID", String.valueOf(adid));
                intent.putExtra(WEBSITE, ownWebUrl);
                intent.putExtra("type", "ads");
                startActivity(intent);
            });
            binding.imgCloseInter.setOnClickListener(v -> finish());*/
            binding.itemIntrustial.closeBtn.setOnClickListener(v -> finish());
        } else {
            finish();
        }


    }

    public class Questions {
        String question;
        String answer;

        public Questions(String hii, String heyy) {
            this.answer = heyy;
            this.question = hii;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }
    }
}