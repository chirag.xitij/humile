package com.livevideocall.livegirl.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OwnAdsRoot {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private int status;

    public List<DataItem> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    public static class DataItem {

        @SerializedName("image")
        private String image;

        @SerializedName("website")
        private String website;

        @SerializedName("btnText")
        private String btnText;

        @SerializedName("color")
        private String color;

        @SerializedName("tagLine")
        private String tagLine;

        @SerializedName("show")
        private boolean show;

        @SerializedName("description")
        private String description;

        @SerializedName("type")
        private String type;

        @SerializedName("title")
        private String title;

        @SerializedName("createdAt")
        private String createdAt;

        @SerializedName("price")
        private int price;

        @SerializedName("__v")
        private int V;

        @SerializedName("name")
        private String name;

        @SerializedName("logo")
        private String logo;

        @SerializedName("_id")
        private String id;

        @SerializedName("updatedAt")
        private String updatedAt;

        public String getImage() {
            return image;
        }

        public String getWebsite() {
            return website;
        }

        public String getBtnText() {
            return btnText;
        }

        public String getColor() {
            return color;
        }

        public String getTagLine() {
            return tagLine;
        }

        public boolean isShow() {
            return show;
        }

        public String getDescription() {
            return description;
        }

        public String getType() {
            return type;
        }

        public String getTitle() {
            return title;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public int getPrice() {
            return price;
        }

        public int getV() {
            return V;
        }

        public String getName() {
            return name;
        }

        public String getLogo() {
            return logo;
        }

        public String getId() {
            return id;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }
    }
}