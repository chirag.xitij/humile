package com.livevideocall.livegirl.models;

import com.google.gson.annotations.SerializedName;

public class ReferRoot {

    @SerializedName("data")
    private Data data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private int status;

    public Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    public static class Data {

        @SerializedName("date")
        private String date;

        @SerializedName("fname")
        private String fname;

        @SerializedName("createdAt")
        private String createdAt;

        @SerializedName("gender")
        private String gender;

        @SerializedName("referralCode")
        private String referralCode;

        @SerializedName("__v")
        private int V;

        @SerializedName("mobile")
        private String mobile;

        @SerializedName("_id")
        private String id;

        @SerializedName("email")
        private String email;

        @SerializedName("coin")
        private int coin;

        @SerializedName("username")
        private String username;

        @SerializedName("updatedAt")
        private String updatedAt;

        public String getDate() {
            return date;
        }

        public String getFname() {
            return fname;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getGender() {
            return gender;
        }

        public String getReferralCode() {
            return referralCode;
        }

        public int getV() {
            return V;
        }

        public String getMobile() {
            return mobile;
        }

        public String getId() {
            return id;
        }

        public String getEmail() {
            return email;
        }

        public int getCoin() {
            return coin;
        }

        public String getUsername() {
            return username;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }
    }
}