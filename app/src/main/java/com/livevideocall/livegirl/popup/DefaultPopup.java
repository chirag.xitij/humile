package com.livevideocall.livegirl.popup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.livevideocall.livegirl.Anylitecs;
import com.livevideocall.livegirl.BuildConfig;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.databinding.PopUpShowAdsBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.models.OwnAdsRoot;
import com.livevideocall.livegirl.retrofit.Const;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class DefaultPopup {

    private static final String TAG = "popup==";
    private final Dialog dialog;
    private final PopUpShowAdsBinding popupbinding;
    SessionManager sessionManager;
    OnPopupClickListnear onPopupClickListnear;
    OwnAdsRoot.DataItem ownAds;
    private Context context;
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Google2 google2;
    private AdvertisementRoot.Facebook facebook;

    public DefaultPopup(Context context, String girlName, String image) {
        sessionManager = new SessionManager(context);
        dialog = new Dialog(context, R.style.customStyle);
        dialog.setCancelable(false);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        popupbinding = DataBindingUtil.inflate(inflater, R.layout.pop_up_show_ads, null, false);
        dialog.setContentView(popupbinding.getRoot());
        popupbinding.itemBannerownad.lytad.setVisibility(View.GONE);
        loadNative();
        Glide.with(context.getApplicationContext())
                .load(BuildConfig.TMDB_API_KEY + image).error(R.drawable.bg_whitebtnround)
                .placeholder(R.drawable.bg_whitebtnround)
                .circleCrop()
                .into(popupbinding.imagepopup);
        popupbinding.tv1.setText("Hello Dear, " + sessionManager.getUser().getFname());
        popupbinding.textview.setText("To countinue with " + girlName + "\n watch this Ads");
        popupbinding.tvPositive.setOnClickListener(v -> onPopupClickListnear.onPostitiveClick());
        popupbinding.tvCencel.setOnClickListener(v -> {
            onPopupClickListnear.onCloseClick();
        });
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    private void loadNative() {
        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                google = sessionManager.getAdsKeys().getGoogle();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                google2 = sessionManager.getAdsKeys().getGoogle2();

            }
            if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                facebook = sessionManager.getAdsKeys().getFacebook();
            }

            setGoogle1();

        }
    }

    private void setGoogle1() {
        AdLoader adLoader = new AdLoader.Builder(context, (google != null) ? google.getJsonMemberNative() : "")
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    popupbinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                    Log.d(TAG, "showPopup: popup native loded");
                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) LayoutInflater.from(context).inflate(R.layout.native_google_popup, null);
                    inflateAd(adView, unifiedNativeAd);

                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {

                        Log.d(TAG, "onAdFailedToLoad: popup nativew  " + adError.getMessage());
                        setGoogle2();
                    }

                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void setGoogle2() {
        AdLoader adLoader = new AdLoader.Builder(context, (google2 != null) ? google2.getJsonMemberNative() : "")
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    // Show the ad.
                    popupbinding.flAdplaceholderbanner.setVisibility(View.VISIBLE);
                    Log.d(TAG, "showPopup: popup native loded222");
                    UnifiedNativeAdView adView =
                            (UnifiedNativeAdView) LayoutInflater.from(context).inflate(R.layout.native_google_popup, null);
                    inflateAd(adView, unifiedNativeAd);

                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError adError) {

                        Log.d(TAG, "onAdFailedToLoad: popup nativew22  " + adError.getMessage());
                        // setGoogle2();
                        setFacebookAd();
                    }

                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        .build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void setFacebookAd() {
        NativeAd nativeAd = new NativeAd(context, (facebook != null) ? facebook.getJsonMemberNative() : " ");
        NativeAdListener nativeAdListener = new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
                Log.e("TAG", "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                popupbinding.flAdplaceholderbanner.setVisibility(View.GONE);
                popupbinding.nativeAdFb.setVisibility(View.GONE);
//                Anylitecs.sendImpression(context,ownAds.getId());
                showOwnAd();
                // Native ad failed to load
                Log.e("TAG", "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                popupbinding.nativeAdFb.setVisibility(View.VISIBLE);
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }

                inflateAd(nativeAd);
                Log.d("TAG", "Native ad is loaded and ready to be displayed!");

             /*   } catch (Exception e) {
                    popupbinding.flAdplaceholderbanner.setVisibility(View.GONE);
                    popupbinding.nativeAdFb.setVisibility(View.GONE);
                    Log.d(TAG, "onAdLoaded: catcgh eror"+e.toString());
                }*/
                // Native ad is loaded and ready to be displayed

            }

            private void inflateAd(NativeAd nativeAd) {
                popupbinding.nativeAdFb.setVisibility(View.VISIBLE);
                nativeAd.unregisterView();

                // Add the Ad view into the ad container.

                NativeAdLayout nativeAdLayout = popupbinding.nativeAdFb;

                LayoutInflater inflater = LayoutInflater.from(context);
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.native_fb_popup, nativeAdLayout, false);
                nativeAdLayout.addView(adView);


                com.facebook.ads.MediaView nativeAdIcon = adView.findViewById(R.id.ad_media);
                ImageView imageView = adView.findViewById(R.id.ad_logo);
                TextView nativeAdTitle = adView.findViewById(R.id.ad_headline);

                TextView nativeAdBody = adView.findViewById(R.id.ad_body);
                TextView nativeAdCallToAction = adView.findViewById(R.id.ad_call_to_action);

                // Set the Text.
                nativeAdTitle.setText(nativeAd.getAdvertiserName() != null ? nativeAd.getAdvertiserName() : "");
                nativeAdBody.setText(nativeAd.getAdBodyText() != null ? nativeAd.getAdBodyText() : "");
                nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                nativeAdCallToAction.setText(nativeAd.hasCallToAction() ? nativeAd.getAdCallToAction() : "");

                // Create a list of clickable views
                List<View> clickableViews = new ArrayList<>();
                clickableViews.add(nativeAdTitle);
                clickableViews.add(nativeAdCallToAction);

                // Register the Title and CTA button to listen for clicks.
                nativeAd.registerViewForInteraction(
                        adView, nativeAdIcon, imageView, clickableViews);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        };
        nativeAd.loadAd(
                nativeAd.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build());

    }

    private void showOwnAd() {
        if (ownAds != null) {
            Log.d(TAG, "showOwnAd: done popup");
            String[] c1 = ownAds.getColor().split("f");
            String color = c1[0];
            popupbinding.itemBannerownad.lytad.setVisibility(View.VISIBLE);
            popupbinding.itemBannerownad.tvtitle.setText(ownAds.getName());
            popupbinding.itemBannerownad.lytad.getBackground().setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
            Glide.with(context.getApplicationContext()).load(BuildConfig.TMDB_API_KEY + ownAds.getLogo()).centerCrop().into(popupbinding.itemBannerownad.imglogo);
            popupbinding.itemBannerownad.tvbtn.setText(ownAds.getBtnText());
            popupbinding.itemBannerownad.tvbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Anylitecs.sendhit(context, ownAds.getId());
                    String url = ownAds.getWebsite();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);
                }
            });
            popupbinding.itemBannerownad.tvdes.setText(ownAds.getDescription());
        } else {
            popupbinding.itemBannerownad.lytad.setVisibility(View.GONE);

        }
    }

    private void inflateAd(UnifiedNativeAdView adView, UnifiedNativeAd unifiedNativeAd) {

        ImageView imageView = adView.findViewById(R.id.ad_media);
        Glide.with(context.getApplicationContext()).load(unifiedNativeAd.getMediaContent().getMainImage()).circleCrop().transform(new RoundedCorners(20)).into(imageView);
        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));


        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());


        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (unifiedNativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
        }

        if (unifiedNativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }




        adView.setNativeAd(unifiedNativeAd);
        popupbinding.flAdplaceholderbanner.removeAllViews();
        popupbinding.flAdplaceholderbanner.addView(adView);
    }

    public void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void sendOwnAd(OwnAdsRoot.DataItem ownAds) {
        popupbinding.itemBannerownad.lytad.setVisibility(View.GONE);
        this.ownAds = ownAds;
    }

    public OnPopupClickListnear getOnPopupClickListnear() {
        return onPopupClickListnear;
    }

    public void setOnPopupClickListnear(OnPopupClickListnear onPopupClickListnear) {
        this.onPopupClickListnear = onPopupClickListnear;
    }

    public interface OnPopupClickListnear {
        void onPostitiveClick();

        void onCloseClick();
    }
}
