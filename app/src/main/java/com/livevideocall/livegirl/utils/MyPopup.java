package com.livevideocall.livegirl.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.livevideocall.livegirl.R;
import com.livevideocall.livegirl.SessionManager;
import com.livevideocall.livegirl.databinding.PopUpShowAdsBinding;
import com.livevideocall.livegirl.models.AdvertisementRoot;
import com.livevideocall.livegirl.retrofit.Const;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class MyPopup {

    SessionManager sessionManager;
    OnDilogClickListnear onDilogClickListnear;
    private Dialog dialog;
    private Context context;
    private AdvertisementRoot.Google google;
    private AdvertisementRoot.Google2 google2;
    private AdvertisementRoot.Facebook facebook;

    public MyPopup(Context context) {

        this.context = context;
    }

    public void showPopup(String text1, String text2, String button) {
        sessionManager = new SessionManager(context);
        dialog = new Dialog(context, R.style.customStyle);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        PopUpShowAdsBinding popupbinding = DataBindingUtil.inflate(inflater, R.layout.pop_up_show_ads, null, false);


        dialog.setContentView(popupbinding.getRoot());


        if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {

            if (sessionManager.getBooleanValue(Const.ADS_Downloded)) {

                if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle().isShow())) {
                    google = sessionManager.getAdsKeys().getGoogle();

                }
                if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getGoogle2().isShow())) {
                    google2 = sessionManager.getAdsKeys().getGoogle2();

                }
                if (Boolean.TRUE.equals(sessionManager.getAdsKeys().getFacebook().isShow())) {
                    facebook = sessionManager.getAdsKeys().getFacebook();
                }

               /* GGAdview bannerUnit = new GGAdview(context);
                bannerUnit.setUnitId((google != null) ? google.getBanner() : " ");  //Replace with your Ad Unit ID here
                bannerUnit.setAdsMaxHeight(400); //Value is in pixels, not in dp*/
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                popupbinding.flAdplaceholderbanner.removeAllViews();
                /*popupbinding.flAdplaceholderbanner.addView(, layoutParams);*/
            }
        }

        if (sessionManager.getStringValue(Const.PROFILE_IMAGE).equals("")) {
            popupbinding.profilechar.setVisibility(View.VISIBLE);
            popupbinding.profilechar.setText(String.valueOf(sessionManager.getUser().getFname().charAt(0)).toUpperCase());
        }

        Glide.with(context.getApplicationContext())
                .load(sessionManager.getStringValue(Const.PROFILE_IMAGE)).error(R.drawable.bg_whitebtnround)
                .placeholder(R.drawable.bg_whitebtnround)
                .circleCrop()
                .into(popupbinding.imagepopup);
        if (sessionManager.getUser().getFname() != null) {
            popupbinding.tv1.setText(text1);
        }
        popupbinding.textview.setText(text2);
        popupbinding.tvPositive.setText(button);
        popupbinding.tvPositive.setOnClickListener(v -> {

            onDilogClickListnear.onDilogPositiveClick();
            //dialog.dismiss();
            // Toast.makeText(context, "Your Request has been Sended!!", Toast.LENGTH_SHORT).show();
        });
        popupbinding.tvCencel.setOnClickListener(v -> dialog.dismiss());


        //dialog.show();

    }

    public void dismissPopup() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public OnDilogClickListnear getOnDilogClickListnear() {
        return onDilogClickListnear;
    }

    public void setOnDilogClickListnear(OnDilogClickListnear onDilogClickListnear) {
        this.onDilogClickListnear = onDilogClickListnear;
    }

    public interface OnDilogClickListnear {
        void onDilogClickClose();

        void onDilogPositiveClick();
    }
}
